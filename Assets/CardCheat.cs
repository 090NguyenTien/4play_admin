﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardCheat : MonoBehaviour
{
    [SerializeField]
    Image Bich, Chuon, Ro, Co;
    [SerializeField]
    Text NumCard;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Init(string IdCard)
    {
        int valCard = int.Parse(IdCard) % 13;
        int ChatBai = int.Parse(IdCard) / 13;
        string JQK = "";
        if (valCard == 10) JQK = "J";
        else if (valCard == 11) JQK = "Q";
        else if (valCard == 12) JQK = "K";
        if (JQK == "")
        {
            NumCard.text = (valCard + 1).ToString();
        }
        else
        {
            NumCard.text = JQK;
        }
        
        EnableChatBai(ChatBai);
    }
    public void EnableChatBai(int ChatBai)
    {
        Bich.gameObject.SetActive(false);
        Chuon.gameObject.SetActive(false);
        Ro.gameObject.SetActive(false);
        Co.gameObject.SetActive(false);
        switch (ChatBai)
        {
            case 0: Bich.gameObject.SetActive(true); break;
            case 1: Chuon.gameObject.SetActive(true); break;
            case 2: Ro.gameObject.SetActive(true); break;
            case 3: Co.gameObject.SetActive(true); break;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
