﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Demo : MonoBehaviour {

    public Image image;
	// Use this for initialization
	void Start () {
        StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=large&access_token=EAAFFvbnFxoUBAK7e4geiwYWp9xt9thnXjKALyDZCiuSogBCpSOtsfHuyqsauq2Xum3WTkuM9ALzsTO0SqkcMC36t7tFTTTSZAJwjbz86qsn69gansMQn0WrG7vjNo0ASdKzVKKZCPmTo5iGZAPZAyOZC4laEByOXk7ndHaL5evJAFpjbNZBXtTdAhyO3zg8erUZD"));
    }
    public IEnumerator UpdateAvatarThread(string _url)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

        image.sprite = spr;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
