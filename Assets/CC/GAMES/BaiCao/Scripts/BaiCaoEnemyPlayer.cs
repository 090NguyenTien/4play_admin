﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using SimpleJSON;

public class BaiCaoEnemyPlayer : BaiCaoBasePlayer
{
    private const int CARD_WIDTH = 56;
    private const int CARD_HEIGHT = 40;

    public GameObject enemyInfo;

    [HideInInspector]
    public bool actived = false;
    [SerializeField]
    private Sprite inviteIcon;
    private Vector3 moneyTextPos;
    [SerializeField]
    PopupInfoEnemy InfoEnemy;
    [SerializeField]
    public int Id_ViTriBom;



    public void NemBomVaoEnemy(int vitri)
    {
		if (actived == true)
        {
            Sprite Ava = this.avatar.sprite;
            Sprite Khung = this.avatarBoder.sprite;
            string ten = this.displayName.text;
            string vip = this.Vip;
            string level = this.txtLevel.text;
            string chip = this.gold.text;
            string gem = Utilities.GetStringMoneyByLong(this.gem);
            string idBang = this.IdBang;
            Debug.LogWarning("Enemy Id = " + this.userId);

            Debug.Log("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem);
            InfoEnemy.Init(userId, Ava, Khung, ten, vip, level, chip, gem, vitri, idBang, sfsId);
        }
    }



    void Awake()
    {
        avatar.gameObject.GetComponent<Button>().onClick.AddListener(() => SendInviteRequest());
        moneyTextPos = moneyText.transform.localPosition;
    }

    public void UpdateTime(float newValue)
    {
        timerImage.fillAmount = newValue;
    }

    private void SendInviteRequest()
    {
        if (!actived)
            controller.SendMessage("BtnInviteOnClick", SendMessageOptions.DontRequireReceiver);
    }

    public override void StopCountDownTime()
    {
        focusPlayer.SetActive(false);
        iTween.Stop(gameObject, true);
        timerBackground.gameObject.SetActive(false);
    }

    public void showSampleCardByIndex(int index)
    {
        if (index == 0) sampleCard.SetActive(true);
        else if (index == 1) sampleCard1.SetActive(true);
        else if (index == 2) sampleCard2.SetActive(true);
    }

    public override void SetInfo(string[] info)
    {
        hideSampleCard();
        enemyInfo.SetActive(true);
        sfsId = int.Parse(info[0]);
        userId = info[1];
        chip = long.Parse(info[3]);

        string userName = info[2];
        string avatar = info[4];
        string vip_collect = info[5];
        this.level = int.Parse(info[6]);
        string My_bor = info[7];
        txtLevel.text = info[6];
        Debug.LogWarning("txtLevel.text=====" + info[6] + "       vip_collect === " + vip_collect + "       bor_avatar === " + My_bor);
        this.Vip = DataHelper.GetStringVipByStringVipPoint(vip_collect);
        //  this.gem = long.Parse(info[8]);

        LayThongTinBangUser(userId);

        int id_Bor = int.Parse(My_bor);
        if (id_Bor != -1)
        {
            if (DataHelper.GetBoderAvatar(My_bor) != null)
            {
                this.avatarBoder.sprite = DataHelper.GetBoderAvatar(My_bor);
                this.avatarBoder.gameObject.SetActive(true);
                this.avatarBoder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                this.avatarBoder.gameObject.SetActive(false);
                this.avatarBoder_Defaut.gameObject.SetActive(true);
            }

        }
        else
        {
            this.avatarBoder.gameObject.SetActive(false);
            this.avatarBoder_Defaut.gameObject.SetActive(true);
        }
        avatarBoder.transform.localScale = new Vector2(1.1f, 1.1f);

        displayName.text = userName;
        gold.text = Utilities.GetStringMoneyByLong(chip);

        this.avatar.gameObject.SetActive(true);
        //this.avatar.transform.GetChild (0).gameObject.SetActive (true);
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        if (MyInfo.SFS_ID == sfsId)
        {
            if (loginType != LoginType.Facebook)
            {
                this.avatar.sprite = DataHelper.GetAvatar(avatar);
            }
            else
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
        }
        else
        {
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
            else
            {
                this.avatar.sprite = DataHelper.GetAvatar(avatar);
            }
        }



        //this.avatarBoder.sprite = DataHelper.GetVip (int.Parse (vip));
        actived = true;
        if (inviteBtn != null)
            inviteBtn.gameObject.SetActive(false);
    }


    public void LayThongTinBangUser(string idUser)
    {
        //  API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangHoi_user_1);
        API.Instance.RequestLayIdBangHoi(idUser, RspLayIdBangHoi);
    }


    void RspLayIdBangHoi(string _json)
    {

        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

        JSONNode node = JSONNode.Parse(_json);

        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

        //  Debug.LogWarning("usserBang------------- " + userBang);
        //  Debug.LogWarning("BangMaster------------- " + BangMaster);

        //IdBangMaster = BangMaster;
        //IdBangUserBiNem = userBang;
        this.IdBang = userBang;
    }





    public void SortCard()
    {
        string name = gameObject.name;
        Vector3 origin = cardGroup.transform.position;
        float distanceX = CARD_WIDTH - 20;
        float x;

        if (name.Equals("Enemy1"))
            x = origin.x - cardGroup.transform.childCount * distanceX;
        else if (name.Equals("Enemy2"))
            x = origin.x - cardGroup.transform.childCount / 2 * distanceX;
        else
            x = origin.x;

        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);
        hash.Add("islocal", true);

        foreach (Transform tf in cardGroup.transform)
        {
            hash["position"] = new Vector3(x, origin.y);
            iTween.MoveTo(tf.gameObject, hash);
            Card cardShow = tf.GetComponent<Card>();

            if (cardShow != null)
            {
                cardShow.setBackCardActive(false);
            }

            x += distanceX;
        }
    }
    public void hideSampleCard()
    {
        sampleCard.SetActive(false);
        sampleCard1.SetActive(false);
        sampleCard2.SetActive(false);
    }
    public override void Reset()
    {
        txtPoint.text = "";
        moneyText.text = "";
        moneyText.gameObject.transform.localPosition = moneyTextPos;
        immediateWinImage.gameObject.SetActive(false);
        rankImage.enabled = false;
        sampleCard.SetActive(false);
        sampleCard1.SetActive(false);
        sampleCard2.SetActive(false);
        Vector3 deckPosition = new Vector3(-400, -400);
        List<Transform> cards = new List<Transform>();
        foreach (Transform tf in cardGroup.transform)
        {
            cards.Add(tf);
        }
        foreach (Transform tf in cards)
        {
            tf.SetParent(controller.deck.transform);
            tf.localScale = Vector3.one;
            tf.localPosition = deckPosition;
        }
    }

    public void ExitRoom()
    {
        actived = false;
        enemyInfo.SetActive(false);
        sampleCard.SetActive(false);
        avatar.sprite = inviteIcon;
		avatar.gameObject.SetActive (false);
        this.avatarBoder.gameObject.SetActive(false);
        this.avatarBoder_Defaut.gameObject.SetActive(false);
        if (inviteBtn != null)
			inviteBtn.gameObject.SetActive(true);
        displayName.text = "";
        moneyText.text = "";
        gold.text = "";
        sfsId = -1;
    }

}
