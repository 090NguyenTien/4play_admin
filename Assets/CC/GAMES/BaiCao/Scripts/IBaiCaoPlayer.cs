﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IBaiCaoPlayer
{

}

public abstract class BaiCaoBasePlayer : MonoBehaviour, IBaiCaoPlayer
{
    [HideInInspector]
    public int sfsId;
    [HideInInspector]
    public string userId;
    [HideInInspector]
    public string IdBang;
    [HideInInspector]
    public long chip;
    [HideInInspector]
    public long gem;
    [HideInInspector]
    public int level;
    public GameObject cardGroup;
    public Text displayName;
    public Image avatar;
    public Image inviteBtn;
    public Image avatarBoder, avatarBoder_Defaut;
    public Text gold;
    public GameObject timerBackground;
    public string Vip;
    protected const float fillFrom = 0.1f, fillTo = 0.9f;
    public Text txtPoint;
    public Image timerImage;
    public GameObject sampleCard;
    public GameObject sampleCard1;
    public GameObject sampleCard2;
    public Text moneyText;
    public Image rankImage;
    public Image immediateWinImage;
    public BaiCaoRoomController controller;

    [SerializeField]
    protected GameObject focusPlayer = null;
    [SerializeField]
    protected Text txtResult = null;
    public abstract void StopCountDownTime();
    public abstract void SetInfo(string[] info);
    //public abstract void ShowMoneyEffect(long money);
    public Text txtLevel, txtExp;
    public GameObject Level;
    public void setResult(long result)
    {
        txtResult.gameObject.SetActive(true);
        txtResult.text = (result > 0 ? "+" : "") + result.ToString();
        if (result > 0)
        {
            txtResult.color = Ulti.ConvertHexStringToColor("FFF135FF");
            txtResult.GetComponent<Outline>().effectColor = Ulti.ConvertHexStringToColor("FF000080");
        }
        else
        {
            txtResult.color = Ulti.ConvertHexStringToColor("929292FF");
            txtResult.GetComponent<Outline>().effectColor = Ulti.ConvertHexStringToColor("0000003D");
        }
    }
    public void hideResult()
    {
        txtResult.gameObject.SetActive(false);
    }

    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    public abstract void Reset();

    internal void showPoint(int point)
    {
        Debug.LogWarning("Bai Cao point=============" + point);
        txtPoint.gameObject.SetActive(true);
        switch (point)
        {
            case 0: txtPoint.text = "BÙ"; break;
			case 1: txtPoint.text = "1 ĐIỂM"; break;
			case 2: txtPoint.text = "2 ĐIỂM"; break;
			case 3: txtPoint.text = "3 ĐIỂM"; break;
			case 4: txtPoint.text = "4 ĐIỂM"; break;
			case 5: txtPoint.text = "5 ĐIỂM"; break;
			case 6: txtPoint.text = "6 ĐIỂM"; break;
			case 7: txtPoint.text = "7 ĐIỂM"; break;
			case 8: txtPoint.text = "8 ĐIỂM"; break;
			case 9: txtPoint.text = "9 ĐIỂM"; break;
            case 33: txtPoint.text = "BA CÀO"; break;
        }

    }
}
