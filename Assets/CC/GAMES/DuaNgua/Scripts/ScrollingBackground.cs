﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingBackground : MonoBehaviour
{
    bool isRun = false;
    public float Speed = 1;
    public List<Image> sprites = new List<Image>();
    public Direction Dir = Direction.Right;


    private float heightCamera;
    private float widthCamera;

    private Vector3 PositionCam;
    private Camera cam;

    private void Awake()
    {
        //cam = Camera.main;
        cam =  GameObject.FindGameObjectsWithTag("MainCamera")[0].GetComponent<Camera>();
        heightCamera = 2f * cam.orthographicSize;
        widthCamera = heightCamera * cam.aspect;
    }

    void Update()
    {
        if (!isRun) return;
        foreach (var item in sprites)
        {
            if (Dir == Direction.Left)
            {
                if (item.transform.position.x + item.sprite.bounds.size.x / 2 < cam.transform.position.x - widthCamera / 3)
                {
                    Image sprite = sprites[0];
                    foreach (var i in sprites)
                    {
                        if (i.transform.position.x > sprite.transform.position.x)
                            sprite = i;
                    }

                    item.transform.position = new Vector2((sprite.transform.position.x + (sprite.sprite.bounds.size.x / 2) + (item.sprite.bounds.size.x / 2)), sprite.transform.position.y);
                }
            }
            else if (Dir == Direction.Right)
            {
                if (item.transform.position.x - item.sprite.bounds.size.x / 2 > cam.transform.position.x + widthCamera / 3)
                {
                    Image sprite = sprites[0];
                    foreach (var i in sprites)
                    {
                        if (i.transform.position.x < sprite.transform.position.x)
                            sprite = i;
                    }

                    item.transform.position = new Vector2((sprite.transform.position.x - (sprite.sprite.bounds.size.x / 2) - (item.sprite.bounds.size.x / 2)), sprite.transform.position.y);
                }
            }
            else if (Dir == Direction.Down)
            {
                if (item.transform.position.y + item.sprite.bounds.size.y / 2 < cam.transform.position.y - heightCamera / 2)
                {
                    Image sprite = sprites[0];
                    foreach (var i in sprites)
                    {
                        if (i.transform.position.y > sprite.transform.position.y)
                            sprite = i;
                    }

                    item.transform.position = new Vector2(sprite.transform.position.x, (sprite.transform.position.y + (sprite.sprite.bounds.size.y / 2) + (item.sprite.bounds.size.y / 2)));
                }
            }
            else if (Dir == Direction.Up)
            {
                if (item.transform.position.y - item.sprite.bounds.size.y / 2 > cam.transform.position.y + heightCamera / 2)
                {
                    Image sprite = sprites[0];
                    foreach (var i in sprites)
                    {
                        if (i.transform.position.y < sprite.transform.position.y)
                            sprite = i;
                    }

                    item.transform.position = new Vector2(sprite.transform.position.x, (sprite.transform.position.y - (sprite.sprite.bounds.size.y / 2) - (item.sprite.bounds.size.y / 2)));
                }
            }


            if (Dir == Direction.Left)
                item.transform.Translate(new Vector2(Time.deltaTime * Speed * -1, 0));
            else if (Dir == Direction.Right)
                item.transform.Translate(new Vector2(Time.deltaTime * Speed, 0));
            else if (Dir == Direction.Down)
                item.transform.Translate(new Vector2(0, Time.deltaTime * Speed * -1));
            else if (Dir == Direction.Up)
                item.transform.Translate(new Vector2(0, Time.deltaTime * Speed));
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y, 0f);
        }

    }


    public void Show()
    {
        gameObject.SetActive(true);
        StartCoroutine(RunAfterFewSecond());
    }
    public void Hide()
    {
        gameObject.SetActive(false);
        isRun = false;
    }
    public void Stop()
    {
        isRun = false;
    }
    private IEnumerator RunAfterFewSecond()
    {
        yield return new WaitForSeconds(3f);
        isRun = true;
        yield return null;
    }
}
public enum Direction
{
    Up,
    Down,
    Left,
    Right
}
