﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelResultEndRace : MonoBehaviour {
    [SerializeField]
    ListHorseMyBet lstHorseMyBet;
    [SerializeField]
    Text TxtHorseWin, txtKhongCuoc;
    [SerializeField]
    Button BtnClose;
    GamePacket DataBoardOld;// dữ liệu tỉ lệ bàn cược trước đó
    [SerializeField]
    Image imgGoldAnimation;

    public Image ImgGoldAnimation
    {
        get
        {
            return imgGoldAnimation;
        }

        set
        {
            imgGoldAnimation = value;
        }
    }

    // Use this for initialization
    public void Init(GamePacket gp)
    {
        imgGoldAnimation.gameObject.SetActive(false);
        if(txtKhongCuoc) txtKhongCuoc.gameObject.SetActive(false);//panel History khong xai bien txtKhongCuoc nen phai check if
        lstHorseMyBet.Init(gp);
    }
    public void EnableTextKhongCuoc(bool val)
    {
        txtKhongCuoc.gameObject.SetActive(val);
    }
    public ItemResultHorseBet getHorseMyPetBySlot(string slot)
    {
       return lstHorseMyBet.getHorseBetBySlot(slot);
    }
    public void SetHorseBetBySlot(string slot, long chipBet,long chipWin)
    {
        lstHorseMyBet.setHorseBetBySlot(slot,chipBet,chipWin);
    }
    public void ShowHistoryBet(string slot, long chipBet, long chipWin,int index, int ratio)
    {
        lstHorseMyBet.ShowHistoryBet(slot, chipBet, chipWin, index, ratio);
    }
    internal void ShowHorseWin(string slotHorseWin)
    {
        gameObject.SetActive(true);
        TxtHorseWin.text = slotHorseWin;
    }
    internal void AddNewDataBoard(GamePacket gp)
    {
        DataBoardOld = gp;
    }
    public void Hide()
    {
        if(DataBoardOld!=null) lstHorseMyBet.Init(DataBoardOld);
        gameObject.SetActive(false);
    }
    public void ClearData()
    {
        lstHorseMyBet.ClearData();
    }
}
