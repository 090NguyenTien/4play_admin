﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListBetWhenHorseRun : MonoBehaviour {
    [SerializeField]
    List<Text> lstText = new List<Text>(15);
    List<Hashtable> lstHashSlot;
	// Use this for initialization
	public void Init () {
        lstHashSlot = new List<Hashtable>();
        int index=0;
        Hashtable itemHash;
        foreach(Text item in lstText)
        {
            item.text = "";
            item.supportRichText = true;            
            item.gameObject.SetActive(false);
            itemHash = new Hashtable();
            itemHash = new Hashtable();
            itemHash.Add("slot", "");
            itemHash.Add("ratio", 0);
            itemHash.Add("bet", long.Parse("1"));
            lstHashSlot.Add(itemHash);
            index++;
        }        
	}
    public void SetData(string slot,int ratio, long betmoney)
    {
        string money = "";
        for (int i=0;i<lstText.Count;i++)
        {
            if (!lstText[i].gameObject.activeSelf)
            {
                lstText[i].gameObject.SetActive(true);
                lstHashSlot[i]["ratio"] = ratio;
                lstHashSlot[i]["slot"] = slot;
                lstHashSlot[i]["bet"] = betmoney;
                money = Utilities.GetStringMoneyByLongBigSmall(betmoney);
                lstText[i].text = slot + "\n<color=#ff0000ff>x" + lstHashSlot[i]["ratio"]+"</color>" + "\n<color=#ffa500ff>" + money+ "</color>";
                break;
            }else
            {
                Hashtable itemHash = getItemSlotBySlot(slot);
                if (itemHash!=null)
                {
                    long bet = (long)lstHashSlot[i]["bet"] + betmoney;
                    lstHashSlot[i]["bet"] = bet;
                    money = Utilities.GetStringMoneyByLongBigSmall(bet);
                    lstText[i].text = slot + "\n<color=#ff0000ff>x" + lstHashSlot[i]["ratio"]+ "</color>" + "\n<color=#ffa500ff>" + money+ "</color>";
                    break;
                }
            }
        }
    }
    public Hashtable getItemSlotBySlot(string slot)
    {
        foreach (Hashtable item in lstHashSlot)
        {
            if (item["slot"].ToString() == slot)
            {
                return item;
            }
        }
        return null;
    }
}
