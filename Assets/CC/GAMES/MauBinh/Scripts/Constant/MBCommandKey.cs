﻿using UnityEngine;
using System.Collections;

namespace ConstMB {
		
	public class MBCommandKey {

		public const string GET_ROOM_INFO = "103";
		public const string PUSH_JOIN_GAME = "104";
		public const string START_GAME = "105";
		public const string MOVE_CARD = "106";
		public const string FINISH_MOVE = "107";
		public const string UNFINISH_MOVE = "108";
		public const string FINISH_GAME = "15";


	}
}