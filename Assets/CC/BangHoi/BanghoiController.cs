﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BanghoiController : MonoBehaviour
{
    [SerializeField]
    Toggle InfoBang, Thanhvien, Choduyet, Cauhinh, LeaveBangHoi, CuuTro;
    [SerializeField]
    GameObject ViewInfo, ViewChoDuyet, ViewThanhvien, ViewCauHinh;
    int curToggle = 0;
    // Start is called before the first frame update
    void Start()
    {
        InfoBang.onValueChanged.AddListener(isToggleChange);
        Thanhvien.onValueChanged.AddListener(isToggleChange);
        Choduyet.onValueChanged.AddListener(isToggleChange);
        Cauhinh.onValueChanged.AddListener(isToggleChange);
        LeaveBangHoi.onValueChanged.AddListener(isToggleChange);
        CuuTro.onValueChanged.AddListener(isToggleChange);

        VisibleView(true, false, false, false);
        ViewInfo.GetComponent<InfoBangHoiController>().Show();
    }
    void VisibleView(bool info, bool thanhvien, bool choduyet, bool cauhinh)
    {
        ViewInfo.SetActive(info);
        ViewThanhvien.SetActive(thanhvien);
        ViewChoDuyet.SetActive(choduyet);
        ViewCauHinh.SetActive(cauhinh);
    }
    public void isToggleChange(bool val)
    {
        if (val == true)
        {
            if (InfoBang.isOn && curToggle!=0)
            {
                curToggle = 0;
                ViewInfo.GetComponent<InfoBangHoiController>().Show();
                VisibleView(true, false, false, false);
            }
            else if (Thanhvien.isOn && curToggle != 1)
            {
                curToggle = 1;
                ViewThanhvien.GetComponent<ThanhVienController>().Show();
                VisibleView(false, true, false, false);
            }
            else if (Choduyet.isOn && curToggle != 2)
            {
                curToggle = 2;
                ViewChoDuyet.GetComponent<ChoDuyetController>().Show();
                VisibleView(false, false, true, false);
            }
            else if (Cauhinh.isOn && curToggle != 3)
            {
                curToggle = 3;
                ViewCauHinh.GetComponent<CauHinhBang>().Show();
                VisibleView(false, false, false, true);
            }
            else if (CuuTro.isOn && curToggle != 4)
            {
                curToggle = 4;
                ViewThanhvien.GetComponent<ThanhVienController>().ShowCuuTro();
                VisibleView(false, true, false, false);
            }
            else if (LeaveBangHoi.isOn)
            {
                AlertController.api.showAlert("Bạn Có Chắc Muốn Rời Khỏi Bang?", ()=> {
                    GamePacket gp = new GamePacket(CommandKey.REQUEST_LEAVE_CLAN);
                    gp.Put("clanid", MyInfo.CLAN_ID);
                    SFS.Instance.SendRoomBangHoiRequest(gp);
                },true);
                
            }            
        }
    }

    public void OnChatResponse(GamePacket param)
    {
        ViewInfo.GetComponent<InfoBangHoiController>().OnChatResponse(param);
        ViewThanhvien.GetComponent<ThanhVienController>().OnChatResponse(param);
        ViewChoDuyet.GetComponent<ChoDuyetController>().OnChatResponse(param);

        switch (param.cmd)
        {
            case CommandKey.REQUEST_LEAVE_CLAN:
                LeaveClanRes(param);
                break;
            case CommandKey.KICK_MEMBER_BANG_HOI:
                KickMemberRes(param);
                break;
            case CommandKey.SET_UP_CLAN:
                SetupClanRes(param);
                break;
        }
    }

    private void SetupClanRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Cấu Hình Bang Hội Thành Công!");
        }
        else
        {
            AlertController.api.showAlert("Cấu Hình Bang Hội Không Thành Công!");
        }
        
    }

    private void KickMemberRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string owner = param.GetString("owner");
            string memberId = param.GetString("memberid");

            if (memberId == MyInfo.ID)
            {
                CloseBangHoi();
            }            
        }
        else
        {
            AlertController.api.showAlert("Kick Thành Viên Thất Bại");
        }
    }

    private void LeaveClanRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberId = param.GetString("memberid");
            if (memberId == MyInfo.ID)
            {
                AlertController.api.showAlert("Rời Bang Thành Công!", LeaveClanComplete);
                MyInfo.BangChatRoomName = "";
            }            
        }
    }

    private void LeaveClanComplete()
    {
        Destroy(gameObject);
    }
    public void CloseBangHoi()
    {
        Destroy(gameObject);
    }
}
