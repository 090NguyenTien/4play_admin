﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThanhVienController : MonoBehaviour
{
    [SerializeField]
    GameObject container, ActionList, HelpMember, BgMad;
    [SerializeField]
    ItemThanhVienBangHoiController ItemThanhVienBangHoi;
    [SerializeField]
    Button BtnNangCap, BtnGiangCap, BtnCloseMad, BtnKickMember, BtnHelpMember;
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    InputField TxtChipCuuTro;
    [SerializeField]
    Text TxtNganKho;
    string owner, deputy;
    string curUserIdClick;
    List<ItemThanhVienBangHoiController> LstThanhVien;
    int typeShowMember;//0 là danh sach thanh vien - 1 là danh sách cứu trợ
    [SerializeField]
    Image BgAlpha;
    // Start is called before the first frame update
    void Start()
    {        
        

    }
    public void Show()
    {
        typeShowMember = 0;
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
        Mad.gameObject.SetActive(false);
        BtnCloseMad.gameObject.SetActive(false);
        BgMad.gameObject.SetActive(false);

        HelpMember.SetActive(false);
        for (int z = 0; z < container.transform.childCount; z++)
        {
            Destroy(container.transform.GetChild(z).gameObject);
        }
        LstThanhVien = new List<ItemThanhVienBangHoiController>();
        GamePacket gp = new GamePacket(CommandKey.GET_LIST_THANH_VIEN);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void ShowCuuTro()
    {
        typeShowMember = 1;
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
        Mad.gameObject.SetActive(false);
        BtnCloseMad.gameObject.SetActive(false);
        BgMad.gameObject.SetActive(false);

        HelpMember.SetActive(false);
        for (int z = 0; z < container.transform.childCount; z++)
        {
            Destroy(container.transform.GetChild(z).gameObject);
        }
        LstThanhVien = new List<ItemThanhVienBangHoiController>();
        GamePacket gp = new GamePacket(CommandKey.GET_LIST_THANH_VIEN_CUU_TRO);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.GET_LIST_THANH_VIEN:
                GetListThanhVienRes(param);
                break;
            case CommandKey.GET_LIST_THANH_VIEN_CUU_TRO:
                GetListThanhVienCuuTroRes(param);
                break;
            case CommandKey.REJECT_CUU_TRO:
                RejectCuuTroRes(param);
                break;
            case CommandKey.UPGRADE_DEPUTY:
                UpGradeDeputyRes(param);
                break;
            case CommandKey.DOWN_GRADE_DEPUTY:
                DownGradeDeputyRes(param);
                break;
            case CommandKey.KICK_MEMBER_BANG_HOI:
               KickMemberRes(param);
                break;
            case CommandKey.HELP_MEMBER_BANGHOI:
               HelpMemberRes(param);
                break;
        }
    }
    private void RejectCuuTroRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            //to do something
            string memberId = param.GetString("memberid");
            foreach (ItemThanhVienBangHoiController item in LstThanhVien)
            {
                if(memberId == item.GetUserId())
                {
                    Destroy(item.gameObject);
                    return;
                }
            }
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void GetListThanhVienCuuTroRes(GamePacket param)
    {
        ISFSArray members = param.GetSFSArray("requesthelps");
        deputy = "";
        if (param.ContainKey("deputy"))
        {
            deputy = param.GetString("deputy");
        }
        owner = param.GetString("owner");
        foreach (ISFSObject member in members)
        {
            ItemThanhVienBangHoiController item = Instantiate(ItemThanhVienBangHoi, container.transform) as ItemThanhVienBangHoiController;
            item.InitInfo(member, owner, deputy);
            item.ShowBtnTuChoiCuuTro();
            item.AddCallBack(OnClickMenuAction);
            LstThanhVien.Add(item);
        }

    }

    private void HelpMemberRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string owner = param.GetString("owner");
            string memberId = param.GetString("memberid");
            if (owner == MyInfo.ID)
            {
                AlertController.api.showAlert("Cứu Trợ Thành Công!");
                InfoBangHoiController.chip = param.GetLong("chip");
                TxtNganKho.text = param.GetLong("chip").ToString();
            }                
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }

    private void KickMemberRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string owner = param.GetString("owner");
            string memberId = param.GetString("memberid");
            if (owner == MyInfo.ID || deputy == MyInfo.ID)
            {
                AlertController.api.showAlert("Kick Thành Viên Thành Công");
                if(typeShowMember==0) Show();
            }
        }        
        
    }
    private void UpGradeDeputyRes(GamePacket param)
    {        
        int status = param.GetInt("status");
        string memberid = param.GetString("memberid");
        string owner = param.GetString("owner");
        if (status == 1)
        {
             memberid = param.GetString("memberid");
             owner = param.GetString("owner");
            if(owner == MyInfo.ID)
            {
                AlertController.api.showAlert("Thăng Chức Thành Công");
                if (typeShowMember == 0) Show();
            }
            else if (memberid == MyInfo.ID)
            {
                AlertController.api.showAlert("Bạn Đã Được Thăng Cấp Thành Bang Phó!");
                if (typeShowMember == 0) Show();
            }            
        }
        else
        {
            AlertController.api.showAlert("Thăng Chức Thất Bại");
        }
    }
    private void DownGradeDeputyRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberid = param.GetString("memberid");
            string owner = param.GetString("owner");
            if (owner == MyInfo.ID)
            {
                AlertController.api.showAlert("Giáng Chức Thành Công");
                if (typeShowMember == 0) Show();
            }
            else if (memberid == MyInfo.ID)
            {
                AlertController.api.showAlert("Bạn Bị Được Giáng Cấp Từ Bang Chủ!");
                if (typeShowMember == 0) Show();
            }
        }
        else
        {
            AlertController.api.showAlert("Giáng Chức Thất Bại");
        }
    }
    private void GetListThanhVienRes(GamePacket param)
    {
        ISFSArray members = param.GetSFSArray("members");
        deputy = "";
        if (param.ContainKey("deputy"))
        {
            deputy = param.GetString("deputy");
        }
        owner = param.GetString("owner");
        foreach (ISFSObject member in members)
        {
            ItemThanhVienBangHoiController item = Instantiate(ItemThanhVienBangHoi, container.transform) as ItemThanhVienBangHoiController;
            item.InitInfo(member, owner, deputy);
            item.AddCallBack(OnClickMenuAction);
        }
       
    }

    private void OnClickMenuAction(string user_id)
    {
        if(owner == MyInfo.ID)//Bang Chủ Click
        {
            if (user_id == owner)
            {
                BtnNangCap.gameObject.SetActive(false);
                BtnGiangCap.gameObject.SetActive(false);
                BtnKickMember.gameObject.SetActive(false);
                BtnHelpMember.gameObject.SetActive(true);
            }
            else if (user_id == deputy)
            {
                BtnNangCap.gameObject.SetActive(false);
                BtnGiangCap.gameObject.SetActive(true);
                BtnKickMember.gameObject.SetActive(true);
                BtnHelpMember.gameObject.SetActive(true);
            }
            else
            {
                BtnNangCap.gameObject.SetActive(true);
                BtnGiangCap.gameObject.SetActive(false);
                BtnKickMember.gameObject.SetActive(true);
                BtnHelpMember.gameObject.SetActive(true);
            }
        }else if (deputy == MyInfo.ID)//Bang Phó Click
        {
            if (user_id == owner)
            {
                BtnNangCap.gameObject.SetActive(false);
                BtnGiangCap.gameObject.SetActive(false);
                BtnKickMember.gameObject.SetActive(false);
                BtnHelpMember.gameObject.SetActive(false);
            }
            else if (user_id == deputy)
            {
                BtnNangCap.gameObject.SetActive(false);
                BtnGiangCap.gameObject.SetActive(false);
                BtnKickMember.gameObject.SetActive(false);
                BtnHelpMember.gameObject.SetActive(false);
            }
            else
            {
                BtnNangCap.gameObject.SetActive(false);
                BtnGiangCap.gameObject.SetActive(false);
                BtnKickMember.gameObject.SetActive(true);
                BtnHelpMember.gameObject.SetActive(false);
            }
        }
        else//Thanh Viên Click
        {
            BtnNangCap.gameObject.SetActive(false);
            BtnGiangCap.gameObject.SetActive(false);
            BtnKickMember.gameObject.SetActive(false);
            BtnHelpMember.gameObject.SetActive(false);
        }
        if (typeShowMember == 1)//cuu tro Menu
        {
            BtnNangCap.gameObject.SetActive(false);
            BtnGiangCap.gameObject.SetActive(false);
            BtnKickMember.gameObject.SetActive(false);
        }
        ActionList.SetActive(ActionList.activeSelf?false:true);
        BgAlpha.gameObject.SetActive(BgAlpha.gameObject.activeSelf ? false : true);
        curUserIdClick = user_id;
    }
    public void CloseActionList()
    {
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
    }
    public void NangCap()
    {
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
        GamePacket gp = new GamePacket(CommandKey.UPGRADE_DEPUTY);
        gp.Put("memberid", curUserIdClick);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void GiangCap()
    {
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
        GamePacket gp = new GamePacket(CommandKey.DOWN_GRADE_DEPUTY);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void XemTaiSan()
    {
        BtnCloseMad.gameObject.SetActive(true);
        BgMad.gameObject.SetActive(true);
        Mad.gameObject.SetActive(true);
        Mad.XemTaiSan(curUserIdClick);        
    }
    public void CloseTaiSan()
    {
        Mad.gameObject.SetActive(false);
        BtnCloseMad.gameObject.SetActive(false);
        BgMad.gameObject.SetActive(false);
    }
    public void KickMember()
    {
        ActionList.SetActive(false);
        BgAlpha.gameObject.SetActive(false);
        GamePacket gp = new GamePacket(CommandKey.KICK_MEMBER_BANG_HOI);
        gp.Put("memberid", curUserIdClick);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void ShowHelpMember()
    {
        HelpMember.SetActive(true);
        TxtNganKho.text = InfoBangHoiController.chip.ToString();
    }
    public void HideHelpMember()
    {
        HelpMember.SetActive(false);
    }
    public void CuuTroClick()
    {
        if(TxtChipCuuTro.text=="" || TxtChipCuuTro.text == "0")
        {
            AlertController.api.showAlert("Bạn Phải Nhập Số Tiền Muốn Cứu Trợ!");
            return;
        }else if (long.Parse(TxtChipCuuTro.text) < InfoBangHoiController.min_help)
        {
            AlertController.api.showAlert("Số Tiền Muốn Cứu Trợ Phải Lớn Hơn "+ InfoBangHoiController.min_help);
            return;
        }
        else if (long.Parse(TxtChipCuuTro.text) > InfoBangHoiController.max_help)
        {
            AlertController.api.showAlert("Số Tiền Muốn Cứu Trợ Phải Nhỏ Hơn " + InfoBangHoiController.max_help);
            return;
        }
        GamePacket gp = new GamePacket(CommandKey.HELP_MEMBER_BANGHOI);
        gp.Put("memberid", curUserIdClick);
        gp.Put("chip", long.Parse(TxtChipCuuTro.text));
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }

}
