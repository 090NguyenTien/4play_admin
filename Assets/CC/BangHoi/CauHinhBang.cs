﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CauHinhBang : MonoBehaviour
{
    [SerializeField]
    Dropdown DropDownVip;
    [SerializeField]
    Toggle ToggleDuyet;
    int valueDropdown;
    // Start is called before the first frame update
    void Start()
    {
        DropDownVip.onValueChanged.AddListener(delegate {
            myDropdownValueChangedHandler(DropDownVip);
        });
        //ToggleDuyet.onValueChanged.AddListener(isToggleChange);
    }
    //public void isToggleChange(bool val)
    //{
    //    if (val == true)
    //    {
    //        if (ToggleDuyet.isOn)
    //        {

    //        }
    //    }
    //}
    void Destroy()
    {
        DropDownVip.onValueChanged.RemoveAllListeners();
    }

    private void myDropdownValueChangedHandler(Dropdown target)
    {
        valueDropdown = target.value;
    }
    public void SetDropdownIndex(int index)
    {
        DropDownVip.value = index;
    }
    public void Show()
    {
        if (InfoBangHoiController.is_public == 0)
        {
            ToggleDuyet.isOn = true;
        }
        else
        {
            ToggleDuyet.isOn = false;
        }
        SetDropdownIndex(InfoBangHoiController.min_vip);
    }
    public void DongY()
    {
        GamePacket gp = new GamePacket(CommandKey.SET_UP_CLAN);
        gp.Put("is_public", ToggleDuyet.isOn?0:1);
        gp.Put("min_vip", DropDownVip.value);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            //case CommandKey.REQUEST_LEAVE_CLAN:
                //LeaveClanRes(param);
                //break;
        }
    }

    
}
