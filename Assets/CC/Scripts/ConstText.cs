﻿using System.Collections.Generic;

public class ConstText
{
	static Dictionary<string, string> dict = new Dictionary<string, string>();
	public static string GetText(string _key)
	{
		return dict [_key];
	}
	public static void SetText(string _key, string _value)
	{
		if (dict.ContainsKey (_key))
			dict [_key] = _value;
		else
			dict.Add (_key, _value);
	}

    public const string RoomIsPlaying = "Bàn đang chơi, vui lòng chọn bàn khác";
    public const string Username = "Tên đăng nhập";
	public const string Password = "Mật khẩu";
	public const string SignIn = "Đăng nhập";
	public const string SignUp = "Đăng ký";
	public const string FogetPassword = "Quên mật khẩu?";

	public const string Win = "Thắng";
	public const string Lose = "Thua";
	public const string Create_Room = "Tạo bàn";
	public const string JoinRoom = "Vào bàn";
	public const string ErrorRoomFull = "Bàn chơi đã đầy";
	public const string ErrorRoomNotExist = "Bàn chơi không tồn tại";
	public const string ErrorCantFindRoom = "Không tìm thấy bàn chơi phù hợp";
	public const string Quick_Game = "Chơi ngay";
	public const string RoomID = "Số bàn";
	public const string Bet = "Mức cược";
	public const string Players = "Số người";
	public const string Poor = "Nông dân";
	public const string Rich = "Địa chủ";
	public const string Vip = "VIP";
	public const string EnterRoomId = "Vào bàn số...";
	public const string MauBinh = "Mậu Binh";
	public const string TaiXiu = "Tài Xỉu";
	public const string Phom = "Phỏm";
	public const string XiTo = "Xì Tố";
	public const string TLMN = "TLMN";
	public const string Notify = "Thông báo";
	public const string ErrorNoMoneyToPlay = "Số chip không đủ. Vui lòng nạp thêm!";
	public const string Close = "Đóng";
	public const string InviteFriend = "Mời bạn";
    public const string NotifyExitBaiCao = "Bạn thực sự muốn thoát game?";
    public const string NotifyExitTLMN = "Ván đánh chưa kết thúc. Thoát bàn sẽ bị xử thua Bét (tính thúi heo và hàng) hoặc thua Cóng";
	public const string NotifyExitPhom = "Ván đánh chưa kết thúc. Thoát bàn sẽ bị xử thua \"Cháy!\"";
	public const string NotifyExitMB = "Ván đánh chưa kết thúc. Thoát bàn sẽ bị trừ 6 chi mỗi nhà!";
	public const string NotifyExitXT = "Ván đánh chưa kết thúc. Thoát bàn sẽ coi như \"Úp bài\"";
	public const string Emotion = "Biểu tượng";
	public const string QuickChat = "Chat nhanh";
	public const string Send = "Gửi";
	public const string Fold = "Úp bài";
	public const string Check = "Nhường";
	public const string Call = "Theo";
//	public const string RaiseUp = "Tăng tố";
	public const string Raise = "Tố";
	public const string RaiseMulti2 = "x2 Tố";
	public const string RaisePart4 = "Tố 1/4";
	public const string RaisePart2 = "Tố 1/2";
	public const string RaiseAll = "Tố tất cả";

	public const string WaitingNewMatch = "Chờ bắt đầu ván mới";
	public const string MatchShowing = "Bàn đang chơi, vui lòng chờ giây lát";
	public const string Sorted = "Xếp xong";
	public const string SortAgain = "Xếp lại";

	public const string ErrorConnection = "Mất kết nối do bạn không tương tác trong game.";
	public const string ErrorConnectionLow = "Vui lòng kiểm tra kết nối mạng.";

	public const string Server_Was_Maintenence = "Không kết nối được với Sever. Vui lòng kiểm tra kết nối mạng của bạn.";

    public const string PasswordNotRight = "Mật khẩu không chính xác";
	public const string PasswordWrong = "Đăng nhập hoặc mật khẩu không chính xác";
	public const string PasswordConfirmWrong = "Xác thực mật khẩu không chính xác";
	public const string PasswordEmpty = "Chưa nhập mật khẩu";
	public const string UsernameEmpty = "Chưa nhập tài khoản";
	public const string UsernameSpecial = "Tài khoản không bao gồm ký tự đặc biệt";
	public const string UsernameExist = "Tài khoản đã tồn tại";
	public const string LimitDevice = "Thiết bị này không thể đăng ký thêm tài khoản.";
	public const string SignUpSuccess = "Đăng ký thành công";

	public const string InvitedRoomId = "Mời bạn vào bàn chơi";


}
