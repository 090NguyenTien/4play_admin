﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using BaseCallBack;

public class EstateControll : MonoBehaviour {

    [SerializeField]
    GameObject BtnController, PanelScroll, ScrollContent, imgAddShop_1, imgAddShop_2, imgAddShop_3, imgAddShop_4, imgAddShop_5, imgAddCar_1, imgAddCar_2, MadButtonControl;
    [SerializeField]
    MadControll Mad;
    public GameObject PanelBuyItem, PanelWorking, ObjKhungAva, ObjKhungAva_df, PaneUpdate;
    public bool View = false;
    [SerializeField]
    Image Avatar, KhungAva;

    [SerializeField]
    Text TxtTen, TxtVip, TxtChip, TxtGem, TxtPoint, TxtTongDoanhThu;
    [SerializeField]
    TestItemInHome PaneBuy;
    //[SerializeField]
    //UpdateItem PaneUpdate;
   // public long TongDoanhThu;




    // public Dictionary<string, int> DuLieuSoHuu = new Dictionary<string, int>();


    public void InitLEFT()
    {
        TxtTen.text = MyInfo.NAME;
        long chip = MyInfo.CHIP;
        long gem = MyInfo.GEM;

        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        TxtPoint.text = DataHelper.MY_DiemTaiSan.ToString();
        TxtTongDoanhThu.text = Utilities.GetStringMoneyByLong(MyInfo.DOANHTHU);
        TxtVip.text = MyInfo.GetVipName();


        Avatar.sprite = MyInfo.sprAvatar;

        
        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar_Shop(MyInfo.BorderAvatarName);
            if (h != null)
            {
                KhungAva.sprite = DataHelper.GetBoderAvatar_Shop(MyInfo.BorderAvatarName);
                ObjKhungAva.SetActive(true);
                ObjKhungAva_df.SetActive(false);
            }
            else
            {
                ObjKhungAva.SetActive(false);
                ObjKhungAva_df.SetActive(true);
            }

        }
        else
        {
            ObjKhungAva.SetActive(false);
            ObjKhungAva_df.SetActive(true);
        }

    }



    public void CapNhatLEFT()
    {
        long chip = MyInfo.CHIP;
        long gem = MyInfo.GEM;

        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        TxtPoint.text = DataHelper.MY_DiemTaiSan.ToString();
        TxtTongDoanhThu.text = Utilities.GetStringMoneyByLong(MyInfo.DOANHTHU);
    }




    public void RequestLayDuLieuVatPham()
    {
        API.Instance.RequestLayDuLieuVatPham(RspLayDuLieuVatPham);
        
    }




    public void RequestMuaVatPham(string Id, string LoaiTien)
    {
       // Debug.LogError("sl kho truoc khi goi ===== " + DataHelper.KhoItem[Id]  + "===========Id=========" + Id);
        API.Instance.RequestMuaVatPham(Id, LoaiTien, RspLayMuaVatPham);
    }

    void RspLayMuaVatPham(string _json)
    {
        Debug.LogWarning("DU LIEU mua SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string idTONG = node["asset_id"]["$id"].Value;
        string id = node["user_asset_id"]["$id"].Value;

        if (sta == "1")
        {
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;






            ThemDuLieuSoHuu(id, idTONG, 1);






            //if (DataHelper.DuLieuSoHuu.ContainsKey(idTONG))
            //{

            //    DataHelper.DuLieuSoHuu[idTONG] = DataHelper.DuLieuSoHuu[idTONG] + 1;
            //    Debug.LogError("sl kho truoc mua ===== " + DataHelper.KhoItem[idTONG] + "===========Id=========" + idTONG);
            //    DataHelper.KhoItem[idTONG] = DataHelper.KhoItem[idTONG];
            //    Debug.LogWarning("Da Them vao du lieu so huu CU");
            //}
            //else
            //{

            //    DataHelper.DuLieuSoHuu.Add(idTONG, 1);

            //    //DataHelper.KhoItem.Add(id, 1);

            //    if (DataHelper.KhoItem.ContainsKey(idTONG))
            //    {
            //        DataHelper.KhoItem[idTONG] = 1;
            //    }
            //    else
            //    {
            //        DataHelper.KhoItem.Add(idTONG, 1);
            //    }
                
            //    Debug.LogWarning("Da Them moi du lieu so huu");
            //}

            //Debug.LogError("sl kho sau mua ===== " + DataHelper.KhoItem[idTONG]);

            //int DiemCongThem = DataHelper.DuLieuDiemTaiSan[idTONG];
            //DataHelper.MY_DiemTaiSan += DiemCongThem;
            CapNhatLEFT();

            if (DataHelper.DuLieuNha.ContainsKey(idTONG))
            {
                //Mad.ChangeHome(idTONG, id);
                PanelScroll.SetActive(true);
                PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
            }
            else if (DataHelper.DuLieuShop.ContainsKey(idTONG))
            {
                //Mad.ChangeShop(idTONG, id);
                PanelScroll.SetActive(true);
                PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
            }
            else if (DataHelper.DuLieuXe.ContainsKey(idTONG))
            {
                //Mad.ChangeCar(idTONG, id);
                PanelScroll.SetActive(true);
                PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
            }
             
            
            PaneBuy.gameObject.SetActive(false);
        }
        else
        {
            string MSG = node["msg"].Value;
            PaneBuy.ThongBaoLoi(MSG);
        }

        Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + idTONG);
    }


    public void RequestLayDuLieuVatPham_SoHuu()
    {
        API.Instance.RequestVatPham_SoHuu(RspLayDuLieuVatPham_SoHuu);

    }

    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);

        //_json = "false";

        if (_json == "false")
        {
            HienThiMadMacDinh();
            DataHelper.MY_DiemTaiSan = 0;
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);

            JSONNode data = node["assets"];
            //data = null;
            if (data == null || data.Count == 0)
            {
                HienThiMadMacDinh();
                DataHelper.MY_DiemTaiSan = 0;
            }
            else
            {

                for (int i = 0; i < data.Count; i++)
                {
                    string id = data[i]["id"]["$id"].Value;

                    string id_tong = data[i]["asset_id"]["$id"].Value;

                    int level = int.Parse(data[i]["level"].Value);


                    ThemDuLieuSoHuu(id, id_tong, level);
                }

                DataHelper.KhoItem = DataHelper.DuLieuSoHuu;
                DataHelper.DuLieuMap.Clear();

                if (node["placed_assets"].Value == "null")
                {
                    HienThiMadMacDinh();
                   // Debug.LogError("vO NULL NE ---------- ");
                }
                else
                {
                    int cou = node["placed_assets"].Count;
                    // Debug.LogError("BAT DC so luong NE HEHE---------- " + node["placed_assets"]);
                    MyInfo.DOANHTHU = 0;
                    for (int i = 0; i < cou; i++)
                    {
                        int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                        string a_id = node["placed_assets"][i]["id"].Value;
                        string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                        long a_level = long.Parse(node["placed_assets"][i]["level"].Value);
                        //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                        // int sl = DataHelper.KhoItem[a_id];

                        if (DataHelper.DuLieuSoHuu_V2.ContainsKey(a_id))
                        {
                            My_Item_SoHuu it = new My_Item_SoHuu();

                            it = DataHelper.DuLieuSoHuu_V2[a_id];

                            it.InMad = true;
                            string type = it.type;

                            Item_InMad itInMad = new Item_InMad();
                            itInMad.id = a_id;
                            itInMad.idTong = a_id_tong;
                            itInMad.Level = a_level;

                            long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                            itInMad.DoanhThu = doanhthu;
                            MyInfo.DOANHTHU += doanhthu;
                            DataHelper.DuLieuMap[pos] = itInMad;

                          //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                            // int sl = DataHelper.KhoItem[a_id];
                            //sl--;
                            //DataHelper.KhoItem[a_id] = sl;
                        }
                        else
                        {
                            Item_InMad itInMad = new Item_InMad();
                            itInMad.id = "";
                            itInMad.idTong = "";
                            itInMad.Level = 0;
                            itInMad.DoanhThu = 0;
                            DataHelper.DuLieuMap[pos] = itInMad;
                            API.Instance.RequestDatVatPhamTrenMad("", pos, "", "", RspDatVatPhamTrenMad);
                        }



                    }

                    for (int i = 0; i < 8; i++)
                    {
                        if (DataHelper.DuLieuMap.ContainsKey(i) == false)
                        {
                            Item_InMad itInMad = new Item_InMad();
                            itInMad.id = "";
                            itInMad.idTong = "";

                            itInMad.Level = 0;
                            itInMad.DoanhThu = 0;
                            DataHelper.DuLieuMap[i] = itInMad;

                            //DataHelper.DuLieuMap.Add(i, "");
                        }

                        //   Debug.LogError("ket vi tri ----- " + i  + " ---------- " + DataHelper.DuLieuMap[i]);
                    }

                }

            }

        }
        //  Mad.InitSpr();
        DataHelper.IsInitShopVatPham = true;
    }




    void RspDatVatPhamTrenMad(string _json)
    {
        Debug.LogWarning("DU LIEU Dat Vat Pham: " + _json);
        JSONNode node = JSONNode.Parse(_json);

        MyInfo.DOANHTHU = 0;
        string sta = node["status"].Value;
        Debug.LogWarning("DU LIEU Dat Vat Pham  STA: " + sta);
        if (sta == "1")
        {
            JSONNode data = node["placed_assets"];
            Debug.LogWarning("DU LIEU Dat Vat Pham  placed_assets: " + data);


            int cou = data.Count;

            for (int i = 0; i < cou; i++)
            {
                int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                string a_id = node["placed_assets"][i]["id"].Value;
                string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                long a_level = long.Parse(node["placed_assets"][i]["level"].Value);

                Item_InMad itInMad = new Item_InMad();
                itInMad.id = a_id;
                itInMad.idTong = a_id_tong;
                itInMad.Level = a_level;

                string type = "house";

                if (pos > 0 && pos < 6)
                {
                    type = "shop";
                }
                else if (pos > 5 && pos < 8)
                {
                    type = "car";
                }

                long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                itInMad.DoanhThu = doanhthu;

                MyInfo.DOANHTHU += doanhthu;


                // Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                DataHelper.DuLieuMap[pos] = itInMad;
            }

        }
        else
        {
            string smg = node["msg"].Value;
            Debug.LogWarning("DU LIEU Dat Vat Pham loiiiiiiiii: " + smg);
        }
    }



    void HienThiMadMacDinh()
    {
        Item_InMad itInMad_Home = new Item_InMad();
        itInMad_Home.id = "5dc03420e52b8e2d238b6045";
        itInMad_Home.idTong = "5dc03420e52b8e2d238b6045";
        itInMad_Home.Level = 0;
        itInMad_Home.DoanhThu = 0;

        DataHelper.DuLieuMap.Add(0, itInMad_Home);

        for (int i = 1; i < 8; i++)
        {

            Item_InMad itInMad = new Item_InMad();
            itInMad.id = "";
            itInMad.idTong = "";
            itInMad.Level = 0;
            itInMad.DoanhThu = 0;


            DataHelper.DuLieuMap.Add(i, itInMad);
        }
    }

    void ThemDuLieuSoHuu(string id, string idTong, int level)
    {
        string key = id;
        My_Item_SoHuu it = new My_Item_SoHuu();
        it.idTong = idTong;
        it.Level = level;
        it.InMad = false;
        string type = CheckType(idTong);
        it.type = type;
        if (DataHelper.DuLieuSoHuu_V2.ContainsKey(key))
        {           
            DataHelper.DuLieuSoHuu_V2[key] = it;
         //   Debug.LogWarning("Da Them vao du lieu so huu CU");
        }
        else
        {
            
            DataHelper.DuLieuSoHuu_V2.Add(key, it);
          //  Debug.LogWarning("Da Them moi du lieu so huu");
        }

        

        if (DataHelper.DanhSach_LoaiVatPham_SoHuu.ContainsKey(idTong))
        {
            DataHelper.DanhSach_LoaiVatPham_SoHuu[idTong] = idTong;
            //   Debug.LogWarning("Da Them vao du lieu so huu CU");
        }
        else
        {

            DataHelper.DanhSach_LoaiVatPham_SoHuu.Add(idTong, idTong);
            //  Debug.LogWarning("Da Them moi du lieu so huu");
        }

        


        int diemVatPham = DataHelper.DuLieuDiemTaiSan[idTong];
        //int DiemCongThem = diemVatPham * sl;
        DataHelper.MY_DiemTaiSan += diemVatPham;
    }


    string CheckType(string idTong)
    {
        string Type = "";

        foreach (var item in DataHelper.DuLieuNha)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "house";
                return Type;
            }

        }

        foreach (var item in DataHelper.DuLieuShop)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "shop";
                return Type;
            }

        }

        foreach (var item in DataHelper.DuLieuXe)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "car";
                return Type;
            }

        }

        return Type;
    }




    void RspLayDuLieuVatPham(string _json)
    {
        if (DataHelper.IsInitItemShop == false)
        {
            

            JSONNode node = JSONNode.Parse(_json);

            Debug.LogWarning("DU LIEU SAN PHAM: " + _json);


            JSONNode data = node["data"];

            Debug.Log(data.Count + " - " + data.ToString());


            //   http://gamebai.gamekt.club/files/assets/thumb_5dc034d15be66_h1.png
            for (int i = 0; i < data.Count; i++)
            {
                //Debug.LogError("iiii" + i);
                //for (int i = 0; i < 5; i++)        {

                string type = data[i]["type"].Value;
                string name = data[i]["name"].Value;

                string id = data[i]["id"].Value;

                string image = data[i]["image"].Value;


                long chip = long.Parse(data[i]["price"]["chip"].Value);
                long gem = long.Parse(data[i]["price"]["gem"].Value);

                long chip_save = long.Parse(data[i]["price"]["sale_chip"].Value);
                long gem_save = long.Parse(data[i]["price"]["sale_gem"].Value);
                long revenueChip = long.Parse(data[i]["revenue_chip"].Value);

                string info = data[i]["info"].Value;
                int point = int.Parse(data[i]["asset_point"].Value);
                int stt = int.Parse(data[i]["stt"].Value);
                //long mydoc_chip = long.Parse(data[i]["myDoc"]["price"]["chip"].Value);

                DataHelper.DuLieuTenImgTheoID.Add(id, image);

                DataHelper.DuLieuDiemTaiSan.Add(id, point);

                if (type == "house")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                    //it.myDoc_chip = mydoc_chip;
                    long thunhap = GetDoanhThuByGiaBan(chip, 1);
                    it.Revenue_chip = thunhap;
                    DataHelper.DuLieuNha.Add(id, it);

                 //   API.Instance.Load_SprHouse(image, id);
                }
                else if (type == "shop")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                    //  it.myDoc_chip = mydoc_chip;
                    long thunhap = GetDoanhThuByGiaBan(chip, 1);
                    it.Revenue_chip = thunhap;
                    DataHelper.DuLieuShop.Add(id, it);

                 //   API.Instance.Load_SprShop(image, id);
                }
                else if (type == "car")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                    //  it.myDoc_chip = mydoc_chip;

                    long giaban = gem * 5000000;
                    long thunhap = GetDoanhThuByGiaBan(giaban, 1);

                    it.Revenue_chip = thunhap;
                    DataHelper.DuLieuXe.Add(id, it);
                  //  API.Instance.Load_SprCar(image, id);
                }

            }

            RequestLayDuLieuVatPham_SoHuu();
            DataHelper.IsInitItemShop = true;
        }
        
    }

    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }


    long GetDoanhThuByIDTong(string idTong,long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    public void ffff()
    {
        Debug.LogWarning("SO NHA--------------- " + DataHelper.DuLieuNha.Count);
        Debug.LogWarning("SO shop--------------- " + DataHelper.DuLieuShop.Count);
        Debug.LogWarning("SO xe--------------- " + DataHelper.DuLieuXe.Count);
    }

    public void BtnClick(int type)
    {
        Debug.Log("DangClick");
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        if (type == 1)
        {
            PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
        }
    }

    public void Btn_Shop_1_Click(int type)
    {
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
        Mad.ID_ShopToChange = type;      
    }

    public void Btn_Car_Click(int type)
    {
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
        Mad.ID_CarToChange = type;
    }


    public void CloseItemScroll()
    {
        PanelScroll.SetActive(false);
        OpenBtnController();
    }
    public void OpenItemScroll()
    {
        PanelScroll.SetActive(true);
    }

    public void OpenBtnController()
    {
        BtnController.SetActive(true);
    }

    public void CloseBtnController()
    {
        BtnController.SetActive(false);
    }

    public void ClosePanelBuyItem()
    {
        PanelBuyItem.SetActive(false);
    }

    public void OpenPanelBuyItem()
    {
        PanelBuyItem.SetActive(true);
    }

    public void OpenPanelWorking()
    {
        PanelWorking.SetActive(true);
    }

    public void ClosePanelWorking()
    {
        PanelWorking.SetActive(false);
    }

    public void IsVisitors()
    {
        imgAddShop_1.SetActive(false);
        imgAddShop_2.SetActive(false);
        imgAddShop_3.SetActive(false);
        imgAddShop_4.SetActive(false);
        imgAddShop_5.SetActive(false);

        imgAddCar_1.SetActive(false);
        imgAddCar_2.SetActive(false);

        MadButtonControl.SetActive(false);
    }

    public void BtnBackOnClick()
    {
        Mad.BackAndSave();
        this.gameObject.SetActive(false);
    }
}




public class My_Item : MonoBehaviour
{

    public string ten, thongtin, id, img, soluong;
    public long chip, chipsave, gem, gemsave, myDoc_chip, Revenue_chip, DiamonPrice_NangCap, ChipPrice_NangCap, DoanhThuSauKhiNangCap = 500000;
    public int diemtaisan;
}

public class My_Item_SoHuu : MonoBehaviour
{

    public string ten, thongtin, idTong, img, type;
    public long chip, chipsave, gem, gemsave, myDoc_chip, Revenue_chip, Level;
    public int diemtaisan;
    public bool InMad;
}

public class Item_InMad : MonoBehaviour
{

    public string id, idTong, img;
    public long  Level, DoanhThu;
  
}
