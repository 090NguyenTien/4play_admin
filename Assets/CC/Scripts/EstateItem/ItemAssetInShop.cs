﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAssetInShop : MonoBehaviour
{
    [SerializeField]
    Image ImgItem, ImgUse;
    [SerializeField]
    GameObject ImgClock, _DiamonPrice, _ChipPrice;
    [SerializeField]
    Text TxtNameItem, TxtDiamonPrice, TxtChipPrice, TxtCount, TxtPoint, TxtDoanhThu;

    [SerializeField]
    TestItemInHome PanelBuy;

    public long LongDiamonPrice, LongChipPrice, Save_Chip, Save_Gem, doanhthu;

    public string Ten, ThongTin, Id, Img, str_doanhthu;

    public int DiemTaiSan, stt, soluong;



    public bool IsClock = true;
    public string MyId = "";
    Button MyButton;
    public Sprite MySpr = null;





    public void Init(string Id, string loaiItem, bool isSale = false, long PercentSale = 0)
    {
        My_Item it = new My_Item();
        
        if (loaiItem == "nha")
        {
            it = DataHelper.DuLieuNha[Id];
           // ImgItem.sprite = DataHelper.dictSprite_House[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, ImgItem);


       
            ThongTin = it.thongtin;
            XuLyChuoiThongTin();

            long giaban = it.chip;


            doanhthu = GetDoanhThuByGiaBan(giaban, 1);
        }
        else if (loaiItem == "shop")
        {
            it = DataHelper.DuLieuShop[Id];
          //  ImgItem.sprite = DataHelper.dictSprite_Shop[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, ImgItem);
            ThongTin = it.thongtin;
            long giaban = it.chip;


            doanhthu = GetDoanhThuByGiaBan(giaban, 1);
        }
        else if (loaiItem == "xe")
        {
            it = DataHelper.DuLieuXe[Id];
            //  ImgItem.sprite = DataHelper.dictSprite_Car[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, ImgItem);
            ThongTin = it.thongtin;
            long giaban = it.gem * 5000000;


            doanhthu = GetDoanhThuByGiaBan(giaban, 1);
        }
        

        MyId = Id;
        MySpr = ImgItem.sprite;


        
        str_doanhthu = Utilities.GetStringMoneyByLong(doanhthu);
        TxtDoanhThu.text = str_doanhthu;

       TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;

        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;
        

        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);
        if (LongChipPrice == 0)
        {

            _ChipPrice.SetActive(false);
        }
        else
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
            _ChipPrice.SetActive(true);
        }
        _DiamonPrice.SetActive(true);
        ImgClock.SetActive(false);


         
       
        GameObject Shop = GameObject.Find("ShopBorderAvatar");  
        ShopBorderAvatar plShop = Shop.GetComponent<ShopBorderAvatar>();  
        GameObject objPanelBuy = plShop.PanelBuy; 
        PanelBuy = objPanelBuy.GetComponent<TestItemInHome>();
           MyButton.onClick.RemoveAllListeners();
          MyButton.onClick.AddListener(BtnClockClick);
       
    }


    long GetDoanhThuByIDTong(string idTong, long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }



    public void XuLyChuoiThongTin()
    {
        ThongTin = ThongTin.Remove(0, 3);

        int i = ThongTin.IndexOf('<');
        ThongTin = ThongTin.Remove(i, 7);
        ThongTin = ThongTin.Insert(i, "\n");

        int u = ThongTin.IndexOf('<');
        int c = ThongTin.Length - 1;

        if (c - u > 3)
        {
            ThongTin = ThongTin.Remove(u, 7);
            ThongTin = ThongTin.Insert(u, "\n");

            int t = ThongTin.IndexOf('<');
            int s = ThongTin.Length;
            ThongTin = ThongTin.Remove(t, s - t);
        }
        else
        {
            ThongTin = ThongTin.Remove(u, 4);
        }


    }






    public void BtnClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.InitItemInShop(MyId, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan, MySpr, str_doanhthu);
        PanelBuy.Show();
    }
    
}
