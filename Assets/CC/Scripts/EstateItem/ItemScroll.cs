﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScroll : MonoBehaviour {
    [SerializeField]
    GameObject Content, ItemHome, ItemShop, ItemCar;
    [SerializeField]
    MadControll Mad;
    Dictionary<string, Sprite> ListSpriteItemHome;
    Dictionary<string, Sprite> ListSpriteItemShop;
    Dictionary<string, Sprite> ListSpriteItemCar;
    Dictionary<string, GameObject> DicHomeItem;
    Dictionary<string, GameObject> DicShopItem;
    Dictionary<string, GameObject> DicCarItem;

    [SerializeField]
    EstateControll Estate;

    public void InitContent(float left, float right)
    {
        //Content.GetComponent<RectTransform>().
    }



    //public void UpdateKhoItem()
    //{
    //    for (int i = 0; i < 8; i++)
    //    {
    //        if (DataHelper.DuLieuMap[i] != null)
    //        {
    //            //string id = DataHelper.DuLieuMap[i];
    //            if (DataHelper.KhoItem[id] != null)
    //            {
    //                DataHelper.KhoItem[id] = DataHelper.KhoItem[id] - 1;
    //            }
               
    //        }

    //    }
    //}


    public void InitItemHomeCroll()
    {
        
        
        
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }

        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        
        DicHomeItem = new Dictionary<string, GameObject>();


        // Hien Vat Pham so Huu
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            My_Item_SoHuu it = item.Value;
            if (it.type == "house")
            {
                InitItemHome(it.idTong, it.Level.ToString(), item.Key, false);
               
            }
        }

        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuNha)
        {
            if (item.Key != "5dc03420e52b8e2d238b6045")
            {
                InitItemHome(item.Key, "0");
            }

        }


        //foreach (var item in DataHelper.DuLieuNha)
        //{
        //    if (item.Key != "5dc03420e52b8e2d238b6045")
        //    {
        //        if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
        //        {
        //            Debug.LogError("DataHelper.KhoItem[Id]==check=== " + DataHelper.KhoItem[item.Key]);
        //            InitItemHome(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
        //        }
        //        else
        //        {
        //            InitItemHome(item.Key, "0");
        //        }
        //    }
                        
        //}

        
    }


    public void InitItemShopInScoll()
    {
        ListSpriteItemShop = DataHelper.dictSprite_Shop;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }

        DicShopItem = new Dictionary<string, GameObject>();

        //   UpdateKhoItem();

        //foreach (var item in DataHelper.DuLieuShop)
        //{
        //    if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
        //    {
        //        InitItemShop(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
        //    }
        //    else
        //    {
        //        InitItemShop(item.Key, "0");
        //    }

        //}



        // Hien Vat Pham so Huu
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            My_Item_SoHuu it = item.Value;
            if (it.type == "shop")
            {
                // InitItemHome(it.idTong, it.Level.ToString(), item.Key, false);
                InitItemShop(it.idTong, it.Level.ToString(), item.Key, false);
            }
        }

        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuShop)
        {
            //if (item.Key != "5dc03420e52b8e2d238b6045")
            //{
            //    InitItemHome(item.Key, "0");
            //}
            InitItemShop(item.Key, "0");
        }





    }



    public void InitItemCarInScoll()
    {
        ListSpriteItemCar = DataHelper.dictSprite_Car;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        DicCarItem = new Dictionary<string, GameObject>();

        //foreach (var item in DataHelper.DuLieuXe)
        //{
        //    if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
        //    {
        //        InitItemCar(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
        //    }
        //    else
        //    {
        //        InitItemCar(item.Key, "0");
        //    }
        //}




        // Hien Vat Pham so Huu
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            My_Item_SoHuu it = item.Value;
            if (it.type == "car")
            {
                // InitItemHome(it.idTong, it.Level.ToString(), item.Key, false);
                InitItemCar(it.idTong, it.Level.ToString(), item.Key, false);
            }
        }

        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuXe)
        {
            //if (item.Key != "5dc03420e52b8e2d238b6045")
            //{
            //    InitItemHome(item.Key, "0");
            //}
            InitItemCar(item.Key, "0");
        }




    }










    public void InitItemHome(string IdTong, string level, string Id = "", bool clock = true)
    {
        GameObject Obj = Instantiate(ItemHome) as GameObject;
        // Obj.transform.SetParent(Content.transform);
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
 
        ItemShopHome MyItem = Obj.GetComponent<ItemShopHome>();


        My_Item it = DataHelper.DuLieuNha[IdTong];



     //   Sprite spr = DataHelper.dictSprite_House[Id];


        MyItem.Init(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicHomeItem.Add(IdTong, Obj);
        }
        else
        {
            DicHomeItem.Add(Id, Obj);
        }
       
        
    }

    public void InitItemShop(string IdTong, string level, string Id = "", bool clock = true )
    {
        GameObject Obj = Instantiate(ItemShop) as GameObject;

        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

       
        My_Item it = DataHelper.DuLieuShop[IdTong];
        

       // Sprite hinh = ListSpriteItemShop[Id];
        string ten = it.ten;
        long gem = it.gem;
        long chip = it.chip;
        int diem = it.diemtaisan;
        string thongtin = it.thongtin;


        MyItem.Init(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicShopItem.Add(IdTong, Obj);
        }
        else
        {
            DicShopItem.Add(Id, Obj);
        }

        
       
    }

    public void InitItemCar(string IdTong, string level, string Id = "", bool clock = true)
    {     
        GameObject Obj = Instantiate(ItemCar) as GameObject;
        // Obj.transform.SetParent(Content.transform);
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

        
        My_Item it = DataHelper.DuLieuXe[IdTong];

        MyItem.InitCar(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicCarItem.Add(IdTong, Obj);
        }
        else
        {
            DicCarItem.Add(Id, Obj);
        }



        
    }

}
