﻿using BaseCallBack;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;


namespace UnityPurchasinghelp
{
public class UnityPurchasingHelper : MonoBehaviour, IStoreListener
    {
        private bool isInitInApp;
        private bool isClickItem;
        private string curProductIdClick;
    private onCallBackStringStringBool callback;
    public bool isCallService = false;
#if UNITY_EDITOR
        public string strTestIAP = "";
#endif
        public bool IsInited
		{
			get
			{
				return this.IsInitialized();
            }
        }

        private static UnityPurchasingHelper instance;

        public static UnityPurchasingHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject container = new GameObject("UnityPurchasingHelper");
                    instance = container.AddComponent<UnityPurchasingHelper>();

                }
                return instance;

            }
        }

        void Awake()
        {
            DontDestroyOnLoad(this);
            IsInitialized();
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            //Debug.Log("================showNativeInAppBuy111111");
            this.controller = controller;
            this.extensions = extensions;

            isInitInApp = true;            
            LoadingManager.Instance.ENABLE = false;
            if (isClickItem == true)
            {
                StartCoroutine(ShowNativeInAppBuy());                
            }

        }
        private IEnumerator ShowNativeInAppBuy()
        {
            //Debug.Log("================showNativeInAppBuy222222");
            yield return new WaitForSeconds(0.5f);
            OnPurchaseClicked(curProductIdClick, callback);
        }
        public void CallServiceGetProductInAppId()
        {
            if (isCallService == false)
            {
                LoadingManager.Instance.ENABLE = true;
                API.Instance.RequestGetProductInAppId(GetProductInAppIdSuccess);
            }else
            {
                //đã call rồi ko call nữa
            }
        }

        private void GetProductInAppIdSuccess(string _json)
        {
            LoadingManager.Instance.ENABLE = false;
            if (this.IsInitialized())
            {
                return;
            }
            JSONNode node = JSONNode.Parse(_json);
            Debug.Log(" GetProductInAppIdSuccess==== " + _json);
            //if (node["status"].AsInt == 1)
            //{
                //JSONArray data = node["data"].AsArray;
                ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance(), new IPurchasingModule[0]);
                List<string> lstProductID = new List<string>();
                foreach(JSONArray arrayItem in node.Childs)
                {
                    foreach (JSONNode item in arrayItem)
                    {
                        lstProductID.Add(item.Value);
                    }
                    
                }

                foreach (string value in lstProductID)
                {//quan trong dong nay add tat ca product_id vao
                    configurationBuilder.AddProduct(value, ProductType.Consumable);
                }
                UnityPurchasing.Initialize(this, configurationBuilder);
                isCallService = true;
            //}

            
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError("OnInitializeFailed=====" + error);
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            Debug.LogError("OnPurchaseFailed=====" + i);
            //LoadingHelper.Get("UnityPurchasing").StopLoading();
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {

            bool validPurchase = true; // Presume valid for platforms with no R.V.

            // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
            // Prepare the validator with the secrets we prepared in the Editor
            // obfuscation window.
           
            
#endif

            //if (validPurchase)
            // {
            //xu ly cong tien,..... o day mua thanh cong roi nek
            // }
            return PurchaseProcessingResult.Complete;
        }


        public ProductMetadata GetProductMetadata(string id)
        {
            try
            {
                Product product = controller.products.all.ToList<Product>().Find((Product e) => e.definition.id.Equals(id));
                if (product != null)
                {
                    return product.metadata;
                }
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log(ex.Message + "     " + id );
            }
            return null;
        }
        
        //PurchasingPackage a co the thay bang product_id
        public void OnPurchaseClicked(string productID, onCallBackStringStringBool _cb)
		{
            //Debug.Log("================showNativeInAppBuy000000"+ isInitInApp);
            if (!isInitInApp)
            {
                LoadingManager.Instance.ENABLE = true;
                curProductIdClick = productID;
                callback = null;
                callback = _cb;
                isClickItem = true;
                return;
            }

            callback = null;
            callback = _cb;


            if (this.controller == null)
            {
                callback("","", false);
                return;
            }
            if (this.controller.products == null)
            {
            callback("", "", false);
            return;
            }
            if (this.controller.products.all == null)
            {
            callback("", "", false);
            return;
            }
            Product product = this.controller.products.all.ToList<Product>().Find((Product e) => productID.Equals(e.definition.id));
            if (product != null)
            {
                //LoadingHelper.Get("UnityPurchasing").ShowLoading(null, null, string.Empty); //e dang show loading o day

                this.controller.InitiatePurchase(product);
            }
        }


        private void onAddNewProductSuccess()
        {

        }
        private bool IsInitialized()
        {
            return this.controller != null && this.extensions != null;
        }

        public void OnInitializeFailed(InitializationFailureReason error, string message)
        {
            throw new NotImplementedException();
        }

        private IStoreController controller;

        private IExtensionProvider extensions;

	}
}
