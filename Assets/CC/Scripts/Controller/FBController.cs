﻿
using MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FBController : MonoBehaviour
{
    public static FBController api;
    private void Awake()
    {
        if (api != null)
        {
            Destroy(gameObject);
            return;
        }
        
        DontDestroyOnLoad(this);
        api = this;
    }

    private Action actionCompleteLoginFb = null;
    public void loginWithFbAccount(Action actionCompleteLoginFb)
    {
        
    }

    private void InitCallback()
    {
      
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }


}
