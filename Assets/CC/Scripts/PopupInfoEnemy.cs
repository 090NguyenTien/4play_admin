﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
using UnityEngine.SceneManagement;
using SimpleJSON;

public class PopupInfoEnemy : MonoBehaviour
{
    [SerializeField]
    GameObject BtnKetBan, TxtStatusFriend;
    [SerializeField]
    Image Avatar, KhungAvatar;
    [SerializeField]
    Text TxtTen, TxtLoaiVip, TxtLevel, TxtChip, TxtGem;
    [SerializeField]
    List<Sprite> ListSprBoom;
    [SerializeField]
    Transform ChuaItemsBoom;
    int Enemy;
    onCallBackInt onClick;
    [SerializeField]
    GameObject Popup;
    [SerializeField]
    ControllerMB.MBInGameController MauBinhControll;
    [SerializeField]
    ControllerXT.XTInGameController XiToControll;

    [SerializeField]
    TLMNRoomController TienLenControll;
    public static PopupInfoEnemy Instance { get; set; }

    int sfsId_MB;
    string userId;

    public string MadUser = "1,2,3,4,5,6,3,2";
    string IdBangUserBiNem = "";
    string IdBangMaster = "";
    string IdBangCuaMinh = "";
    [SerializeField]
    MadControll Mad;
    int VipUser;
    // Use this for initialization
    public void Init(string _userId, Sprite _ava, Sprite _KhungAva, string _ten, string _vip, string _level, string _chip, string _gem, int _enemy, string idBang, int sfsid = -2)
    {
        Debug.Log("Vo Toi Init");
        if (MyInfo.CHAT_STATUS==0)
        {
            BtnKetBan.SetActive(false);
        }else
        {
            BtnKetBan.SetActive(true);
        }
        this.userId = _userId;
        if (_ava != null)
        {
            Avatar.sprite = _ava;
        }

        if (_KhungAva != null)
        {
            KhungAvatar.sprite = _KhungAva;
        }
        TxtTen.text = _ten;
        TxtLoaiVip.text = "Vip " + _vip;
        TxtLevel.text = "Level " + _level;
        TxtChip.text = _chip;
        TxtGem.text = _gem;

        Enemy = _enemy;
        IdBangCuaMinh = MyInfo.ID_BANG;
        sfsId_MB = sfsid;

        //LayThongTinBangUser_BiNem(_userId);
        IdBangMaster = MyInfo.ID_BANG_MASTER;


        IdBangUserBiNem = idBang;

        int MyVip = MyInfo.MY_ID_VIP;
        VipUser = int.Parse(_vip);
        for (int i = 0; i < ChuaItemsBoom.childCount; i++)
        {

            // ChuaItemsBoom.GetChild(i).GetComponent<Button>().onClick.AddListener(ItemBoomOnClick);
            int VipUnClock = DataHelper.DicItemBoom[i];
            GameObject Boom = ChuaItemsBoom.GetChild(i).gameObject;
            if (MyVip >= VipUnClock)
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
            }
            else
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomClockOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
                Boom.GetComponent<Image>().color = Color.gray;
                Text TxtVip = Boom.transform.GetChild(0).gameObject.GetComponent<Text>();
                TxtVip.text = "Vip " + VipUnClock.ToString();
                TxtVip.gameObject.SetActive(true);
            }


        }

        Popup.SetActive(true);
        //Check đã là bạn chưa
        CheckIsFriend();
        API.Instance.RequestVatPham_SoHuu_Enemy(_userId, RspLayDuLieuVatPham_SoHuu);

    }




    public void LayThongTinBangUser_BiNem(string idUser_BiNem)
    {
        //  API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangHoi_user_1);
        API.Instance.RequestLayIdBangHoi(idUser_BiNem, RspLayIdBangHoi_BiNem);
    }






    void RspLayIdBangHoi_BiNem(string _json)
    {

        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

        JSONNode node = JSONNode.Parse(_json);

        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

        Debug.LogWarning("usserBang------------- " + userBang);
        Debug.LogWarning("BangMaster------------- " + BangMaster);

        IdBangMaster = BangMaster;
        IdBangUserBiNem = userBang;

    }



    bool CheckPhanDame()
    {

        if (IdBangUserBiNem == "")
        {
            return false;
        }

        if (IdBangUserBiNem == IdBangCuaMinh)
        {
            return false;
        }

        if (IdBangUserBiNem != IdBangMaster)
        {
            return false;
        }

        return true;
    }





    public void KickUser()
    {
        Debug.LogError("KickUser===" + MyInfo.SFS_ID + " =====sfsidBekick" + sfsId_MB);
        if(MyInfo.MY_ID_VIP<= VipUser)
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip Lớn Hơn Người Chơi Bạn Muốn Kick!");
        }
        else
        {
            GamePacket param = new GamePacket(CommandKey.KICK_USER_BY_HOST);
            param.Put(ParamKey.USER_ID, sfsId_MB);
            SFS.Instance.SendRoomRequest(param);
        }        
        ClosePopup();
    }
    void CheckIsFriend()
    {

    }
    public void AddFriend()
    {
        BtnKetBan.SetActive(false);
        TxtStatusFriend.GetComponent<Text>().text = "Đang Chờ Đồng Ý";
        SFS.Instance.RequestAddFriend(userId);
    }
    Sprite GetSprite(int _index)
    {
        return ListSprBoom[_index];
    }


    public void ItemBoomClockOnClick(int id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        // Popup.SetActive(false);
    }

    //public void ItemBoomOnClick(int id)
    //{

    //    SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

    //    Popup.SetActive(false);


    //    if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
    //    {

    //        TienLenControll.NemBoomControll(0, Enemy, id);
    //    }
    //    else if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
    //    {
    //        PhomRoomController.Instance.NemBoomControll(0, Enemy, id);
    //    }
    //    else if (SceneManager.GetActiveScene().name.Equals(GameScene.MBScene.ToString()))
    //    {
    //        MauBinhControll.NemBoomControll(0, Enemy, id, sfsId_MB);
    //    }
    //    else if (SceneManager.GetActiveScene().name.Equals(GameScene.XTScene.ToString()))
    //    {
    //        XiToControll.NemBoomControll(0, Enemy, id);
    //    }
    //    else if (SceneManager.GetActiveScene().name.Equals(GameScene.BaiCaoScence.ToString()))
    //    {
    //        BaiCaoRoomController.Instance.NemBoomControll(0, Enemy, id);
    //    }



    //}



    public void ItemBoomOnClick(int id)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);


        if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
        {
            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                TienLenControll.PhanDame(Enemy, id);
            }
            else
            {
                // TienLenControll.NemBoomControll(0, Enemy, id);
                TienLenControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
            }

        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
        {
            //  PhomRoomController.Instance.NemBoomControll(0, Enemy, id);


            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                PhomRoomController.Instance.PhanDame(Enemy, id);
            }
            else
            {
                // TienLenControll.NemBoomControll(0, Enemy, id);
                PhomRoomController.Instance.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
            }


        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.MBScene.ToString()))
        {
            // MauBinhControll.NemBoomControll(0, Enemy, id, sfsId_MB);

            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                MauBinhControll.PhanDame(Enemy, id);
            }
            else
            {
                // TienLenControll.NemBoomControll(0, Enemy, id);
                MauBinhControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
            }




        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.XTScene.ToString()))
        {
            // XiToControll.NemBoomControll(0, Enemy, id);
            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                XiToControll.PhanDame(Enemy, id);
            }
            else
            {
                // TienLenControll.NemBoomControll(0, Enemy, id);
                XiToControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
            }


        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.BaiCaoScence.ToString()))
        {
            // BaiCaoRoomController.Instance.NemBoomControll(0, Enemy, id);

            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                BaiCaoRoomController.Instance.PhanDame(Enemy, id);
            }
            else
            {
                // TienLenControll.NemBoomControll(0, Enemy, id);
                BaiCaoRoomController.Instance.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
            }



        }

        IdBangUserBiNem = "";

    }




    public void ClosePopup()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);
    }




    public void RequestLayDuLieuVatPham_SoHuu()
    {
        

    }


    long GetDoanhThuByIDTong(string idTong, long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }



    void HienThiMadMacDinh_Enemy()
    {
        Item_InMad itInMad_Home = new Item_InMad();
        itInMad_Home.id = "5dc03420e52b8e2d238b6045";
        itInMad_Home.idTong = "5dc03420e52b8e2d238b6045";
        itInMad_Home.Level = 0;
        itInMad_Home.DoanhThu = 0;

        DataHelper.DuLieuMap_Enemy.Add(0, itInMad_Home);

        for (int i = 1; i < 8; i++)
        {

            Item_InMad itInMad = new Item_InMad();
            itInMad.id = "";
            itInMad.idTong = "";
            itInMad.Level = 0;
            itInMad.DoanhThu = 0;


            DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
        }
    }




    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);
        DataHelper.DuLieuMap_Enemy.Clear();
        if (_json == "false")
        {
            //  Debug.LogError("DU LIEU SAN PHAM so huu------------------------:");
            //DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
            //for (int i = 1; i < 8; i++)
            //{
            //    DataHelper.DuLieuMap_Enemy.Add(i, "");
            //}
            HienThiMadMacDinh_Enemy();
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);

            //   Debug.LogWarning("000000000000000000000000v       " + node["placed_assets"].Value);
            // string dataMap = node["placed_assets"].Value;
            if (node["placed_assets"].Value == "null")
            {

                //DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
                //for (int i = 1; i < 8; i++)
                //{
                //    DataHelper.DuLieuMap_Enemy.Add(i, "");
                //}
                HienThiMadMacDinh_Enemy();
                //   Debug.LogError("BAT DC NE HEHE---------- ");
            }
            else
            {
                int cou = node["placed_assets"].Count;
                // Debug.LogError("BAT DC so luong NE HEHE---------- " + cou);

                for (int i = 0; i < cou; i++)
                {
                    int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                    string a_id = node["placed_assets"][i]["id"].Value;
                    string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                    long a_level = long.Parse(node["placed_assets"][i]["level"].Value);

                    Item_InMad itInMad = new Item_InMad();
                    itInMad.id = a_id;
                    itInMad.idTong = a_id_tong;
                    itInMad.Level = a_level;

                    string type = "house";

                    if (pos > 0 && pos < 6)
                    {
                        type = "shop";
                    }
                    else if (pos > 5 && pos < 8)
                    {
                        type = "car";
                    }

                    long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                    itInMad.DoanhThu = doanhthu;


                    DataHelper.DuLieuMap_Enemy[pos] = itInMad;
                    //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                    // DataHelper.DuLieuMap_Enemy[pos] = a_id;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (DataHelper.DuLieuMap_Enemy.ContainsKey(i) == false)
                    {
                        Item_InMad itInMad = new Item_InMad();
                        itInMad.id = "";
                        itInMad.idTong = "";
                        //if (i == 0)
                        //{
                        //    itInMad.id = "5dc03420e52b8e2d238b6045";
                        //    itInMad.idTong = "5dc03420e52b8e2d238b6045";
                        //}
                        itInMad.Level = 0;
                        itInMad.DoanhThu = 0;

                        DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
                    }

                    //   Debug.LogError("ket vi tri ----- " + i  + " ---------- " + DataHelper.DuLieuMap[i]);
                }

            }
        }


        Mad.XemNha();

    }


}
