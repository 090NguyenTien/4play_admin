﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using BaseCallBack;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class ShopBorderAvatar : MonoBehaviour {

    [SerializeField]
    GameObject ItemBuyBorAvatar, ContentAvatar, ItemNha, ItemShop, ItemXe, ContentNha, ContentShop, ContentXe, CrollKhung, CrollNha, CrollShop, CrollXe;
    [SerializeField]
    GameObject BtnKhung, BtnNha, BtnShop, BtnXe;
    [SerializeField]
    Text TxtChip, TxtGem, TxtNameUser;

    [SerializeField]
    HomeControllerV2 Home;
    [SerializeField]
    LobbyView _LobbyView;
    [SerializeField]
    Image Avatar, Border;
    [SerializeField]
    public GameObject PanelBuy;

    private static ShopBorderAvatar instance;

    [SerializeField]
    PopupAlertManager popupAlert;
    [SerializeField]
    GameObject PanelThongBaoMuaVatPhamThanhCong;
    [SerializeField]
    Text TxtMuaThanhCong;

    public static ShopBorderAvatar Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject obj = new GameObject("ShopBorderAvatar");
                instance = obj.AddComponent<ShopBorderAvatar>();
            }
            return instance;
        }
    }


    

void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);

        instance = this;

        DontDestroyOnLoad(gameObject);

        popupAlert.Init();
        popupAlert.gameObject.SetActive(false);

        InitTop();
    }


    public void InitTop()
    {
        Avatar.sprite = DataHelper.GetAvatar(MyInfo.AvatarName);
        if (DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName) != null)
        {
            Border.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
        }        
        TxtNameUser.text = MyInfo.NAME;
        UpDatePrice();
    }






    public void UpDatePrice()
    {
        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
    }




    public void InitPanelBuyBorAvatar()
    {
        Dictionary<string, BoderAvatar> DicBor = DataHelper.DicDataBoderAvatar;
        //   Debug.LogError("DicBor.Count;------------ " + DicBor.Count);

        
        int count = ContentAvatar.transform.childCount;

      //  Debug.LogError("count;------------ " + count);
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject obj = ContentAvatar.transform.GetChild(i).gameObject;
                Destroy(obj);
            }
        }

        int BorCount = MyInfo.MY_BORDERS_AVATAR.Count;
        List<int> ListBors = MyInfo.MY_BORDERS_AVATAR;

        for (int i = 1; i < DicBor.Count; i++)
        {
            bool t = CheckSoHuu(i);
            if (t == true)
            {

                string key = i.ToString();
                //  Debug.LogError("DicBor[i.ToString()].id ------------ " + DataHelper.DicDataBoderAvatar[key]);
                GameObject obj = Instantiate(ItemBuyBorAvatar) as GameObject;
                obj.transform.SetParent(ContentAvatar.transform);
                obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                ItemBuyBorderAvatar item = obj.GetComponent<ItemBuyBorderAvatar>();
                string id = DicBor[i.ToString()].id;
                string vip = DicBor[i.ToString()].vip_require;
                string chip = DicBor[i.ToString()].chip;
                string gem = DicBor[i.ToString()].gem;

                Sprite spr = DataHelper.dictSpriteBoderAvt_Shop[id];
                //  Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " +gem + "  Hinh " + spr);
                if (i > 7)
                {
                    // Debug.LogError("mua gem");
                    //   Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " + gem + "  Hinh " + spr);
                    item.Init(id, vip, gem, spr, true);
                }
                else
                {
                    //  Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " + gem + "  Hinh " + spr);
                    item.Init(id, vip, chip, spr);
                }


            }
        }
        
    }


    public void BtnKhungAvataClick()
    {
        BtnKhung.transform.GetChild(0).gameObject.SetActive(true);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);

        CrollKhung.SetActive(true);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        CrollNha.SetActive(false);
    }


    public void ThongBaoMuaThanhCong()
    {
        TxtMuaThanhCong.text = "Giao dịch thành công.";
        PanelThongBaoMuaVatPhamThanhCong.SetActive(true);
    }

    public void Close_ThongBaoMuaThanhCong()
    {
        PanelThongBaoMuaVatPhamThanhCong.SetActive(false);
    }


    public void InitScroll_Nha()
    {
        int cn = ContentNha.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentNha.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        foreach (var item in DataHelper.DuLieuNha)
        {

            if (item.Key != "5dc03420e52b8e2d238b6045")
            {
                GameObject Obj = Instantiate(ItemNha) as GameObject;
                // Obj.transform.SetParent(Content.transform);
                Obj.GetComponent<RectTransform>().SetParent(ContentNha.GetComponent<RectTransform>().transform);
                Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();
                string keyID = item.Key;
                MyItem.Init(keyID, "nha");
            }

        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        CrollNha.SetActive(true);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(true);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void InitScroll_Shop()
    {
        int cn = ContentShop.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentShop.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }

        foreach (var item in DataHelper.DuLieuShop)
        {


            GameObject Obj = Instantiate(ItemShop) as GameObject;
            // Obj.transform.SetParent(Content.transform);
            Obj.GetComponent<RectTransform>().SetParent(ContentShop.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();




            string keyID = item.Key;


            MyItem.Init(keyID, "shop");
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(true);
        CrollXe.SetActive(false);
        CrollNha.SetActive(false);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(true);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
    }




    public void InitScroll_Xe()
    {
        int cn = ContentXe.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentXe.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }

        foreach (var item in DataHelper.DuLieuXe)
        {


            GameObject Obj = Instantiate(ItemXe) as GameObject;
            // Obj.transform.SetParent(Content.transform);
            Obj.GetComponent<RectTransform>().SetParent(ContentXe.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();




            string keyID = item.Key;


            MyItem.Init(keyID, "xe");
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(true);
        CrollNha.SetActive(false);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(true);
    }




    bool CheckSoHuu(int id)
    {
        int BorCount = MyInfo.MY_BORDERS_AVATAR.Count;
        List<int> ListBors = MyInfo.MY_BORDERS_AVATAR;

        for (int i = 0; i < BorCount; i++)
        {
            if (id == ListBors[i])
            {
                return false;
            }
        }

        return true;
    }


    public void MuaKhungAvartar(int id_Border)
    {
        
        API.Instance.RequestBuyAvatarBorder(id_Border, RspBuyBorder);
    }

    void RspBuyBorder(string _json)
    {
        int Id_Vip = int.Parse(Id_Vip_Can);
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log("node Mua Border   " + node);
        Debug.Log("status Mua Border   " + node["status"].Value);
        int status = node["status"].AsInt;
        if (status ==  -1)
        {
            popupAlert.Show("Bạn đã sở hữu khung Avatar này!", popupAlert.Hide);
           // Debug.LogError("avatar đã sở hữu");
        }
        else if (status == -2)
        {
            popupAlert.Show("Khung Avatar không tồn tại!", popupAlert.Hide);
            Debug.Log("avatar không tồn tại");
        }
        else if (status == -3)
        {
            popupAlert.Show("Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!", popupAlert.Hide);
            Debug.Log("ko đủ mức vip mua avatar này (ở đây sẽ có thêm param vip_require chỉ mức vip cần để mua cho client hiện thông báo)");
        }
        else if (status == -4)
        {
            popupAlert.Show("Không đủ CHIP!", popupAlert.Hide);
            Debug.Log("không đủ chip để mua avatar này");
        }
        else if (status == -5)
        {
            popupAlert.Show("Không đủ GEM!", popupAlert.Hide);
            Debug.Log("không đủ gem để mua avatar này");
        }
        else if (status == 1)
        {
            MyInfo.MY_NEW_AVARTAR_BORDER = node["new_avatar_border"].Value;
            MyInfo.MY_BORDERS_AVATAR.Clear();
            for (int i = 0; i < node["avatar_border"].Count; i++)
            {
                MyInfo.MY_BORDERS_AVATAR.Add(node["avatar_border"][i].AsInt);
            }

            MyInfo.CHIP = long.Parse(node["chip"].Value);
            MyInfo.GEM = long.Parse(node["gem"].Value);

            UpDatePrice();

            Scene my_Scene = SceneManager.GetActiveScene();

            if (my_Scene.name == "HomeSceneV2")
            {
                Home.UpdateInfoUser();
            }
            else if (my_Scene.name == "WaitingRoom")
            {
                _LobbyView.UpdateChipUser();
                _LobbyView.UpdateGemUser();
            }

            
            DongPaneMua();

            popupAlert.Show("Giao dịch thành công!", popupAlert.Hide);
            Debug.Log("So Chip Con lai: " + node["chip"].Value);
            Debug.Log("So Gem Con lai: " + node["gem"].Value);
        }
        
    }

    [SerializeField]
    public Image ImgBor, ImgIcon;
    [SerializeField]
    public Text TxtTenVipMua, TxtGia;
    [SerializeField]
    public Sprite NutVang, KimCuong;
    [SerializeField]
    public GameObject PanelMua;

    private string Id_Bor_CanMua;
    private string Id_Vip_Can;
    private long GiaChip;
    private long GiaGem;
    private bool byGem;

    public void MoPanelMua(string id, string TenVip, string gia, bool ByGem = false)
    {
        //  Debug.LogError("Vo Shop -> SprBor " + DataHelper.dictSpriteBoderAvt_Shop[id] + "   TenVip = " + TenVip + "   Gia = " + gia + "  ByGem = " + ByGem);
        //  ImgBor.sprite = sprBor;
        //   Debug.Log("Images-------------- " + ImgBor + "  TxtTenVipMua-----------------  " + TxtTenVipMua + "  TxtGia------------------ " + TxtGia);
        //  Debug.Log("DataHelper.dictSpriteBoderAvt_Shop[id]--------------- " + DataHelper.dictSpriteBoderAvt_Shop[id]);

        Id_Bor_CanMua = id;
        Id_Vip_Can = TenVip;
        byGem = ByGem;


        ImgBor.sprite = DataHelper.dictSpriteBoderAvt_Shop[id];
        TxtTenVipMua.text = "VIP" + TenVip;

        if (ByGem == true)
        {
            TxtGia.text = "<color=#49EEFDFF>" + gia + "</color>";
            ImgIcon.sprite = KimCuong;
            GiaGem = long.Parse(gia);
            GiaChip = -1;
        }
        else
        {

            long t = long.Parse(gia);
            string Gia = Utilities.GetStringMoneyByLong(t); ;

            TxtGia.text = Gia;
            ImgIcon.sprite = NutVang;

            GiaChip = long.Parse(gia);
            GiaGem = -1;
        }
        PanelMua.SetActive(true);
    }


    public void DongPaneMua()
    {
        ImgBor.sprite = null;
        TxtTenVipMua.text = "";
        TxtGia.text = "";
        ImgIcon.sprite = NutVang;
        PanelMua.SetActive(false);

        Id_Bor_CanMua = "";
        Id_Vip_Can = "";
        GiaChip = 0;
        GiaGem = 0;
        byGem = false;


}

    public void BtnMuaClick()
    {
        bool check = Check();

        if (check == true)
        {
            int id = int.Parse(Id_Bor_CanMua);
            MuaKhungAvartar(id);
        }
    }

    bool Check()
    {
        int Id_Vip = int.Parse(Id_Vip_Can);
        if (MyInfo.MY_ID_VIP < Id_Vip) // Kiểm tra loại Vip
        {
          //  Debug.LogError("Không đủ điều kiện VIP. Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!");
            popupAlert.Show("Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!", popupAlert.Hide);
            return false;
        }

        if (byGem == false) // kiểm tra đủ tiền để mua không
        {
            // Mua bằng Chip
            if (MyInfo.CHIP < GiaChip) 
            {
                Debug.Log("Không đủ CHIP để mua!");
                popupAlert.Show("Không đủ CHIP!", popupAlert.Hide);
                return false;
            }
        }
        else
        {
            // Mua bằng Gem
            if (MyInfo.GEM < GiaGem)
            {
                Debug.Log("Không đủ GEm để mua!");
                popupAlert.Show("Không đủ GEM!", popupAlert.Hide);
                return false;
            }
        }

        if (DataHelper.DicDataBoderAvatar.ContainsKey(Id_Bor_CanMua) == false) // Kiểm tra khung có tồn tại hay không
        {
            Debug.Log("Khung Avatar không tồn tại!");
            popupAlert.Show("Khung Avatar không tồn tại!", popupAlert.Hide);
            return false;
        }

        List<int> KhungSoHuu = MyInfo.MY_BORDERS_AVATAR; //Kiểm tra khung đã sỡ hữu
        int Id_Bor = int.Parse(Id_Bor_CanMua);
        for (int i = 0; i < KhungSoHuu.Count; i++)
        {
            if (Id_Bor == KhungSoHuu[i])
            {
                Debug.Log("Bạn đã sở hữu khung Avatar này!");
                popupAlert.Show("Bạn đã sở hữu khung Avatar này!", popupAlert.Hide);
                return false;
            }
        }


        Debug.Log("Đang gửi request mua");
        return true;
    }



}
