﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemTopArenaView : MonoBehaviour {

	[SerializeField]
	Image imgRank;
	[SerializeField]
	Text txtRank, txtName, txtChip;

	public void Show(int _rank, string _name, string _chip){

		Debug.Log (_rank + " _ " + _name + " __ " + _chip);

		if (_rank < 4) {

			txtRank.gameObject.SetActive (false);
			imgRank.gameObject.SetActive (true);

			imgRank.sprite = DataHelper.GetIconAchive (_rank);

		} else {

			txtRank.gameObject.SetActive (true);
			imgRank.gameObject.SetActive (false);

			txtRank.text = _rank.ToString ();

		}

		txtName.text = _name;

		txtChip.text = _chip;
	}

}
