﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class KetSatConTroll : MonoBehaviour
{
    [SerializeField]
    GameObject PanelKetSat, PanelPass, PanelGui, PanelRut, PanelInfo, PanelVip, PanelAva, PanelChangePass;

    // Tao Mat Khau
    [SerializeField]
    InputField InputMatKhau, InputXacNhanMatKhau, InputEmail;

    [SerializeField]
    Text TxtLuotGui, TxtLuotRut;

    int idVIP = 4;
    string TURN = "0";
    #region Kiểm Tra đã tạo pass chưa

    public void KiemTraKetSat()
    {
        //if (MyInfo.MY_ID_VIP < idVIP)
        //{
        //    AlertController.api.showAlert("Bạn cần đạt Vip 4 để mở Két!");
        //    return;
        //}
        
        //PanelKetSat.SetActive(true);
        //PanelInfo.SetActive(false);
        //PanelVip.SetActive(false);
        //PanelAva.SetActive(false);
        //PanelChangePass.SetActive(false);

        //PanelPass.SetActive(true);
        //PanelGui.SetActive(false);
        //PanelRut.SetActive(false);


        //PanelPass.SetActive(false);
        //PanelGui.SetActive(true);
        //PanelRut.SetActive(false);

        // kiểm tra đã có Pass hay chưa
           API.Instance.RequestCheckPass(RspCheckPass);
    }

    public void Init()
    {
        PanelPassInit();
        PanelGuiInit();
        PanelRutInit();
    }

    void RspCheckPass(string _json)
    {
        Debug.LogWarning("DU LIEU check pass: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"];

        if (sta == "-1")
        {
            //Tạo Pass
            Init();
            PanelPass.SetActive(true);
            PanelGui.SetActive(false);
            PanelRut.SetActive(false); 
            
        }
        else if (sta == "1")
        {
            //Mở Panel Gửi
            Init();

            long chip = long.Parse(node["chip"]);
            long chipKet = long.Parse(node["bank"]);

            TURN = node["turn"];

            MyInfo.CHIP = chip;
            MyInfo.CHIPBANK = chipKet;
            TxtChip.text = Utilities.GetStringMoneyByLong(chip);
            TxtChipTrongKet.text = Utilities.GetStringMoneyByLong(chipKet);
            PanelPass.SetActive(false);
            PanelGui.SetActive(true);
            PanelRut.SetActive(false);

            Debug.LogWarning("Turrrrn----------- " + TURN);
            KiemTraLuot();

        }
        else if (sta == "-2")
        {
            int idVip = int.Parse(node["vip_required"]);

            string vip = "VIP " + idVip.ToString();

            AlertController.api.showAlert("Bạn cần đạt " + vip +" để mở Két!");
            return;
           // Debug.LogWarning("kkkkkkkkkkkkk------------------- " + idVip);
        }

      //  PanelKetSat.SetActive(true);

    }

    void KiemTraLuot()
    {
        int l = int.Parse(TURN);
        if (l > 0)
        {
            TxtLuotGui.text = "Bạn còn " + TURN + " lượt giao dịch trong ngày!";
            TxtLuotRut.text = "Bạn còn " + TURN + " lượt giao dịch trong ngày!";
        }
        else
        {
            TxtLuotGui.text = "Bạn đã hết lượt giao dịch trong ngày!";
            TxtLuotRut.text = "Bạn đã hết lượt giao dịch trong ngày!";
            AlertController.api.showAlert("Số lượt giao dịch trog ngày đã hết. Bạn cần nâng cấp mức VIP để thêm lượt!");
            return;
        }
    }



    #endregion


    #region Tao Pass


    void PanelPassInit()
    {
        SetInputPass();
    }


    void SetInputPass()
    {
        InputMatKhau.contentType = InputField.ContentType.IntegerNumber;
        InputMatKhau.characterLimit = 4;

        InputXacNhanMatKhau.contentType = InputField.ContentType.IntegerNumber;
        InputXacNhanMatKhau.characterLimit = 4;
    }

    bool CheckInitPass()
    {
        bool HoanThanh = false;

        if (InputMatKhau.text == "")
        {
            AlertController.api.showAlert("Bạn cần nhập mật khẩu cho KÉT SẮT!");
            return HoanThanh;
        }

        if (InputMatKhau.text.Length < 4)
        {
            AlertController.api.showAlert("Mật Khẩu cần 4 ký tự!");
            return HoanThanh;
        }

        if (InputXacNhanMatKhau.text == "")
        {
            AlertController.api.showAlert("Bạn vui lòng xác nhận mật khẩu!");
            return HoanThanh;
        }

        if (InputXacNhanMatKhau.text != InputMatKhau.text)
        {
            AlertController.api.showAlert("Mật khẩu xác nhận không trùng khớp. Bạn vui lòng kiểm tra lại!");
            return HoanThanh;
        }

        HoanThanh = true;
        return HoanThanh;
    }

    public void InitPass()
    {
        if (CheckInitPass() == false)
        {
            return;
        }

        if (InputEmail.text == "")
        {
            AlertController.api.showAlert("Bạn cần nhập Email phòng trường hợp quên mật khẩu!");
            return;
        }

        string pass = InputMatKhau.text;
        string email = InputEmail.text;

        API.Instance.RequestInitPass(email, pass, RspInitPass);

    }

    void RspInitPass(string _json)
    {
        Debug.LogWarning("DU LIEU tao pass: " + _json);

        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"];

        if (sta == "-1")
        {
            //Tạo Pass thât bai

        }
        else
        {
            //Mở Panel Gửi
            PanelPass.SetActive(false);
            PanelGui.SetActive(true);
            PanelRut.SetActive(false);

        }

    }



    #endregion

    public void MoPanelGUI()
    {
        PanelGui.SetActive(true);
        PanelRut.SetActive(false);
        PanelGuiInit();
    }

    public void MoPanelRUT()
    {
        PanelGui.SetActive(false);
        PanelRut.SetActive(true);
        PanelRutInit();
    }


    #region Gửi CHIP

    [SerializeField]
    InputField InputCHIPgui;
    [SerializeField]
    Text TxtChip, TxtChipTrongKet;
    [SerializeField]
    Button BtnGui_G, BtnRut_G;

    string CHIP_gui = "0";
    void PanelGuiInit()
    {
        SetInputGui();
        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtChipTrongKet.text = Utilities.GetStringMoneyByLong(MyInfo.CHIPBANK);
        BtnGui_G.onClick.RemoveAllListeners();
        BtnGui_G.onClick.AddListener(MoPanelGUI);

        BtnRut_G.onClick.RemoveAllListeners();
        BtnRut_G.onClick.AddListener(MoPanelRUT);
    }

    void SetInputGui()
    {
        InputCHIPgui.contentType = InputField.ContentType.IntegerNumber;       
    }

    public void Cong_Tien(string t)
    {
        if (InputCHIPgui.text == "")
        {
            InputCHIPgui.text = "0";
        }
        long tien = long.Parse(t);

        long l = long.Parse(InputCHIPgui.text);

        l += tien;

        string k = string.Format("{0:0,0}", l);

        InputCHIPgui.text = k;

    }

    public void GuiChip()
    {
        int l = int.Parse(TURN);
        if (l > 0)
        {
            CHIP_gui = InputCHIPgui.text;
            long c = long.Parse(CHIP_gui);
            if (CHIP_gui != "" && c > 0)
            {
                API.Instance.RequestGuiChip(CHIP_gui, RspGuiChip);
            }
        }
        else
        {
            AlertController.api.showAlert("Số lượt giao dịch trog ngày đã hết. Bạn cần nâng cấp mức VIP để thêm lượt!");
            return;
        }
        
    }


    public void HuyGuiChip()
    {
        InputCHIPgui.text = "0";
    }

    void RspGuiChip(string _json)
    {
        Debug.LogWarning("DU LIEU gui chip: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"];

        if (sta == "-1")
        {

            // Gui that bai
            string msg = node["msg"];
            AlertController.api.showAlert(msg);
           // PanelPass.SetActive(true);
           // PanelGui.SetActive(false);
           // PanelRut.SetActive(false);

        }
        else
        {
            // Gui Thanh Cong
            string msg = node["msg"];
            AlertController.api.showAlert(msg);
            long chip = long.Parse(node["chip"]);
            long chipKet = long.Parse(node["bank"]);
            TxtChip.text = Utilities.GetStringMoneyByLong(chip);
            TxtChipTrongKet.text = Utilities.GetStringMoneyByLong(chipKet);
            TURN = node["turn"];
            KiemTraLuot();
            MyInfo.CHIP = chip;
            MyInfo.CHIPBANK = chipKet;

            InputCHIPgui.text = "0";
            //PanelPass.SetActive(false);
            //PanelGui.SetActive(true);
            //PanelRut.SetActive(false);

        }
    }

    #endregion




    #region RUT CHIP

    [SerializeField]
    InputField InputChipRut;
    [SerializeField]
    Text TxtChip_R, TxtChipTrongKet_R;
    [SerializeField]
    Button BtnGui_R, BtnRut_R;
    [SerializeField]
    InputField InputMatKhauRut;

    string CHIP_rut = "0";
    void PanelRutInit()
    {

        InputChipRut.contentType = InputField.ContentType.IntegerNumber;

        InputMatKhauRut.contentType = InputField.ContentType.IntegerNumber;
        InputMatKhauRut.characterLimit = 4;

        TxtChip_R.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtChipTrongKet_R.text = Utilities.GetStringMoneyByLong(MyInfo.CHIPBANK);
        BtnGui_R.onClick.RemoveAllListeners();
        BtnGui_R.onClick.AddListener(MoPanelGUI);

        BtnRut_R.onClick.RemoveAllListeners();
        BtnRut_R.onClick.AddListener(MoPanelRUT);
    }


    public void Cong_Tien_Rut(string t)
    {
        if (InputChipRut.text == "")
        {
            InputChipRut.text = "0";
        }

        long tien = long.Parse(t);

        long l = long.Parse(InputChipRut.text);

        l += tien;

        string k = string.Format("{0:0,0}", l);

        InputChipRut.text = k;

    }


    bool CheckInputPass()
    {
        bool HoanThanh = false;

        if (InputMatKhauRut.text == "")
        {
            AlertController.api.showAlert("Bạn cần nhập mật khẩu KÉT SẮT để rút CHIP!");
            return HoanThanh;
        }

        if (InputMatKhauRut.text.Length < 4)
        {
            AlertController.api.showAlert("Mật Khẩu KÉT SẮT cần 4 ký tự!");
            return HoanThanh;
        }

        HoanThanh = true;
        return HoanThanh;
    }


    public void RutChip()
    {
        int l = int.Parse(TURN);
        if (l > 0)
        {

            bool check = CheckInputPass();
            if (check == true)
            {
                CHIP_rut = InputChipRut.text;
                long c = long.Parse(CHIP_rut);
                if (CHIP_rut != "" && c > 0)
                {
                    string pass = InputMatKhauRut.text;
                    API.Instance.RequestRutChip(pass, CHIP_rut, RspRutChip);
                }
                else
                {
                    AlertController.api.showAlert("Mời bạn nhập số CHIP cần gửi!");
                    return;
                }
            }

        }
        else
        {
            AlertController.api.showAlert("Số lượt giao dịch trog ngày đã hết. Bạn cần nâng cấp mức VIP để thêm lượt!");
            return;
        }
    }


    void RspRutChip(string _json)
    {
        Debug.LogWarning("DU LIEU rut chip: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"];

        if (sta == "-1")
        {

            // Gui that bai
            string msg = node["msg"];
            AlertController.api.showAlert(msg);
            return;
            // PanelPass.SetActive(true);
            // PanelGui.SetActive(false);
            // PanelRut.SetActive(false);

        }
        else
        {
            // Gui Thanh Cong
            string msg = node["msg"];
            AlertController.api.showAlert(msg);
            long chip = long.Parse(node["chip"]);
            long chipKet = long.Parse(node["bank"]);
            TxtChip_R.text = Utilities.GetStringMoneyByLong(chip);
            TxtChipTrongKet_R.text = Utilities.GetStringMoneyByLong(chipKet);

            MyInfo.CHIP = chip;
            MyInfo.CHIPBANK = chipKet;
            TURN = node["turn"];
            KiemTraLuot();
            InputChipRut.text = "0";
            //PanelPass.SetActive(false);
            //PanelGui.SetActive(true);
            //PanelRut.SetActive(false);

        }
    }




    public void QuenMatKhau()
    {
        API.Instance.RequestQuenMatKhau(RspQuenMatKhau);
    }


    void RspQuenMatKhau(string _json)
    {
        Debug.LogWarning("DU LIEU gui chip: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"];

        if (sta == "-1")
        {

            // Gui that bai
            string msg = node["msg"];
            AlertController.api.showAlert(msg);
            return;
            // PanelPass.SetActive(true);
            // PanelGui.SetActive(false);
            // PanelRut.SetActive(false);

        }
        else
        {
            AlertController.api.showAlert("Mật khẩu mới đã được gửi vào Email của bạn!");
            return;
        }
    }


    #endregion
}


