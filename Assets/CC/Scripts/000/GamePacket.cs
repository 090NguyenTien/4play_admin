﻿using Sfs2X.Entities.Data;

public class GamePacket
{
    public readonly string cmd;
    public readonly SFSObject param;

    public GamePacket(string cmd, SFSObject param)
    {
        this.cmd = cmd;
        this.param = param;
    }

    public GamePacket(string cmd)
    {
        this.cmd = cmd;
        param = new SFSObject();
    }

    public void Put(string key, object value)
    {
        if (value is bool)
            param.PutBool(key, (bool)value);
        else if (value is byte)
            param.PutByte(key, (byte)value);
        else if (value is short)
            param.PutShort(key, (short)value);
        else if (value is int)
            param.PutInt(key, (int)value);
        else if (value is long)
            param.PutLong(key, (long)value);
        else if (value is float)
            param.PutFloat(key, (float)value);
        else if (value is double)
            param.PutDouble(key, (double)value);
        else if (value is string)
            param.PutUtfString(key, (string)value);
        else if (value is bool[])
            param.PutBoolArray(key, (bool[])value);
        else if (value is short[])
            param.PutShortArray(key, (short[])value);
        else if (value is int[])
            param.PutIntArray(key, (int[])value);
        else if (value is long[])
            param.PutLongArray(key, (long[])value);
        else if (value is float[])
            param.PutFloatArray(key, (float[])value);
        else if (value is double[])
            param.PutDoubleArray(key, (double[])value);
        else if (value is string[])
            param.PutUtfStringArray(key, (string[])value);
    }

    public bool GetBool(string key)
    {
        return param.GetBool(key);
    }

    public byte GetByte(string key)
    {
        return param.GetByte(key);
    }

    public short GetShort(string key)
    {
        return param.GetShort(key);
    }

    public int GetInt(string key)
    {
        return param.GetInt(key);
    }

    public long GetLong(string key)
    {
        return param.GetLong(key);
    }

    public float GetFloat(string key)
    {
        return param.GetFloat(key);
    }

    public double GetDouble(string key)
    {
        return param.GetDouble(key);
    }

    public string GetString(string key)
    {
        return param.GetUtfString(key);
    }

    public bool[] GetBoolArray(string key)
    {
        return param.GetBoolArray(key);
    }

    public short[] GetShortArray(string key)
    {
        return param.GetShortArray(key);
    }

    public int[] GetIntArray(string key)
    {
        return param.GetIntArray(key);
    }

    public long[] GetLongArray(string key)
    {
        return param.GetLongArray(key);
    }

    public float[] GetFloatArray(string key)
    {
        return param.GetFloatArray(key);
    }

    public double[] GetDoubleArray(string key)
    {
        return param.GetDoubleArray(key);
    }

    public string[] GetStringArray(string key)
    {
        return param.GetUtfStringArray(key);
    }

    public ISFSArray GetSFSArray(string key)
    {
        return param.GetSFSArray(key);
    }
    public ISFSObject GetSFSObject(string key)
    {
        return param.GetSFSObject(key);
    }
    public bool ContainKey(string key)
    {
        return param.ContainsKey(key);
    }
}
