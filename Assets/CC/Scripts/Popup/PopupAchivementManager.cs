﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using BaseCallBack;
using System.Collections.Generic;

public class PopupAchivementManager : MonoBehaviour {

	[SerializeField]
	GameObject panel, panelNapTien, panelChip, panelTaiSan;
	[SerializeField]
	Toggle toggleNgay, toggleTuan, toggleTopThang, toggleTopChip, toggleTopTaiSan;

    //[SerializeField]
    //GameObject panelTopTuan, panelTopEvent;

    [SerializeField]
    GameObject objItemTopRich, objItemTopNapTien;
	[SerializeField]
	Text txtToggleTopEvent;

    [SerializeField]
    Transform trsfRichParent, trsfTaiSanParent, trsfNapTienParent;

	[SerializeField]
	Button btnExit;

	[SerializeField]
	GameObject Loading;

    [SerializeField]
    Text txtTienDaNap;

    [SerializeField]
    MadControll Mad;
    [SerializeField]
    GameObject ObjTaiSan;
    //	[SerializeField]
    //	List<ItemTopChipView> lstTopChip;
    //	[SerializeField]
    //	List<ItemTopWinView> lstTopWin;

    string json = "";
	bool isFirst;
	
	onCallBack _backOnClick;
	public void Init(onCallBack _back )
	{
		isFirst = true;
        toggleTopChip.onValueChanged.AddListener(TogglePanelOnChange);
        toggleTopTaiSan.onValueChanged.AddListener(TogglePanelOnChange);
        toggleTuan.onValueChanged.AddListener (TogglePanelOnChange);
        toggleTopThang.onValueChanged.AddListener (TogglePanelOnChange);
        toggleNgay.onValueChanged.AddListener(TogglePanelOnChange);
        //toggleTopWinEvent.onValueChanged.AddListener (TogglePanelOnChange);

        //toggleTLNM.onValueChanged.AddListener (ToggleTLMNOnChange);
        //toggleXiTo.onValueChanged.AddListener (ToggleXiToOnChange);
        //toggleMB.onValueChanged.AddListener (ToggleMBOnChange);
        //togglePhom.onValueChanged.AddListener (TogglePhomOnChange);
        //      toggleTLDL.onValueChanged.AddListener(ToggleTLDLOnChange);  
        
        btnExit.onClick.AddListener (BtnExitOnClick);

		_backOnClick = _back;
        

	}

	public bool LOADING{
		set{ Loading.SetActive (value); }
	}

	void BtnExitOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
		_backOnClick ();
	}

	public void SetEnable(bool _enable)
	{
        
        if (_enable) {
            toggleNgay.isOn = true;
            Loading.SetActive (true);
            panelChip.SetActive(false);
            panelTaiSan.SetActive(false);
            panelNapTien.SetActive(true);
        }
	}

	void ShowItemTop(int _index, string _name, long _chip, string id)
	{
		ItemTopChipView itemView;
        //if (_index >= trsfRichParent.childCount) {
			GameObject obj = Instantiate (objItemTopRich) as GameObject;

			obj.transform.SetParent (trsfRichParent);

			obj.transform.SetSiblingIndex (_index);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			itemView = obj.GetComponent<ItemTopChipView> ();

//			lstTopChip.Add (itemView);
		//} else {
		//	itemView = trsfRichParent.GetChild (_index).GetComponent<ItemTopChipView> ();
		//}
		itemView.Init ();
		itemView.Show (id, _index, _name, _chip);
	}



    void ShowItemTopTaiSan(int _index, string _name, long _chip, string id)
    {
        ItemTopChipView itemView;
        
        GameObject obj = Instantiate(objItemTopRich) as GameObject;

        obj.transform.SetParent(trsfTaiSanParent);

        obj.transform.SetSiblingIndex(_index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;

        itemView = obj.GetComponent<ItemTopChipView>();

        //			lstTopChip.Add (itemView);
        //} else {
        //	itemView = trsfRichParent.GetChild (_index).GetComponent<ItemTopChipView> ();
        //}
        itemView.Init();
        itemView.Show(id, _index, _name, _chip, true);
    }







    void ShowItemTopNapTien(int _index, string _name, string _chip, int _count)
    {
        ItemTopWinView itemView;
       // if (_index >= trsfNapTienParent.childCount)
       // {
            GameObject obj = Instantiate(objItemTopNapTien) as GameObject;

            obj.transform.SetParent(trsfNapTienParent);

            obj.transform.SetSiblingIndex(_index);
            obj.transform.localScale = Vector3.one;
            obj.transform.position = Vector3.zero;

            itemView = obj.GetComponent<ItemTopWinView>();
            
        //}
       // else
        //{
        //    itemView = trsfNapTienParent.GetChild(_index).GetComponent<ItemTopWinView>();
      //  }       
        itemView.Init();
        itemView.Show(_index, _name, _chip,_count,0);
    }

    void ShowItemTopVip(int _index, string _name, long _chip)
    {
        ItemTopChipView itemView;

        GameObject obj = Instantiate(objItemTopRich) as GameObject;

        obj.transform.SetParent(trsfTaiSanParent);

        obj.transform.SetSiblingIndex(_index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;

        itemView = obj.GetComponent<ItemTopChipView>();

        itemView.Init();
        //itemView.Show(_index, _name, _chip, true);
    }

    void ShowItemTopWinEvent(int _index, string _name, int _win, int _lose)
	{
        /*
		ItemTopWinEventView itemView;

		if (_index >= trsfEventParent.childCount) {
			GameObject obj = Instantiate (objItemTopEvent) as GameObject;

			obj.transform.SetParent (trsfEventParent);

			obj.transform.SetSiblingIndex (_index);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			itemView = obj.GetComponent<ItemTopWinEventView> ();

			//			lstTopWin.Add (itemView);
		} else {
			itemView = trsfEventParent.GetChild (_index).GetComponent<ItemTopWinEventView> ();
		}
		itemView.Init ();
		itemView.Show(_index, _name, _win, _lose);
        */
	}

	void ClearitemTop(int _indexFrom)
	{
		for (int i = 0; i < trsfRichParent.childCount; i++) {
            //Debug.LogError("Loichilcount====" + i);
			GameObject.Destroy (trsfRichParent.GetChild (i).gameObject);
		}
	}

    void ClearitemTopTaiSan(int _indexFrom)
    {
        for (int i = 0; i < trsfTaiSanParent.childCount; i++)
        {
            //Debug.LogError("Loichilcount====" + i);
            GameObject.Destroy(trsfTaiSanParent.GetChild(i).gameObject);
        }
    }

    void ClearitemTopNapTien()
    {
        for (int i = 0; i < trsfNapTienParent.childCount; i++)
        {
            GameObject.Destroy(trsfNapTienParent.GetChild(i).gameObject);
        }
    }
    void ClearitemTopWinEvent(int _indexFrom)
	{
        /*
		for (int i = _indexFrom; i < trsfEventParent.childCount; i++) {
			GameObject.Destroy (trsfEventParent.GetChild (i).gameObject);
		}
        */
	}

	#region Toggle OnChange

	void TogglePanelOnChange (bool _enable)
	{
		if (!_enable)
			return;
        //panelTopTuan.SetActive (toggleTuan.isOn);
        //panelTopEvent.SetActive (toggleTopChip.isOn);
        panelNapTien.SetActive(true);
        panelChip.SetActive(false);
        panelTaiSan.SetActive(false);
        //if (toggleNgay.isOn)
        //{
        //    RequestTopNapTien("day");
        //}
        //else if (toggleTuan.isOn)
        //{
        //    RequestTopNapTien("week");
        //}
        //else if (toggleTopThang.isOn)
        //{
        //    RequestTopNapTien("month");
        //}
        if (toggleNgay.isOn)
        {
            RequestTopVip();
        }
        else if (toggleTuan.isOn)
        {
            RequestTopGem();
        }
        else if (toggleTopChip.isOn)
        {
            RequestTopChip();
            panelNapTien.SetActive(false);
            panelTaiSan.SetActive(false);
            panelChip.SetActive(true);
        }
        else if (toggleTopTaiSan.isOn)
        {
            RequestTopTaiSan();
            panelNapTien.SetActive(false);
            panelChip.SetActive(false);
            panelTaiSan.SetActive(true);
        }
    }
    #endregion

    void RequestTopNapTien(string mode)
    {
        API.Instance.RequestTopNapTien(mode,RspTopNapTien);
        LOADING = true;
    }
    void RequestTopGem()
    {
        API.Instance.RequestTopGem(RspTopGem);
        LOADING = true;
    }
    void RspTopGem(string _json)
    {
        Debug.LogWarning("RspTopGem------------- " + _json);


        JSONNode node = JSONNode.Parse(_json);

        JSONNode data = node["data"];

        Debug.Log(data.Count + " - " + data.ToString());
        ClearitemTopNapTien();
        for (int i = 0; i < data.Count; i++)
        {
            //Debug.LogError("iiii" + i);
            //for (int i = 0; i < 5; i++)        {
            string name = data[i]["username"].Value;
            long gem = long.Parse(data[i]["items"]["gem"].Value);

            ShowItemTopNapTien(i, name, gem.ToString(),0);
        }


        LOADING = false;
    }
    void RequestTopVip()
    {
        API.Instance.RequestTopVip(RspTopVip);
        LOADING = true; 
    }
    void RspTopVip(string _json)
    {
        Debug.LogWarning("RspTopVip------------- " + _json);


        JSONNode node = JSONNode.Parse(_json);

        JSONNode data = node["data"];

        Debug.Log(data.Count + " - " + data.ToString());
        ClearitemTopNapTien();
        for (int i = 0; i < data.Count; i++)
        {
            //Debug.LogError("iiii" + i);
            //for (int i = 0; i < 5; i++)        {
            string name = data[i]["username"].Value;
            int vip = int.Parse(data[i]["items"]["vip_collect"].Value)/1000;

            ShowItemTopNapTien(i, name, vip.ToString(), 0);
        }


        LOADING = false;
    }
    void RequestTopChip()
	{
		API.Instance.RequestTopChip (RspTopChip);
		LOADING = true;
	}

    void RequestTopTaiSan()
    {
        API.Instance.RequestTopTaiSan(RspTopTaiSan);
        LOADING = true;
    }



    void RspTopTaiSan(string _json)
    {
        Debug.LogWarning("RspTopTaiSan------------- " + _json);
        

        JSONNode node = JSONNode.Parse(_json);

        JSONNode data = node["data"];

        Debug.Log(data.Count + " - " + data.ToString());
        ClearitemTopTaiSan(data.Count);
        for (int i = 0; i < data.Count; i++)
        {
            Debug.LogError("iiii" + i);
            //for (int i = 0; i < 5; i++)        {
            string name = data[i]["username"].Value;
            long chip = long.Parse(data[i]["asset_point"].Value);
            string uID = data[i]["user_id"]["$id"].Value;
           // Debug.LogWarning("id user: " + uID);
            ShowItemTopTaiSan(i, name, chip, uID);
        }


        LOADING = false;

        
    }


    public void XemTaiSan(string idUser)
    {
        
        API.Instance.RequestVatPham_SoHuu_Enemy(idUser, RspLayDuLieuVatPham_SoHuu);
    }

    public void TatXemTaiSan()
    {
        ObjTaiSan.SetActive(false);
        
    }




    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);
        DataHelper.DuLieuMap_Enemy.Clear();
        if (_json == "false")
        {
            ////  Debug.LogError("DU LIEU SAN PHAM so huu------------------------:");
            //DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
            //for (int i = 1; i < 8; i++)
            //{
            //    DataHelper.DuLieuMap_Enemy.Add(i, "");
            //}

            HienThiMadMacDinh_Enemy();
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);

            //   Debug.LogWarning("000000000000000000000000v       " + node["placed_assets"].Value);
            // string dataMap = node["placed_assets"].Value;
            if (node["placed_assets"].Value == "null")
            {

                //DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
                //for (int i = 1; i < 8; i++)
                //{
                //    DataHelper.DuLieuMap_Enemy.Add(i, "");
                //}
                HienThiMadMacDinh_Enemy();
                //   Debug.LogError("BAT DC NE HEHE---------- ");
            }
            else
            {
                int cou = node["placed_assets"].Count;
                // Debug.LogError("BAT DC so luong NE HEHE---------- " + cou);

                for (int i = 0; i < cou; i++)
                {
                    int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                    string a_id = node["placed_assets"][i]["id"].Value;
                    string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                    long a_level = long.Parse(node["placed_assets"][i]["level"].Value);

                    Item_InMad itInMad = new Item_InMad();
                    itInMad.id = a_id;
                    itInMad.idTong = a_id_tong;
                    itInMad.Level = a_level;

                    string type = "house";

                    if (pos > 0 && pos < 6)
                    {
                        type = "shop";
                    }
                    else if (pos > 5 && pos < 8)
                    {
                        type = "car";
                    }

                    long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                    itInMad.DoanhThu = doanhthu;


                    DataHelper.DuLieuMap_Enemy[pos] = itInMad;
                    //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                    // DataHelper.DuLieuMap_Enemy[pos] = a_id;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (DataHelper.DuLieuMap_Enemy.ContainsKey(i) == false)
                    {
                        Item_InMad itInMad = new Item_InMad();
                        itInMad.id = "";
                        itInMad.idTong = "";
                        //if (i == 0)
                        //{
                        //    itInMad.id = "5dc03420e52b8e2d238b6045";
                        //    itInMad.idTong = "5dc03420e52b8e2d238b6045";
                        //}
                        itInMad.Level = 0;
                        itInMad.DoanhThu = 0;

                        DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
                    }

                    //   Debug.LogError("ket vi tri ----- " + i  + " ---------- " + DataHelper.DuLieuMap[i]);
                }

            }
        }


        Mad.XemNha();
        ObjTaiSan.SetActive(true);
    }




    long GetDoanhThuByIDTong(string idTong, long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }



    void HienThiMadMacDinh_Enemy()
    {
        Item_InMad itInMad_Home = new Item_InMad();
        itInMad_Home.id = "5dc03420e52b8e2d238b6045";
        itInMad_Home.idTong = "5dc03420e52b8e2d238b6045";
        itInMad_Home.Level = 0;
        itInMad_Home.DoanhThu = 0;

        DataHelper.DuLieuMap_Enemy.Add(0, itInMad_Home);

        for (int i = 1; i < 8; i++)
        {

            Item_InMad itInMad = new Item_InMad();
            itInMad.id = "";
            itInMad.idTong = "";
            itInMad.Level = 0;
            itInMad.DoanhThu = 0;


            DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
        }
    }










    void RequestTopWinEvent(){
		API.Instance.RequestTopWinEvent (RspTopWinEvent);
	}

	void RspTopWinEvent (string _json)
	{
		
		JSONNode node = JSONNode.Parse (_json);

		if (node==null || string.IsNullOrEmpty(node.ToString()))
			return;

		Debug.Log ("RSP ___ EVENT TOP:\n" + node.ToString ());

//		bool enable = true;

//		ToggleTopEvent.SetActive (enable);

		string sName = node["game"];

		txtToggleTopEvent.text = sName;

		JSONNode data = node ["data"];

		for (int i = 0; i < data.Count; i++) {
			string name = data [i] ["username"].Value;
			int win = data [i] ["win"].AsInt;
			int lose = data [i] ["lose"].AsInt;

			ShowItemTopWinEvent (i, name, win, lose);
		}
		ClearitemTopWinEvent (data.Count);
	}

	void RspTopChip(string _json)
	{
		JSONNode node = JSONNode.Parse (_json);

		JSONNode data = node ["data"];

		Debug.Log (data.Count + " -Aaaaaaaaa- " + data.ToString ());
        ClearitemTop(data.Count);
        for (int i = 0; i < data.Count; i++)
        {
           // Debug.LogError("iiii" + i);
            //for (int i = 0; i < 5; i++)        {
			string name = data [i] ["username"].Value;
            string uID = data[i]["id"].Value;
            long chip = long.Parse (data [i] ["items"] ["chip"].Value);
           // Debug.LogWarning("id user: " + uID);
			ShowItemTop (i, name, chip, uID);
		}
		

		LOADING = false;
	}


    void RspTopNapTien(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);

        JSONNode data = node["data"];
        string curMoneyUser = data["mine"].Value;
        txtTienDaNap.text = curMoneyUser;
        Debug.Log(data.Count + " - " + data.ToString());
        JSONNode data2 = data["data"];
        ClearitemTopNapTien();
        for (int i = 0; i < data2.Count; i++)
        {
            //Debug.Log("iiiiiiiiii=====" + i);   
            string name = data2[i]["_id"].Value;
            string chip = data2[i]["total_amount"].Value;
            int count = int.Parse(data2[i]["count"].Value);
            ShowItemTopNapTien(i, name, chip, count);
        }
        

        LOADING = false;
    }

}

/*
 ========= CONTROLLER ========
 
 [SerializeField]
	PopupAchivementManager Achivement;
	
	Achivement.Init (BackAchivementOnClick);
	
public void BtnAchivementOnClick()
	{
		Achivement.SetEnable (true);
		SetEnableContent (false);
		_screen = ScreenLobby.Achivement;
	}
	void BackAchivementOnClick()
	{
		View.SetEnableContent (true);
		Achivement.SetEnable (false);
	}
	void SetEnableContent(bool _enable)
	{
		View.SetEnableContent (_enable);
	}

====== VIEW ======
[SerializeField]
	Button btnAchivement;

		btnAchivement.onClick.AddListener (BtnAchivementOnClick);


void BtnAchivementOnClick()
	{
		Controller.BtnAchivementOnClick ();
	}
	public void SetEnableContent(bool _enable)
	{
		panelUI.SetActive (_enable);
	}

 */
