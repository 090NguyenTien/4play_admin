﻿using Sfs2X.Entities.Data;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class PopupNapTichLuy : MonoBehaviour {

    [SerializeField]
    Text txtCurMoney;
    List<ItemTichLuyRenderer> lstItemTichLuy;
    [SerializeField]
    Transform ScrollViewContent;
    [SerializeField]
    Button btnClose;
    [SerializeField]
    GameObject panelReceiveGift;
    [SerializeField]
    ItemTichLuyRenderer objItemTichluy;
    private SFS sfs
    {
        get { return SFS.Instance; }
    }

    

    //onCallBack _closeOnClick;

    List<ItemTichLuyRenderer> lstItem;
    // Use this for initialization
   
    
    public void Init()
    {
        if (lstItem == null) lstItem = new List<ItemTichLuyRenderer>();
        btnClose.onClick.AddListener(CloseOnClick);
        Show();
        panelReceiveGift.SetActive(false);
    }
    void CloseOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Hide();
        gameObject.SetActive(false);
        HomeViewV2.api.UpdateChip(Utilities.GetStringMoneyByLong(MyInfo.CHIP));
    }    

    public void Show()
    {
        API.Instance.RequestNapTichLuy(ResNapTichLuy);
    }

    private void ResNapTichLuy(string s)
    {
        Debug.Log("ResNapTichLuy: " + s);
        JSONNode node = JSONNode.Parse(s);
        //GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;
        int status = int.Parse(node["status"].Value);
       
        JSONNode config = node["config"];
        JSONNode receiveIndex=null;
        long curTotal=0;
        if (status == 1)
        {
            curTotal = long.Parse(node["total"]);
            receiveIndex = node["receive_index"];
        }
        //txtCurMoney.text = curTotal.ToString();

        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
        txtCurMoney.text = double.Parse(curTotal.ToString()).ToString("#,###", cul.NumberFormat) + "";

        long total;
        long chip;
        int wheel;
            
        ItemTichLuyRenderer item;
        for(int i=0;i<config.Count;i++)
        {
            item = Instantiate(objItemTichluy) as ItemTichLuyRenderer;
            total = long.Parse(config[i]["total"]);
            chip = long.Parse(config[i]["chip"]);
            wheel = int.Parse(config[i]["wheel"]);                
            item.Init(curTotal,total, chip, wheel,i, receiveIndex, panelReceiveGift);
            item.transform.SetParent(ScrollViewContent);
            item.transform.localScale = Vector3.one;
            lstItem.Add(item);
        }           
        
    }

    public void Hide()
    {
        ClearAllitem();
        btnClose.onClick.RemoveListener(CloseOnClick);
    }

    public void ClearAllitem(int _indexFrom = 0)
    {   
        for (int i = _indexFrom; i < ScrollViewContent.childCount; i++)
        {
            GameObject.Destroy(ScrollViewContent.GetChild(i).gameObject);
        }
    }

}
