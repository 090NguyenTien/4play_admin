﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;
using BaseCallBack;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

public class PopupLuckyWheelManager : MonoBehaviour
{

    [SerializeField]
    GameObject panel;
    [SerializeField]
    Transform trsfLuckyWheel;
    [SerializeField]
    Button btnStart, btnBack;
    [SerializeField]
    Image imgFree;
    [SerializeField]
    Sprite sprLightTrue, sprLightFalse;

    [SerializeField]
    Image imgBtnPlay, imgIconPlay;
    [SerializeField]
    GameObject CountDown;
    [SerializeField]
    PopupAlertManager AlertResult;

    [SerializeField]
    private Animator animLighting = null;


    float timeRemainSecond;

    List<string> lstType;
    List<long> lstValue;
    int HourResetTime;
    long Cost;
    int Turn;
    int RemainTimeSecond;

    onCallBack backClick;
    //	onCallBackBool luckWheelState;
    [HideInInspector]
    public bool IsRolling = false;
    private string adsSecretkey = "";
    private int adsType = 1;
    private string vungleRewardId = "";
    bool adInited = false;
    bool IsInited = false;
    public void Init()
    {

        if (IsInited)
            return;

        btnStart.onClick.AddListener(StartWheelOnClick);
        isLight = true;

        IsInited = true;

        SetActiveBtnPlay(true);

        AlertResult.Init();
    }

    private void OnEnable()
    {
        //string appId = "";
        //string rewardId = "";
        //switch (Application.identifier)
        //{
        //    case "com.gamebai.gamebai4play":
        //        appId = "5c38142fbf572a456f33b7ec";
        //        rewardId = "REWARDVIDEO-8388483";
        //        break;
        //    case "com.gamebai4play.TLMN":
        //        appId = "5c8f0301d13ede0012e154fa";
        //        rewardId = "DEFAULT-1524168";
        //        break;
        //    case "com.gamebai.gamebai4play.MauBinh":
        //        appId = "5c3740d22a7f9a421232c451";
        //        rewardId = "REWARDVIDEO-6113817";
        //        break;
        //    case "com.gamebai.gamebai4play.Xito":
        //        appId = "5c381491bf572a456f33ba2b";
        //        rewardId = "REWARDVIDEO-5375386";
        //        break;
        //    case "com.gamebai.gamebai4play.TLDL":
        //        appId = "5c3717a37c736545b214e56a";
        //        rewardId = "REWARDVIDEO-1584061";
        //        break;
        //    case "com.gamebai.gamebai4play.Phom":
        //        appId = "5c38138e5307f7455ea52893";
        //        rewardId = "REWARDVIDEO-4745904";
        //        break;
        //}
        //vungleRewardId = rewardId;
        //Vungle.init(appId);
        //initializeEventHandlers();
    }

    #region Button Play State

    void SetActiveBtnPlay(bool _enable)
    {
        imgBtnPlay.raycastTarget = _enable;
        if (imgIconPlay != null) imgIconPlay.enabled = _enable;
    }

    #endregion

    int currentSecond;

    public void BtnBackOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        backClick();
    }


    public void Show()
    {
        panel.SetActive(true);
    }
    public void Hide()
    {
        panel.SetActive(false);
    }

    void StartWheelOnClick()
    {
        if (IsRolling)
            return;
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        trsfLuckyWheel.DORotate(new Vector3(0, 0, 360), 10, RotateMode.FastBeyond360).SetEase(Ease.InQuint);
        API.Instance.RequestAdsFreeWheel((_json) =>
        {
            JSONNode node = JSONNode.Parse(_json);
            Debug.Log(" RspRequestAds " + _json);
            if (node["status"].AsInt == 1)
            {
                adsSecretkey = node[ParamKey.ADS_SECRET_KEY].Value;
#if UNITY_IOS || UNITY_ANDROID
                if (adsType == 1)
                    ShowUnityVideoRewardAd();
                //else if (Vungle.isAdvertAvailable(vungleRewardId))
                //{
                //    Vungle.playAd(vungleRewardId);
                //}
                else
                {
                    ShowUnityVideoRewardAd();
                }
#endif
            }
        });
    }

    void RspStartLuckyWheel(string _json)
    {
        Debug.Log(_json);

        JSONNode jsonNode = JSONNode.Parse(_json);

        if (jsonNode["code"].AsInt == -1)
        {
            ShowError(jsonNode["message"].Value);
        }
        else
        {
            JSONNode node = jsonNode["rewards"];

            long chip = long.Parse(node["value"].Value);
            string type = node["type"].Value;


            int index = jsonNode["index"].AsInt - 1;

            JSONNode items = jsonNode["items"];
            MyInfo.CHIP = long.Parse(items["chip"].Value);
            MyInfo.GOLD = long.Parse(items["gold"].Value);
            MyInfo.VIPPOINT = int.Parse(items["vippoint"].Value);

            IsRolling = true;
            //			luckWheelState (!IsRolling);

            StartLuckyWheel(index, () =>
            {
                ShowError("Chúc mừng bạn đã nhận được\n" + Utilities.GetStringMoneyByLong(chip) + " " + type, () => {
                    IsRolling = false;
                    if (HomeControllerV2.api != null)
                        HomeControllerV2.api.UpdateInfoUser();
                });

                //				StartCoroutine(ShowAnimChipThread());

                //				luckWheelState(!IsRolling);
            });
        }
    }

    void Show(GameObject _obj)
    {
        _obj.SetActive(true);
        _obj.GetComponent<Animation>().Play();
    }
    public void Hide(GameObject _obj)
    {
        _obj.SetActive(false);
    }

    public Ease easeType = Ease.InOutExpo;
    public float TimeWheel = 7;
    void StartLuckyWheel(int _index, onCallBack _callBack = null)
    {
        Vector3 result = GetRotationZ(_index);
        Debug.Log("index chip nek:   " + _index + "    result   " + result);

        animLighting.SetTrigger("ShowWheeling");

        trsfLuckyWheel.DOKill(false);
        trsfLuckyWheel.DORotate(result, Random.Range(TimeWheel - 2, TimeWheel + 2), RotateMode.FastBeyond360).SetEase(easeType).OnComplete(() => {
            if (_callBack != null) ;
            _callBack();

            animLighting.SetTrigger("ShowWheelComplete");

            if (lightThread != null)
                StopCoroutine(lightThread);
        });
    }

    void ShowError(string _msg)
    {
        AlertResult.Show(_msg, AlertResult.Hide);
    }
    void ShowError(string _msg, onCallBack _callBack)
    {
        AlertResult.Show(_msg, () => {
            AlertResult.Hide();
            _callBack();
        });
    }
    bool isLight = true;
    public float deltaTimeWheel = .1f;
    public float distantTime = .1f;
    Coroutine lightThread;


    int numPart = 8;//so phan chia tren wheel
    const int angle = 45;//1 phan chiem 45 do
    const int Loop = 20;
    Vector3 GetRotationZ(int _index)
    {
        int min = angle * (_index - 1);
        int max = angle * _index;
        int rotation = Random.Range(min, max) - 360;

        float result = (rotation - (Random.Range(Loop - 2, Loop + 2) * 360));
        return new Vector3(0, 0, result);
    }
    private void callReceiveGiftWatched()
    {
        API.Instance.RequestStartWheel(adsSecretkey, RspStartLuckyWheel);
    }
#if UNITY_IOS || UNITY_ANDROID
    public string placementId = "rewardedVideo";
    void ShowUnityVideoRewardAd()
    {
        ShowOptions options = new ShowOptions();
       // options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched();
        }
        else if (result == ShowResult.Skipped)
        {
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            //if (Vungle.isAdvertAvailable(vungleRewardId))
            //{
            //    Vungle.playAd(vungleRewardId);
            //}
            //else
            //{
                AlertController.api.showAlert("Hiện không có video nào khả dụng, vui lòng quay lại sau");
            //}
        }
    }

#endif
    //void initializeEventHandlers()
    //{
    //    Vungle.onAdStartedEvent += (placementID) => {
    //        Debug.Log("Ad " + placementID + " is starting!  Pause your game animation or sound here.");
    //    };
    //    Vungle.onAdFinishedEvent += (placementID, args) => {
    //        Debug.Log("Ad finished - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked + ", is completed view:"
    //            + args.IsCompletedView);
    //        if (args.IsCompletedView == true)
    //        {
    //            callReceiveGiftWatched();
    //        }
    //        else
    //        {
    //            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
    //        }
    //    };


    //    Vungle.adPlayableEvent += (placementID, adPlayable) => {
    //        Debug.Log("Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
    //        //placements[placementID] = adPlayable;
    //    };

    //    Vungle.onInitializeEvent += () => {
    //        adInited = true;
    //        btnStart.interactable = true;
    //    };
    //}
}
