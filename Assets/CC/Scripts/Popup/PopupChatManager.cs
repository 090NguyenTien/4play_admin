﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;
using DG.Tweening;

public class PopupChatManager : MonoBehaviour
{

    [SerializeField]
    private GameObject panel, popupChat, panelBg, panelEmotion, panelEmotion_New, panelQuick;

    [SerializeField]
    Transform trsfEmotionParent, trsfQuickParent, trsfEmotionParent_New;

    [SerializeField]
    Toggle toggleEmotion, toggleQuick;

    [SerializeField]
    InputField inputChat;
    [SerializeField]
    Button btnSendChat, btnClose, btnChat;

    //	[SerializeField]
    //	List<Animator> lstObjChatEmotion;	//lst cac hinh hien thi chat ngoai
    [SerializeField]
    List<Transform> lstObjChatEmotion;
    [SerializeField]
    List<Text> lstObjChatText;	//lst cac text hien thi chat ngoai

    [SerializeField]
    Button BtnEmotion;

    string vip = "0";

    List<Coroutine> lstThreadChatEmotion, lstThreadChatText;

    public static PopupChatManager instance;

    const float TIME_CHAT = 3;

    const string _Z_ = "#";

    List<string> lstQuickChat = new List<string>() {
        "Chào người mới!",
        "Nhanh lên nào!",
        "Thua đi cưng!",
        "Ahihi...!!!",
        "Cho chết! Hihi.",
        "Tạm biệt!",
        "Mày hả bưởi!",
        "Cẩn thận nhé! ^^",
        "Sợ chưa?",
        "Bài đen quá!",
        "Có hàng, có hàng...!",
        "Xui rồi :(",
        "Xem quảng cáo đi mài",
        "Tiền Nhiều Để Làm Gì",
        "Chơi hết luôn đi",
        "Nhanh nào! nhà bao việc"
    };
    List<Animator> lstAnim = new List<Animator>();

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        instance = this;

        toggleEmotion.onValueChanged.AddListener(OnToggleEmotion);

        btnSendChat.onClick.AddListener(BtnSendChatOnClick);
        btnClose.onClick.AddListener(BtnCloseOnClick);

        btnChat.onClick.AddListener(BtnChatOnClick);

        BtnEmotion.onClick.RemoveAllListeners();
        BtnEmotion.onClick.AddListener(OnEmotion);

        lstThreadChatEmotion = new List<Coroutine>();
        lstThreadChatText = new List<Coroutine>();

        for (int i = 0; i < trsfEmotionParent.childCount; i++)
        {
            trsfEmotionParent.GetChild(i).GetComponent<ItemIdView>().Init(i, ItemChatEmotionOnClick);
            trsfEmotionParent.GetChild(i).GetComponent<Image>().sprite = GetSprite(i);
        }

        for (int i = 0; i < trsfEmotionParent_New.childCount; i++)
        {
            trsfEmotionParent_New.GetChild(i).GetComponent<ItemIdView>().Init(i, ItemChatEmotionOnClick_New);
            trsfEmotionParent_New.GetChild(i).GetComponent<Image>().sprite = GetSprite(i);
        }


        for (int j = 0; j < trsfQuickParent.childCount; j++)
        {
            ItemIdView item = trsfQuickParent.GetChild(j).GetComponent<ItemIdView>();
            item.Init(j, ItemQuickChatOnClick);
            item.InitText(GetText(j));

        }

        for (int k = 0; k < lstObjChatEmotion.Count; k++)
        {
            lstThreadChatEmotion.Add(null);
            lstThreadChatText.Add(null);
        }


        Hide();
    }

    void BtnChatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        //if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        //{
            Show();
        //}else
        //{
        //    AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        //}
        
    }

    void OnToggleEmotion(bool _enable)
    {

        panelEmotion.SetActive(_enable);
        panelQuick.SetActive(!_enable);
    }



    public void OnEmotion()
    {
        //if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        //{
            panelEmotion_New.SetActive(true);
            BtnEmotion.onClick.RemoveAllListeners();
            BtnEmotion.onClick.AddListener(OffEmotion);
        //}
        //else
        //{
        //    AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        //}
        
    }

    public void OffEmotion()
    {
        panelEmotion_New.SetActive(false);
        BtnEmotion.onClick.RemoveAllListeners();
        BtnEmotion.onClick.AddListener(OnEmotion);
    }



    public void Show()
    {
        popupChat.SetActive(true);
        btnClose.gameObject.SetActive(true);

        inputChat.text = "";
    }
    public void Hide()
    {
        btnClose.gameObject.SetActive(false);
        popupChat.SetActive(false);

        //		Debug.Log ("Hide CHAT");
    }

    void BtnSendChatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        if (string.IsNullOrEmpty(inputChat.text))
            return;
        if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        {
            RqChatFree(inputChat.text);
            Hide();
        }else
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        }
        
    }
    void ItemChatEmotionOnClick(int _id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        RqChatEmotion(_id);

        Hide();
    }

    void ItemChatEmotionOnClick_New(int _id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        RqChatEmotion(_id);
        panelEmotion_New.SetActive(false);
        // Hide();
    }


    void ItemQuickChatOnClick(int _index)
    {
        RqQuickChat(_index);

        Hide();
    }

    #region Request SFS

    void RqChatEmotion(int _id)
    {
        //		GamePacket packet = new GamePacket (cmd);

        int idUser = MyInfo.SFS_ID;
        int type = 0;

        string msg = idUser.ToString() + _Z_ + type + _Z_ + _id.ToString();

        //		packet.Put (paramK, msg);
        //if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        //{
            SFS.Instance.SendPublicMsg(msg);
        //}
        //else
        //{
        //    AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        //}
        
    }
    void RqQuickChat(int _index)
    {
        RqChatFree(GetText(_index));
        Hide();

        return;
        //		GamePacket packet = new GamePacket (cmd);

        int idUser = MyInfo.SFS_ID;
        int type = 1;

        string msg = idUser.ToString() + _Z_ + type + _Z_ + _index.ToString();

        //		packet.Put (paramK, msg);
        //if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        //{
            SFS.Instance.SendPublicMsg(msg);
        //}
        //else
        //{
        //    AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        //}
        
    }
    void RqChatFree(string _msg)
    {
        //		GamePacket packet = new GamePacket (cmd);
        //if (MyInfo.MY_ID_VIP >= MyInfo.VIP_CHAT_ALLOW)
        //{
            int idUser = MyInfo.SFS_ID;
            int type = 2;

            string msg = idUser.ToString() + _Z_ + type + _Z_ + _msg;

            //		packet.Put (paramK, msg);

            SFS.Instance.SendPublicMsg(msg, 1);
        //}else
        //{
        //    AlertController.api.showAlert("Bạn Phải Đạt Vip " + MyInfo.VIP_CHAT_ALLOW.ToString() + "Mới Có Thể Dùng Tính Năng Này");
        //}

        
    }

    #endregion

    #region Response SFS

    public void RspChat(int _position, string _msg, string _vip)
    {
        //		string s = _param.GetString (paramK);
        vip = _vip;
        string[] info = _msg.Split('#');

        int idUser = int.Parse(info[0]);
        int type = int.Parse(info[1]);

        //EMOTION
        if (type == 0)
        {
            int index = int.Parse(info[2]);

            print("position: " + _position);
            if (lstThreadChatEmotion[_position] != null)
                StopCoroutine(lstThreadChatEmotion[_position]);

            lstThreadChatEmotion[_position] = StartCoroutine(ShowChatEmotion(index, _position));
        }
        //QUICK
        else if (type == 1)
        {
            int index = int.Parse(info[2]);

            if (lstThreadChatText[_position] != null)
                StopCoroutine(lstThreadChatText[_position]);

            lstThreadChatText[_position] = StartCoroutine(ShowChatQuick(index, _position));
        }
        //CUSTOM
        else if (type == 2)
        {
            string msg = info[2];

            if (lstThreadChatText[_position] != null)
                StopCoroutine(lstThreadChatText[_position]);

            lstThreadChatText[_position] = StartCoroutine(ShowChatFree(msg, _position));
        }
    }

    #endregion

    #region Show/Hide Chat

    Tween twChatEmotion;
    IEnumerator ShowChatEmotion(int _index, int _position)
    {
        if (_index > -1 && _index < lstSpriteEmotion.Count)
        {

            lstObjChatEmotion[_position].localScale = new Vector3(3, 3, 0);
            lstObjChatEmotion[_position].gameObject.SetActive(true);
            lstObjChatEmotion[_position].GetComponent<Image>().sprite = GetSprite(_index);


            Debug.Log("Index: " + (_index + 1));
            //			lstObjChatEmotion [_position].SetInteger ("ChatEmotion", _index + 1);


            if (twChatEmotion != null)
            {
                twChatEmotion.Pause();

                lstObjChatEmotion[_position].transform.localScale = new Vector3(6, 6, 0);
            }

            twChatEmotion = lstObjChatEmotion[_position].transform.DOPunchScale(new Vector3(1f, 1f, 1), 2, 2).SetLoops(3);



            yield return new WaitForSeconds(3);
            HideChatEmotion(_position);
        }
    }
    void HideChatEmotion(int _position)
    {
        //lstObjChatEmotion[_position].gameObject.SetActive(false);
  
        lstObjChatEmotion[_position].gameObject.SetActive(false);
    }

    IEnumerator ShowChatQuick(int _index, int _position)
    {
        if (_index > -1 && _index < lstQuickChat.Count)
        {
            if (lstObjChatText[_position].gameObject.transform.parent.name.StartsWith("BG"))
            {
                lstObjChatText[_position].gameObject.transform.parent.gameObject.SetActive(true);
            }
            lstObjChatText[_position].gameObject.SetActive(true);
            lstObjChatText[_position].text = GetText(_index);

            yield return new WaitForSeconds(3);
            HideChatQuick(_position);
        }
    }
    public void HideChatQuick(int _position)
    {
		if (lstObjChatText[_position].gameObject.transform.parent.name.StartsWith("BG"))
        {
            lstObjChatText[_position].gameObject.transform.parent.gameObject.SetActive(false);
        }
        lstObjChatText[_position].gameObject.SetActive(false);
    }

    IEnumerator ShowChatFree(string _msg, int _position)
    {
		if (lstObjChatText[_position].gameObject.transform.parent.name.StartsWith("BG"))
        {
            lstObjChatText[_position].gameObject.transform.parent.gameObject.SetActive(true);
        }
        lstObjChatText[_position].gameObject.SetActive(true);
        //newMsg.textObject.color = new Color(255.0f/255.0f, 20.0f/255.0f, 147.0f/255.0f);
        //vip = Random.Range(4, 15).ToString(); //test
        GoiVip gv = VIP_Controll.DicVipData[int.Parse(vip)];
        string[] colorPoint;
        if (int.Parse(vip) > 3)
        {
            colorPoint = gv.ColorChat.Split(',');
            lstObjChatText[_position].color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
        }
        
        lstObjChatText[_position].text = _msg;

        yield return new WaitForSeconds(3);
        HideChatFree(_position);
    }
    public void HideChatFree(int _position)
    {
		if (lstObjChatText[_position].gameObject.transform.parent.name.StartsWith("BG"))
        {
            lstObjChatText[_position].gameObject.transform.parent.gameObject.SetActive(false);
        }
        lstObjChatText[_position].gameObject.SetActive(false);
    }

    #endregion

    void BtnCloseOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Hide();
    }

    #region Utils

    [SerializeField]
    List<Sprite> lstSpriteEmotion;

    void Wait(onCallBack _callBack, float _time = TIME_CHAT)
    {
        StartCoroutine(WaitThread(_time, _callBack));
    }
    IEnumerator WaitThread(float _time, onCallBack _callBack)
    {
        yield return new WaitForSeconds(_time);
        _callBack();
    }

    Sprite GetSprite(int _index)
    {
        return lstSpriteEmotion[_index];
    }
    string GetText(int _index)
    {
        if (_index < 0 || _index >= lstQuickChat.Count)
            return "...";
        else
            return lstQuickChat[_index];
    }

    #endregion

    public void OnResponsePublicMessage(string _msg)
    {
        //		Debug.Log ("Rsp CHAT _ " + _msg);

        //		RspChat (_msg);

    }
}

/*

===== VIEW =====

		[SerializeField]
		Button btnChat;

		btnChat.onClick.AddListener (BtnChatOnClick);
		void BtnChatOnClick()
		{
			Controller.BtnChatOnClick ();
		}


===== CONTROLLER =====
#region CHAT

		public void BtnChatOnClick()
		{
			PopupChatManager.instance.Show ();
		}
		
		public void OnPublicMsg(string _msg)
		{
			int idUser = int.Parse (_msg.Split('#')[0]);

			//Get Position by ID
			int position = dictIdPos[idUser];


			PopupChatManager.instance.RspChat (position, _msg);


		}

		#endregion

 */
