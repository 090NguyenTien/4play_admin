﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;

public class PopupInboxManager : MonoBehaviour {

    private static PopupInboxManager instance;
    public static PopupInboxManager Instance
    {
        get
        {
            if (instance == null)
            {
                if (HomeViewV2.api != null)
                {
                    instance = HomeViewV2.api.Controller.Inbox;
                    return instance;
                }
                else 
                {
                    instance = HomeControllerV2.api.Inbox;
                    return instance;
                }
            }
            return instance;
        }
    }
    [SerializeField]
    GameObject panel;
    [SerializeField]
    Transform trsfPopup;
    [SerializeField]
    GameObject inboxList;
    [SerializeField]
    GameObject inboxDetail;
    [SerializeField]
    Button btnExit;
    onCallBack _exitOnClick;
    [SerializeField]
    Button btnBack;
    [SerializeField]
    GameObject objItemInbox;
    [SerializeField]
    Transform trsfItemInboxParent;
    [SerializeField]
    Text txtDetailFromName;
    [SerializeField]
    Text txtDetailTitle;
    [SerializeField]
    Text txtDetailDesc;
    [SerializeField]
    Text txtNoMessage;
    [SerializeField]
    Image ImgUnreadInbox;
    [SerializeField]
    Text TxtUnreadInbox;
    [SerializeField]
    Button BtnReceive;
    void Awake()
    {
        instance = this;
    }
    

    public void Init(onCallBack _back)
    {
        BtnReceive.gameObject.SetActive(false);
        txtNoMessage.gameObject.SetActive(false);
        btnExit.onClick.AddListener(BtnExitOnClick);
        _exitOnClick = _back;
        btnBack.onClick.AddListener(BtnBackOnClick);
        RequestInbox();
    }

    void BtnExitOnClick()
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
        BtnReceive.gameObject.SetActive(false);
        _exitOnClick();
    }

    public void addNotification()
    {
        MyInfo.UNREAD_INBOX = MyInfo.UNREAD_INBOX + 1;

        TxtUnreadInbox.text = MyInfo.UNREAD_INBOX + "";
        TxtUnreadInbox.gameObject.SetActive(true);
        ImgUnreadInbox.gameObject.SetActive(true);
    }

    void BtnBackOnClick()
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        btnBack.gameObject.SetActive(false);
        inboxDetail.SetActive(false);
        inboxList.SetActive(true);
        BtnReceive.gameObject.SetActive(false);
    }
    Inbox curInbox;
    public void BtnSelectOnClick(Inbox inbox)
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        txtDetailTitle.text = inbox.Title;
        txtDetailFromName.text = inbox.Author;
        if (inbox.HaveItem)
        {
            string chipVal = inbox.Items["chip"].Value;
            string gemVal = inbox.Items["gem"].Value;
            string strContent = "";
            if(chipVal !="" && chipVal != "null" && int.Parse(chipVal) != 0)
            {
                strContent += chipVal + " Chip "; 
            }else if (gemVal!="" && int.Parse(gemVal) != 0 && gemVal != "null")
            {
                strContent += gemVal + " Gem ";
            }
            txtDetailDesc.text = "Bạn Được Tặng : " + strContent + " \nVới Tin Nhắn : " + inbox.Content;
        }
        else
        {
            txtDetailDesc.text = inbox.Content;
        }
        
        inboxList.SetActive(false);
        inboxDetail.SetActive(true);
        btnBack.gameObject.SetActive(true);
        
        BtnReceive.gameObject.SetActive(inbox.HaveItem);
        if (inbox.Status == 1)//đã nhận rồi
        {
            BtnReceive.gameObject.SetActive(false);
        }
        curInbox = inbox;
    }
    public void ReceiveGiftInbox()
    {
        //API.Instance.RequestReceiveInbox(curInbox.Id, OnReceiveComplete);
        GamePacket gp = new GamePacket(CommandKey.RECEIVE_INBOX);
        gp.Put("inbox_id", curInbox.Id);
        SFS.Instance.SendZoneRequest(gp);
    }
    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.RECEIVE_INBOX:
                OnReceiveInboxFromServer(gp);
                break;
        }
    }

    private void OnReceiveInboxFromServer(GamePacket gp)
    {
        int status = gp.GetInt("status");
        if (status == 1)
        {
            string receive = gp.GetLong("receive").ToString();
            string gemVal = gp.GetLong("gem").ToString();
            string chip = gp.GetLong("chip").ToString();
            string strContent = "";
            if (receive != "" && receive != "null" && int.Parse(receive) != 0)
            {
                strContent += receive + " Chip ";
            }
            else if (gemVal != "" && int.Parse(gemVal) != 0 && gemVal != "null")
            {
                strContent += gemVal + " Gem ";
            }


            AlertController.api.showAlert("Đã Nhận: " + strContent);
            MyInfo.CHIP = long.Parse(chip);
            MyInfo.GEM = long.Parse(gemVal);
            BtnExitOnClick();
        }
        else
        {
            AlertController.api.showAlert(gp.GetString("msg"));
        }
    }

    private void OnReceiveComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        if (int.Parse(node["status"].Value) == 1)
        {
            string chipVal = node["receive_chip"]["chip"].Value;
            string gemVal = node["receive_chip"]["gem"].Value;
            string strContent = "";
            if (chipVal != "" && chipVal != "null" && int.Parse(chipVal) != 0)
            {
                strContent += chipVal + " Chip ";
            }
            else if (gemVal != "" && int.Parse(gemVal) != 0 && gemVal != "null")
            {
                strContent += gemVal + " Gem ";
            }


            AlertController.api.showAlert("Đã Nhận: " + strContent);
            MyInfo.CHIP = long.Parse(node["chip"].Value);
            MyInfo.GEM = long.Parse(node["gem"].Value);
            BtnExitOnClick();
        }
        else
        {
            AlertController.api.showAlert("Bạn Đã Nhận Rồi!");
        }
        BtnReceive.gameObject.SetActive(false);
    }

    public void BtnDeleteOnClick(string id)
    {
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

        print("inboxes count 1: " + MyInfo.INBOXES.Count);

        MyInfo.INBOXES = MyInfo.INBOXES.Where(x => !x.Id.Equals(id)).ToList();

        print("inboxes count 2: " + MyInfo.INBOXES.Count);
        RequestDelete(id);
    }

    void ShowItemInbox(int index, Inbox inbox)
    {
        ItemInboxView itemView;
        GameObject obj = Instantiate(objItemInbox) as GameObject;
        obj.transform.SetParent(trsfItemInboxParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<ItemInboxView>();
        itemView.Init(inbox);
        itemView.Show(index);
    }

    public void RequestInbox()
    {
        Debug.LogError("RequestInbox nekkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
        API.Instance.RequestInbox(ResponseInbox);
    }

    void RequestDelete(string id)
    {
        print("RequestDelete " + id);



        API.Instance.RequestDeleteInbox(id, ResponseDeleteInbox);
    }

    void ResponseInbox(string json)
    {
        print("ResponseInbox " + json.ToString());
        JSONNode data = JSONNode.Parse(json);

        MyInfo.INBOXES = new List<Inbox>();

        for (int i = 0; i < data.Count; i++)
        {
            Inbox inbox = new Inbox();
            inbox.Id = data[i]["id"].Value;
            inbox.Title = data[i]["title"].Value;
            inbox.Content = data[i]["desc"].Value;
            inbox.Author = data[i]["from_name"].Value;
            if (data[i]["items"].Value.ToString()!= "null")
            {
                inbox.HaveItem = true;
                inbox.Status = int.Parse(data[i]["items"]["status"].Value);
                inbox.Items = data[i]["items"];
            }                
            //inbox.CreatedDate = DateTime.Parse(data[i]["created_date"].Value);
            inbox.CreatedDate = (data[i]["created_date"].Value);
            MyInfo.INBOXES.Add(inbox);
        }

        UpdateInboxes();
    }

    void ResponseDeleteInbox(string json)
    {
        UpdateInboxes();
    }

    public void Show()
    {
        MyInfo.UNREAD_INBOX = 0;
        TxtUnreadInbox.gameObject.SetActive(false);
        ImgUnreadInbox.gameObject.SetActive(false);
        inboxDetail.SetActive(false);
        inboxList.SetActive(true);
        btnBack.gameObject.SetActive(false);
        panel.SetActive(true);
        GameHelper.DoOpen(trsfPopup, OnShow);
    }

    public void Hide()
    {
        GameHelper.DoClose(trsfPopup, OnHide);
    }

    void OnShow()
    {

    }

    void OnHide()
    {
        panel.SetActive(false);
    }

    private void UpdateInboxes()
    {
        for (int i = 0; i < trsfItemInboxParent.childCount; i++)
            Destroy(trsfItemInboxParent.GetChild(i).gameObject);

        for (int i = 0; i < MyInfo.INBOXES.Count; i++)
            ShowItemInbox(i, MyInfo.INBOXES[i]);

        if (MyInfo.INBOXES.Count > 0)
            txtNoMessage.gameObject.SetActive(false);
        else
            txtNoMessage.gameObject.SetActive(true);
    }
}

public class Inbox {
    public string Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string Author { get; set; }
    public JSONNode Items { get; set; }
    public bool HaveItem { get; set; }
    public int Status { get; set; }
    public string CreatedDate { get; set; }
}