﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using BaseCallBack;
using SimpleJSON;
using UnityEngine.EventSystems;
using System;

public class PopupEventManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	Transform trsfItemParent;
	[SerializeField]
	Image imgMain, imgAction;
	[SerializeField]
	Button btnAction, btnWebView, btnBack;
	[SerializeField]
	GameObject objEventThumb,objEvenDapHeo;
    //[SerializeField]
    //PopupAlertManager popupAlertManager;
    [SerializeField]
    private Button btnFreeWheel = null;

    //[SerializeField]
    //private GameObject firstTimePurchasePanel = null;
	static bool isLoad = false;
	static List<Sprite> lstSprItem;

	bool isLoadCurrentScene = false;

	onCallBack backClick;

	int CurrentIndex;

//	List<Sprite> lstSprSmall, lstSprBig, lstSprButton;
//	List<string> lstLink;

	ItemEventThumbView[] lstItemThumb;
	ItemEventMainView[] lstItemMain;

	[SerializeField]
	PopupLuckyWheelManager Wheel;

	int CurrentIndexEvent;
	int IndexWheel;

	int EventCount{
		get{ return GameHelper.nodeEventData.Count; }
	}
	static JSONNode nodeEvent{
		get{ return GameHelper.nodeEventData; }
	}

	public void Init(onCallBack _callBack)
	{
		btnBack.onClick.AddListener (BtnBackOnClick);

        btnFreeWheel.onClick.AddListener(showFreeWheel);
        //btnFirstTimePurchase.onClick.AddListener(showFirstTimePurchase);

        backClick = _callBack;

		//btnAction.onClick.AddListener (BtnActionOnClick);
		CurrentIndex = 0;

        //RspEvent();
        Wheel.Init();
        //Wheel.RequestLuckyWheelData();
    }
    private GameObject curEvent = null;
    private void showFreeWheel()
    {
        Wheel.Show();
        curEvent = Wheel.gameObject;
    }

    public void showFirstTimePurchase()
    {
        //firstTimePurchasePanel.gameObject.SetActive(true);
        //popupAlertManager.ShowFirstPurchase(OpenFirstPurchasePanel);
    }

    private void OpenFirstPurchasePanel()
    {
        throw new NotImplementedException();
    }

    void OnSetEnableBtnBack (bool _enable)
	{
		btnBack.gameObject.SetActive (_enable);
	}

	#region THUMB

	void OnGetSprThumb (int i, Sprite spr)
	{
		lstItemThumb [i].ShowSprite (spr);
	}

	void ItemThumbOnClick (int _index)
	{
//		Debug.Log ("Index: " + _index);
		if (CurrentIndex == _index || Wheel.IsRolling)
			return;

		//Phong to nho hinh thumb
		for (int i = 0; i < lstItemThumb.Length; i++) {
			lstItemThumb [i].IsSelected = i == _index;
		}
		CurrentIndex = _index;

		//Show Sprite Main
		if (lstItemMain [_index].SprBig == null)
			StartCoroutine (GameHelper.Thread (lstItemMain [_index].UrlSpr, OnShowSprMain));
		else
			OnShowSprMain ();

        //Neu index == indexWHeel thi show Wheel
        if (IndexWheel == _index)
        {
            Wheel.Show();
        }
        else
        {
            Wheel.Hide();
        }
		
		bool _isAction = lstItemMain [_index].IsAction;

		//Neu index nay co Action thi show buttonAction
		if (_isAction) {
			if (lstItemMain [_index].SprAction == null)
				StartCoroutine (GameHelper.Thread (lstItemMain [_index].UrlSpr, OnShowSprAction));
			else
				OnShowSprAction ();
		}

		btnAction.gameObject.SetActive (_isAction);

	}

	#endregion

#region MAIN

	void OnShowSprMain()
	{
		imgMain.sprite = lstItemMain [CurrentIndex].SprBig;
	}
	void OnShowSprMain(Sprite _spr)
	{
		lstItemMain [CurrentIndex].SprBig = _spr;
		imgMain.sprite = _spr;
	}
	void OnShowSprAction()
	{
		imgAction.gameObject.SetActive (true);
		imgAction.sprite = lstItemMain [CurrentIndex].SprAction;
	}
	void OnShowSprAction(Sprite _spr)
	{
		lstItemMain [CurrentIndex].SprAction = _spr;
		imgAction.gameObject.SetActive (true);
		imgAction.sprite = _spr;
	}

	#endregion


	public void Show()
	{
        panel.SetActive(true);
	}

	void BtnActionOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		Debug.Log (lstItemMain [CurrentIndex].UrlAction);
		
	}

	void BtnWebViewOnClick(string _link)
	{
	
	}

	public void BtnBackOnClick()
	{

        SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		if (Wheel.IsRolling)
			return;
		
		backClick ();
        curEvent.gameObject.SetActive(false);

    }

	public void Hide()
	{
		panel.SetActive (false);
	}

	public IEnumerator Thread(string _url1, onCallBackSprite _spr1)
	{
		WWW www1 = new WWW (_url1);
		yield return www1;

		Texture2D mainImage1 = www1.texture;
		Sprite spr = Sprite.Create(mainImage1, new Rect(0, 0, mainImage1.width, mainImage1.height), new Vector2(0.5f, 0.5f));

		_spr1 (spr);
	}

    public void RequestToInitPig()
    {
        API.Instance.RequestToInitPig(RspInitPig);
    }

    void RspInitPig(string _json)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        string maxChip_1 = "0", maxChip_2 = "0", maxChip_3 = "0";

        string wheel_2 = "0", wheel_3 = "0", card_2 = "0", card_3 = "0";

        string Iron_hammer = "0", Silver_hammer = "0", Gold_hammer = "0";

        Dictionary<int, ItemShopDapHeo> ShopDH_1 = new Dictionary<int, ItemShopDapHeo>();
        Dictionary<int, ItemShopDapHeo> ShopDH_2 = new Dictionary<int, ItemShopDapHeo>();

        JSONNode node = JSONNode.Parse(_json);
        JSONNode data = node["data"];


        Iron_hammer = node["iron_hammer"].Value;
        Silver_hammer  = node["silver_hammer"].Value;
        Gold_hammer = node["gold_hammer"].Value;

        JSONNode shop = node["shop"];

        Debug.Log(data.Count + " - " + node);
        

        Debug.Log("Iron_hammer = " + Iron_hammer + " Silver_hammer = " + Silver_hammer + " Gold_hammer = " + Gold_hammer);
        for (int i = 0; i < data.Count; i++)
        {
            //for (int i = 0; i < 5; i++)        {
            string da = data[i]["gifts"]["chip_max"].Value;
            Debug.Log("vo toi day");
            da = string.Format("{0:n0}", int.Parse(da));

            Debug.Log("vo toi day luon " + da);
            //Debug.Log("id ne = " + da);
            if (i == 0)
            {
                maxChip_1 = da;
            }
            else if (i == 1)
            {
                maxChip_2 = da;
                string wh = data[i]["gifts"]["wheel_max"].Value;
                string ca = data[i]["gifts"]["card_max"].Value;
                ca = string.Format("{0:n0}", int.Parse(ca));
                wheel_2 = wh;
                card_2 = ca;
            }
            else if (i == 2)
            {
                maxChip_3 = da;
                string wh = data[i]["gifts"]["wheel_max"].Value;
                string ca = data[i]["gifts"]["card_max"].Value;
                ca = string.Format("{0:n0}", int.Parse(ca));
                wheel_3 = wh;
                card_3 = ca;
            }          
        }
       
        JSONNode itemShop_1 = shop[0]["item"];
        JSONNode itemShop_2 = shop[1]["item"];


        for (int i = 0; i < itemShop_1.Count; i++)
        {
            ItemShopDapHeo item = new ItemShopDapHeo();
            item.id = itemShop_1[i]["id"].Value;
            item.price_vnd = itemShop_1[i]["price_vnd"].Value;
            item.price_usd = itemShop_1[i]["price_usd"].Value;
            item.turn = itemShop_1[i]["turn"].Value;
            item.StoreId = itemShop_1[i]["store_id"].Value;
            ShopDH_1.Add(i, item);

        }

        for (int i = 0; i < itemShop_2.Count; i++)
        {
            ItemShopDapHeo item = new ItemShopDapHeo();
            item.id = itemShop_2[i]["id"].Value;
            item.price_vnd = itemShop_2[i]["price_vnd"].Value;
            item.price_usd = itemShop_2[i]["price_usd"].Value;
            item.turn = itemShop_2[i]["turn"].Value;
            item.StoreId = itemShop_2[i]["store_id"].Value;
            ShopDH_2.Add(i, item);
        }


        //  Debug.Log("chip_1 = " + maxChip_1 + " chip_2 = " + maxChip_2 + " chip_3 = " + maxChip_3);

        //   Debug.Log("wheel_2 = " + wheel_2 + " wheel_3 = " + wheel_3);

        //   Debug.Log("card_2 = " + card_2 + " card_3 = " + card_3);

        //   GameObject obj = Instantiate(objEvenDapHeo) as GameObject;
        //   obj.transform.SetParent(gameObject.transform);

        EvenDapHeo DH = objEvenDapHeo.GetComponent<EvenDapHeo>();
        DH.CountHammer(Iron_hammer, Silver_hammer, Gold_hammer);

        DH.InitShop(ShopDH_1, ShopDH_2);

        DH.Init(maxChip_1, maxChip_2, maxChip_3, wheel_2, wheel_3, card_2, card_3);

        DH.CheckButton();

        // ClearitemTopRich(data.Count);

        // LOADING = false;
    }




}

public class ItemShopDapHeo
{
    public string id;
    public string price_vnd;
    public string price_usd;
    public string turn;
    public string StoreId;
}