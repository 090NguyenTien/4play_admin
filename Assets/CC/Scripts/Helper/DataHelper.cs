﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class DataHelper : MonoBehaviour
{

    static Dictionary<string, Sprite> dictSprite = new Dictionary<string, Sprite>();
    static Dictionary<string, Sprite> dictSpriteAvt = new Dictionary<string, Sprite>();
    static ArrayList avatarDefaultList = new ArrayList();
    public static Dictionary<string, Sprite> dictSpriteBoderAvt_Shop = new Dictionary<string, Sprite>();

    public static Dictionary<string, Sprite> dictSpriteBoderAvt = new Dictionary<string, Sprite>();

    public static Dictionary<string, BoderAvatar> DicDataBoderAvatar = new Dictionary<string, BoderAvatar>();

    static DataHelper instance;

    public static Sprite HinhKhungmacDinh;

    public static Dictionary<int, int> DicItemBoom = new Dictionary<int, int>();



    public static Dictionary<string, Sprite> dictSprite_House = new Dictionary<string, Sprite>();
    public static Dictionary<string, Sprite> dictSprite_Shop = new Dictionary<string, Sprite>();
    public static Dictionary<string, Sprite> dictSprite_Car = new Dictionary<string, Sprite>();



    public static Dictionary<string, My_Item> DuLieuNha = new Dictionary<string, My_Item>();
    public static Dictionary<string, My_Item> DuLieuShop = new Dictionary<string, My_Item>();
    public static Dictionary<string, My_Item> DuLieuXe = new Dictionary<string, My_Item>();

    public static Dictionary<string, int> DuLieuSoHuu = new Dictionary<string, int>();
    public static Dictionary<string, int> KhoItem = new Dictionary<string, int>();


    public static Dictionary<string, My_Item_SoHuu> DuLieuSoHuu_V2 = new Dictionary<string, My_Item_SoHuu>();

    public static Dictionary<string, string> DanhSach_LoaiVatPham_SoHuu = new Dictionary<string, string>();

    public static Dictionary<int, Item_InMad> DuLieuMap = new Dictionary<int, Item_InMad>();
    public static Dictionary<int, Item_InMad> DuLieuMap_Enemy = new Dictionary<int, Item_InMad>();
    public static Dictionary<string, int> DuLieuDiemTaiSan = new Dictionary<string, int>();
    public static Dictionary<string, string> DuLieuTenImgTheoID = new Dictionary<string, string>();

    public static Dictionary<int, string> DanhSachMod = new Dictionary<int, string>();

    public static Sprite SprKhung;
    public static int MY_DiemTaiSan;

    public static bool IsInitShopVatPham = false;
    public static bool IsInitItemShop = false;

    public static string ZALO_ADD = "";






    void Start()
    {
        DontDestroyOnLoad(this);
        instance = this;
    }

    public static void GetAvatarV2(string _name, Sprite avatar)
    {
        //		Debug.Log ("GetAVT: " + _name);
        string key = _name;
        if (_name.Contains("http"))
        {
            MyStartCoroutine(_name, avatar);
            return;
        }
        if (dictSpriteAvt.ContainsKey(key))
        {
            avatar = dictSpriteAvt[key];
        }
        else
            avatar = Resources.Load<Sprite>("Avatar/avt.png");
    }










    public static Sprite GetAvatar(string _name, Sprite _avatar = null)
    {
        Debug.Log("GetAVT: " + _name);
        string key = _name;
        Sprite avatar = null;
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        if (_name.Contains("http"))
        {
            MyStartCoroutine(_name, avatar);
            return avatar;
        }
        try
        {
            Debug.LogError("API.PREFIX_AVT + _name:============ " + API.PREFIX_AVT + _name);
            MyStartCoroutine(API.PREFIX_AVT + _name, avatar);
            //ApiHelper.Instance.CallBack(_name, API.PREFIX_AVT + _name, (nameReturn, _spr) =>
            //{
            //    _avatar = _spr;                
            //});
            return avatar;
        }
        catch (Exception e)
        {
            return Resources.Load<Sprite>("Avatar/avt.png");
        }
        //if (dictSpriteAvt.ContainsKey(key))
        //{
        //    avatar = dictSpriteAvt[key];
        //    return avatar;
        //}
        //return Resources.Load<Sprite>("Avatar/avt.png");
    }

    public IEnumerator ThreadAvatarDefault(int _level, string _url, BaseCallBack.onCallBackIntSprite _spr)
    {
        int level = _level;
        WWW www = new WWW(_url);
        yield return www;
        Texture2D mainImage = www.texture;
        Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

        _spr(_level, spr);
    }

    public static Sprite GetBoderAvatar(string _name)
    {
        if (_name != "-1")
        {
            Debug.Log("GetBorAVT: " + _name);
            string key = _name;
            Sprite BorAvatar = null;

            if (dictSpriteBoderAvt.ContainsKey(key))
            {
                BorAvatar = dictSpriteBoderAvt[key];
                return BorAvatar;
            }
            else
            {
                Debug.Log("Không có hình trong dictSpriteBoderAvt voi key = " + key + "   so lu?ng trong dictSpriteBoderAvt = " + dictSpriteBoderAvt.Count);
                return Resources.Load<Sprite>("Icon/borderAvatar");
            }

        }
        return Resources.Load<Sprite>("Icon/borderAvatar");
    }







    public static Sprite GetBoderAvatar_Shop(string _name)
    {
        Debug.Log("GetBorAVT: " + _name);
        if (_name != "-1")
        {
            string key = _name;
            Sprite BorAvatar = null;
            LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            if (_name.Contains("http"))
            {
                MyStartCoroutine(_name, BorAvatar);
                return BorAvatar;
            }
            if (dictSpriteBoderAvt_Shop.ContainsKey(key))
            {
                BorAvatar = dictSpriteBoderAvt_Shop[key];
                return BorAvatar;
            }

        }
        return HinhKhungmacDinh;
    }







    public static void MyStartCoroutineImage(string _url, Image avatar)
    {
        instance.StartCoroutine(UpdateAvatarThread(_url, avatar));
    }


    public static void MyStartCoroutine(string _url, Sprite avatar)
    {
        instance.StartCoroutine(UpdateAvatarThread(_url, avatar));
    }
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    public static IEnumerator UpdateAvatarThread(string _url, Sprite avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;
        try
        {
            mainImage = www.texture;
            avatar = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
        }
        catch (Exception e)
        {
            Debug.Log("Khong Load Duoc Avatar hahahahaha=======_url:::" + _url);
        }

    }
    public static Dictionary<string, Sprite> GetAvatars()
    {
        return dictSpriteAvt;
    }

    public static Dictionary<string, Sprite> GetBoderAvatars_Shop()
    {
        return dictSpriteBoderAvt_Shop;
    }


    public static Dictionary<string, Sprite> GetBoderAvatars()
    {
        return dictSpriteBoderAvt;
    }


    public static void AddAvatar(string _name)
    {
        if (!avatarDefaultList.Contains(_name))
        {
            avatarDefaultList.Add(_name);
        }
    }

    public static ArrayList GetAvatarList()
    {
        return avatarDefaultList;
    }
    public static void AddAvatar(string _name, Sprite _spr)
    {

        string key = _name;
        if (dictSpriteAvt.ContainsKey(key))
            dictSpriteAvt[key] = _spr;
        else
            dictSpriteAvt.Add(key, _spr);
    }

    public static void AddBoderAvatar_Shop(string _name, Sprite _spr)
    {

        string key = _name;
        if (dictSpriteBoderAvt_Shop.ContainsKey(key))
        {
            dictSpriteBoderAvt_Shop[key] = _spr;
        }
        else
        {
            dictSpriteBoderAvt_Shop.Add(key, _spr);
        }
    }



    public static void AddBoderAvatar(string _name, Sprite _spr)
    {

        string key = _name;
        if (dictSpriteBoderAvt.ContainsKey(key))
        {
            dictSpriteBoderAvt[key] = _spr;
        }
        else
        {
            dictSpriteBoderAvt.Add(key, _spr);
        }



    }

    public static string GetStringVipByStringVipPoint(string VipPoint)
    {
        string Vip = "0";

        long intVip = long.Parse(VipPoint);
        if (intVip >= 200000000)
        {
            Vip = "15";
        }
        else if (intVip >= 175000000)
        {
            Vip = "14";
        }
        else if (intVip >= 135000000)
        {
            Vip = "13";
        }
        else if (intVip >= 75000000)
        {
            Vip = "12";
        }
        else if (intVip >= 55000000)
        {
            Vip = "11";
        }
        else if (intVip >= 35000000)
        {
            Vip = "10";
        }
        else if (intVip >= 20000000)
        {
            Vip = "9";
        }
        else if (intVip >= 12000000)
        {
            Vip = "8";
        }
        else if (intVip >= 6000000)
        {
            Vip = "7";
        }
        else if (intVip >= 4000000)
        {
            Vip = "6";
        }
        else if (intVip >= 2000000)
        {
            Vip = "5";
        }
        else if (intVip >= 500000)
        {
            Vip = "4";
        }
        else if (intVip >= 100000)
        {
            Vip = "3";
        }
        else if (intVip >= 50000)
        {
            Vip = "2";
        }
        else if (intVip >= 10000)
        {
            Vip = "1";
        }

        return Vip;
    }




    public static Sprite GetVip(int _vipPoint)
    {

        string key = "vip" + GameHelper.GetVipLevel(_vipPoint).ToString("D2");

        //		Debug.Log(_vipPoint + " is Vip " + key);
        if (dictSprite.ContainsKey(key))
        {
            return dictSprite[key];
        }

        return Resources.Load<Sprite>("Avatar/avtBorder.png");
    }
    public static void AddVip(int _levelVip, Sprite _spr)
    {
        string key = "vip" + _levelVip.ToString("D2");
        if (dictSprite.ContainsKey(key))
            dictSprite[key] = _spr;
        else
            dictSprite.Add(key, _spr);
    }

    public static Sprite GetGoldItem(int _id)
    {

        string key = "gold" + _id.ToString();

        if (_id > 6)
            key = "gold6";

        if (dictSprite.ContainsKey(key))
        {
            return dictSprite[key];
        }

        dictSprite.Add(key, Resources.Load<Sprite>("SHOP/" + key));
        return dictSprite[key];
    }


    public static Sprite GetIconAchive(int _order)
    {
        string key = "achiv" + _order.ToString();

        if (dictSprite.ContainsKey(key))
        {
            return dictSprite[key];
        }
        else
        {
            dictSprite.Add(key, Resources.Load<Sprite>("Achivement/" + key));
            return dictSprite[key];
        }
    }
}
public class BoderAvatar
{
    public string id;
    public string vip_require;
    public string chip = "";
    public string gem = "";
}

