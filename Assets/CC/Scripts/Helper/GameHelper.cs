﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BaseCallBack;
using DG.Tweening;
using System.Collections.Generic;
using SimpleJSON;

public class GameHelper : MonoBehaviour
{
    public static string base64EncodedPublicKey="";
    public static string bugState = "Service";
    public static Dictionary<string, string> dicConfig;
	public static int currentGid;

    public static DataPayment CardPayment = new DataPayment();
    public static bool IS_LOAD_LOGIN = false;
    public static bool IsChangeVippoint = false;
	public static long IsHaveBonusDaily = 0;
    
    public static long IsHaveBonusDaily_item = 0;
    public static string IsUpgradeVip = "";

	public static bool EnableCardWheel = false;
    public static bool EnableBarGame = false;
    public static GameKind gameKind;
	public static string AndroidBase64 = "";

	public static Dictionary<string, bool> dictEnableGame = new Dictionary<string, bool>();

    public static Dictionary<string, DataNap> dictHisBuyInApp = new Dictionary<string, DataNap>();

    public static JSONNode nodeEventData = new JSONNode();

	public static List<string> lstEventBlock = new List<string> ();

    public static bool isEnalbeTournament = false;
    public static string getAppStoreURL()
    {
#if UNITY_ANDROID
        return "https://play.google.com/store/apps/details?id=" + Application.identifier;
#elif UNITY_IPHONE
        GameKind gameKind = getGameKind();
        switch (gameKind)
        {
            case GameKind.All:
                return "https://itunes.apple.com/us/app/4play-game-b%C3%A0i-online/id1392877739?ls=1&mt=8";
            case GameKind.TLMN:
                break;
            case GameKind.MB:
                break;
            case GameKind.XT:
                break;
            case GameKind.Phom:
                break;
            case GameKind.TaiXiu:
                break;
            case GameKind.TLDL:
                break;
            default:
                break;
        }
        return "";
#endif
        return "";
    }
    public static string getConfig(string key)
    {
        if (dicConfig != null && dicConfig.ContainsKey(key) == true)
        {
            return dicConfig[key];
        }
        return "";
    }
    private static List<string> listBundleID = new List<string>
    {
        "com.gamebai.gamebai4play",//0z
        //"com.Gamble.KTTT",//0
        "com.gamebai4play.TLMN",//1
        "com.gamebai4play.MauBinh",//2
        "com.gamebai.gamebai4play.Xito",//3
        "com.gamebai4play.Phom",//4
        "com.gamebai.gamebai4play.TaiXiu",//5
        "com.gamebai.gamebai4play.TLDL"//6
    };

    public static string getGameName()
    {
        GameKind curGameLKind = getGameKind();

        switch (curGameLKind)
        {
            case GameKind.All:
                return "4Play";
            case GameKind.TLMN:
                return "TLMN";
            case GameKind.MB:
                return "MẬU BINH";
            case GameKind.XT:
                return "XÌ TỐ";
            case GameKind.Phom:
                return "PHỎM";
            case GameKind.TaiXiu:
                return "TÀI XỈU";
            case GameKind.TLDL:
                return "TLĐL";
        }

        return "";
    }
    public static string getGameNameProduct()
    {
        GameKind curGameLKind = getGameKind();

        switch (curGameLKind)
        {
            case GameKind.All:
                return "4Play";
            case GameKind.TLMN:
                return "TLMN";
            case GameKind.MB:
                return "Mậu Binh";
            case GameKind.XT:
                return "Xì Tố";
            case GameKind.Phom:
                return "Phỏm";
            case GameKind.TaiXiu:
                return "Tài Xỉu";
            case GameKind.TLDL:
                return "TLĐL";
        }

        return "";
    }
    public static string getBundleIDByGameKind(GameKind gameKind)
    {
        return listBundleID[(int)gameKind];
    }
    public static GameKind getGameKind()
    {
        //return GameKind.TLMN;
        string bundleID = "";
//#if UNITY_STANDALONE
//        bundleID = "com.gamebai.gamebai4play";
//#else
        bundleID = Application.identifier;
//#endif
        //Debug.LogError("bundle ID " + bundleID);
        return (GameKind)listBundleID.IndexOf(bundleID);
    }

    public static void ChangeScene(GameScene _screen)
		{
				SceneManager.LoadScene (_screen.ToString ());
		}

	public static void ChangeScene(int _gameID)
	{
		GAMEID game = (GAMEID)_gameID;
		switch (game) {
		case GAMEID.XiTo:
			SceneManager.LoadScene (GameScene.XTScene.ToString());
			break;
		case GAMEID.MauBinh:
			SceneManager.LoadScene (GameScene.MBScene.ToString());
			break;
		case GAMEID.TaiXiu:
			SceneManager.LoadScene (GameScene.TaiXiuScene.ToString());
			break;
		case GAMEID.Phom:
			SceneManager.LoadScene (GameScene.PhomScene.ToString());
			break;
		case GAMEID.TLMN:
        case GAMEID.TLDL:
            SceneManager.LoadScene (GameScene.TLMNScene.ToString());
			break;
        case GAMEID.DuaNgua:
            SceneManager.LoadScene(GameScene.DuaNguaScence.ToString());
            break;
        case GAMEID.BaiCao:
            SceneManager.LoadScene(GameScene.BaiCaoScence.ToString());
            break;
        case GAMEID.BauCua:
            SceneManager.LoadScene(GameScene.BauCuaScene.ToString());
            break;
        }
	}

	public static IEnumerator Thread(string _url, onCallBackSprite _spr)
	{
		WWW www = new WWW (_url);
		yield return www;

		Texture2D mainImage = www.texture;
		Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
		_spr (spr);
	}
	public static IEnumerator Thread(int _index, string _url, onCallBackIntSprite _spr)
	{
		WWW www = new WWW (_url);
		yield return www;

		Texture2D mainImage = www.texture;
		Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

		_spr (_index, spr);
	}

	public static IEnumerator Thread(float _time, onCallBack _callBack)
	{
		yield return new WaitForSeconds (_time);
		_callBack ();
	}

	public static string GetNameGame(int _gameID)
	{
		GAMEID game = (GAMEID)_gameID;
		switch (game) {
		case GAMEID.XiTo:
			return "XÌ TỐ";
		case GAMEID.MauBinh:
			return "MẬU BINH";
		case GAMEID.TaiXiu:
			return "TÀI XỈU";
		case GAMEID.Phom:
			return "PHỎM";
		case GAMEID.TLMN:
			return "TIẾN LÊN MIỀN NAM";

		}
				return "Another";
	}
	public static string GetNameGame(GAMEID _gameID)
	{
		switch (_gameID) {
		case GAMEID.XiTo:
			return "XÌ TỐ";
		case GAMEID.MauBinh:
			return "MẬU BINH";
		case GAMEID.TaiXiu:
			return "TÀI XỈU";
		case GAMEID.Phom:
			return "PHỎM";
		case GAMEID.TLMN:
			return "TIẾN LÊN MIỀN NAM";

		}
		return "Another";
	}

	public static string GetNameLevel(int _level)
	{
		if (_level == 1)
			return "Bình dân";
		else if (_level == 2)
			return "Đại gia";
		else if (_level == 3)
			return "VIP";
		else
			return "Level Noname";
	}

#region VIPPOINT CONFIG
	//{100, 200, 300}
	//Vip 1: 0-99
	//Vip 2: 100-199
	//Vip 3: 200-299
	static List<int> lstMinVipPoint = new List<int> ();
	public static void SetVipPointConfig(List<int> _lstVip){
		lstMinVipPoint = new List<int> ();
		for (int i = 0; i < _lstVip.Count; i++) {
			lstMinVipPoint.Add (_lstVip [i]);
		}
	}
	public static int GetVipLevel(int _point){
		for (int i = lstMinVipPoint.Count; i > 0; i--) {
			if (_point >= lstMinVipPoint [i - 1]) {
				return i;
			}
		}
		return 0;
	}
    public static int GetMinVipPoint(int vipLevel) {
        if (vipLevel > lstMinVipPoint.Count)
            return 100000;
        
        if (vipLevel <= 0)
            return 0;

        return lstMinVipPoint[vipLevel - 1];
    }
#endregion

    const float TIME_MOTION = .3f;

	public static void DoOpen(Transform trsf, TweenCallback _callBack){
		trsf.localScale = new Vector3 (.3f, .3f, 1);
		trsf.DOScale (Vector3.one, TIME_MOTION).SetEase (Ease.OutBack).OnComplete (_callBack);
	}
	public static void DoClose(Transform trsf, TweenCallback _callBack){
		trsf.localScale = Vector3.one;
		trsf.DOScale (new Vector3 (.3f, .3f, 1), TIME_MOTION).SetEase (Ease.InBack).OnComplete (_callBack);
	}

	public static void SaveUsername(string _value)
	{
			PlayerPrefs.SetString("uname", _value);
	}
	public static string GetUsername()
	{
			return PlayerPrefs.GetString ("uname");
	}
	public static void SavePass(string _value){
		PlayerPrefs.SetString ("pass", _value);
	}
	public static string GetPass(){
		return PlayerPrefs.GetString ("pass");
	}

	public static bool EnablePayment{
		get;set;
	}
	public static bool EnableExchange{
		get;set;
	}

	public static void OpenWebView(string _url){
#if UNITY_ANDROID || UNITY_IOS

		

#endif
	}
}

public enum GAMEID
{
	None,
	XiTo = 1,
	MauBinh = 2,
	TaiXiu = 3,
	Phom = 4,
	TLMN = 5,
	TLDL = 6,
    Lottery = 8,
    DuaNgua=9,
    BaiCao=10,
    BauCua=11,
    TaiXiuMini=12,
    Event=13
}

public class DataNap : MonoBehaviour
{
    public string Token = "";
    public string Gia = ""; //productID
    public string store = "";
    public onCallBackString RspPaymentInApp;
}
