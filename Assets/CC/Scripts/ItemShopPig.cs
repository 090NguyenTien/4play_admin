﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShopPig : MonoBehaviour {

    [SerializeField]
    Text TxtTurn, TxtAppPrice, TxtCardPrice;
    [SerializeField]
    Sprite ImgBua_2, ImgBua_3;
    [SerializeField]
    public Button BtnCard, BtnApp;
    [SerializeField]
    Image Hammer;
    public bool IsCardPay = false;
    public EvenDapHeo evenDH;
    public ShopPigController ShopInApp;
    public string CardPrice = "";
    int Id_Pig = 0;
    public string StoreId = "";
 
    public int Id_Item = 0;
    //iron_hammer, silver_hammer, gold_hammer)

    public void Init(bool CardPay, int TypeHammer, string  turn, string app, string card, string storeId)
    {
        if (CardPay == true)
        {
            BtnCard.gameObject.SetActive(true);
        }
        else
        {
            BtnCard.gameObject.SetActive(false);
        }

        if (TypeHammer == 2)
        {
            Hammer.sprite = ImgBua_2;
        }
        else if (TypeHammer == 3)
        {
            Hammer.sprite = ImgBua_3;
        }

        StoreId = storeId;
        Id_Pig = TypeHammer;

        TxtTurn.text = turn + " BÚA";
        TxtAppPrice.text = app + "$";


        CardPrice = card;
        card = string.Format("{0:n0}", int.Parse(card));
        TxtCardPrice.text = card + "Đ";
        

        BtnApp.onClick.RemoveAllListeners();
        BtnApp.onClick.AddListener(BtnAppClick);
        BtnCard.onClick.RemoveAllListeners();
        BtnCard.onClick.AddListener(btnCardClick);
        if (MyInfo.IS_APK_PURE)
        {
            BtnApp.gameObject.SetActive(false);
        }
    }


    public void btnCardClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        evenDH.ShowPanelCard(CardPrice, Id_Pig);
    }

    public void BtnAppClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        ShopInApp.BuyTurnBuyInApp(Id_Item, StoreId);
    }


}
