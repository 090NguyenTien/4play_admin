﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class HomeView : MonoBehaviour
{
    /*
    public static HomeView api = null;

    [HideInInspector]
    public HomeController Controller;

    [SerializeField]
    GameObject panelUI;
    [SerializeField]
    private GameObject panelVideoReward = null;
    [SerializeField]
    Text txtUsername, txtChip;
    [SerializeField]
    Image imgAvatar, imgBorderAvt;
    [SerializeField]
	Button btnAchivement, btnShop;
	[SerializeField]
	Button btnAvatar;
    [SerializeField]
    Button BtnSettingLottery;

    [SerializeField]
	Transform trsfItemGameParent;

	[SerializeField]
	Button btnModule, btnEvent, btnCardWheel, btnInbox, btnVipInfo, btnNap, btnUuDai, btnExchange;

	[SerializeField]
	List<GameObject> lstEffect169, lstEffect43;
    [SerializeField]
    Image ImgUnreadInbox;
    [SerializeField]
    Text TxtUnreadInbox;

	[SerializeField]
	GameObject Arena;

    private void Awake()
    {
        if (api != null)
        {
            return;
        }
        api = this;
    }
    public void Init(HomeController _controller)
	{
        Debug.LogError("init homenekkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
				Controller = _controller;

		btnAchivement.onClick.AddListener (BtnAchivementOnClick);
		btnAvatar.onClick.AddListener (BtnAvatarOnClick);

		btnModule.onClick.AddListener (ModuleOnClick);
        BtnSettingLottery.onClick.AddListener(ModuleOnClick);

        btnShop.onClick.AddListener (ShopOnClick);

		btnEvent.onClick.AddListener (rewardVideo);
        panelVideoReward.GetComponent<PopupVideoReward>().initFirstTime();
        btnInbox.onClick.AddListener(InboxOnClick);
        btnVipInfo.onClick.AddListener(VipInfoOnClick);
        btnNap.onClick.AddListener(NapOnClick);
        btnUuDai.onClick.AddListener(UuDaiOnClick);
//		btnCardWheel.onClick.AddListener (CardWheelOnClick);

//		btnExchange.onClick.AddListener (Controller.BtnExchangeOnClick);


		//Scale Item game, Show Particle
		Intro ();
	}
	[SerializeField]
	GameObject TLMN, MB,XT,P,TX;
	public void SetEnableGameTLMN(bool _enable){
		TLMN.SetActive (_enable);
	}
	public void SetEnableGameMB(bool _enable){
		MB.SetActive (_enable);
	}
	public void SetEnableGameXT(bool _enable){
		XT.SetActive (_enable);
	}
	public void SetEnableGameP(bool _enable){
		P.SetActive (_enable);
	}
	public void SetEnableGameTX(bool _enable){
		TX.SetActive (_enable);
	}

	public void SetEnableArena(bool _enable){
//		Arena.SetActive (_enable);
	}

	#region LUCKY WHEEL

	void CardWheelOnClick ()
	{
		Controller.BtnCardWheelOnClick ();
	}

	#endregion

	void LogOutOnClick ()
	{
		Controller.LogOutOnClick ();
	}

	public void ModuleOnClick()
	{
		Controller.ModuleOnClick ();
	}

    void rewardVideo()
    {
        panelVideoReward.SetActive(true);
    }

    void EventOnClick ()
	{
		Controller.EventOnClick ();
	}

    void InboxOnClick()
    {
        Controller.InboxOnClick();
    }

    void VipInfoOnClick()
    {
        Controller.VipInfoOnClick();
    }

    void NapOnClick()
    {
        Controller.NapOnClick();
    }

    void UuDaiOnClick()
    {
        Controller.UuDaiOnClick();
    }

    void Intro()
	{
//		StartCoroutine (IntroItemGameThread ());

		float height = Screen.height;
		float width = Screen.width;


		float ratio1 = width / height;
		float ratio2 = 16f / 11f;
		bool is169 = ratio1 > ratio2;

		for (int i = 0; i < lstEffect169.Count; i++) {

			lstEffect169 [i].SetActive (false);
			lstEffect43 [i].SetActive (false);


//			lstEffect169 [i].SetActive (is169);
//			lstEffect43 [i].SetActive (!is169);
		}
	}
	IEnumerator IntroItemGameThread()
	{
		for (int i = 0; i < trsfItemGameParent.childCount; i++) {
			trsfItemGameParent.GetChild (i).localScale = Vector3.zero;
		}
		for (int j = 0; j < trsfItemGameParent.childCount; j++) {
			trsfItemGameParent.GetChild (j).DOScale (Vector3.one, .3f).SetEase (Ease.OutBack);
			yield return new WaitForSeconds (.05f);
		}


		float height = Screen.height;
		float width = Screen.width;


		float ratio1 = width / height;
		float ratio2 = 16f / 11f;
		bool is169 = ratio1 > ratio2;

		for (int i = 0; i < lstEffect169.Count; i++)
        {
			lstEffect169 [i].SetActive (is169);
			lstEffect43 [i].SetActive (!is169);
		}
	}

	public void ShopOnClick()
	{
		Controller.ShopOnClick ();
	}
	void BtnAchivementOnClick()
	{
		Controller.BtnAchivementOnClick ();
	}
	void BtnAvatarOnClick()
	{
		Controller.BtnAvatarOnClick ();
	}

    public void LotteryAvataOnClick()
    {
        Controller.BtnAvatarOnClick();
    }

	public void SetEnablePanelUI(bool _enable)
	{
		panelUI.SetActive (_enable);
	}
	public void SetEnableExchange(bool _enable){
//		btnExchange.gameObject.SetActive (_enable);
	}
	public void SetEnableCardWheel(bool _enable){
//		btnCardWheel.gameObject.SetActive (_enable);
	}

	public void ShowInfoUser(string _chip, string _gold)
    {
		txtChip.text = _chip;
//		txtGold.text = _gold;
	}
	public void ShowInfoUser(string _uname, string  _chip, string _gold,  string _avtName, int _vipPoint)
	{
		txtUsername.text = _uname;
		txtChip.text = _chip;
        //		txtGold.text = _gold;
        Sprite avatar = null;
        if (!MyInfo.AVATAR_FACEBOOK_LOAD)//
        {
            avatar = DataHelper.GetAvatar(_avtName);           
        }
        ShowAvatar(avatar, _avtName);
        imgBorderAvt.sprite = DataHelper.GetVip (_vipPoint);
//		StartCoroutine (GameHelper.Thread (_avtName, ShowAvatar));
	}
	public void UpdateAvatar(){
		imgAvatar.sprite = DataHelper.GetAvatar (MyInfo.AvatarName);
	}
	public void UpdateChip(string _chip)
	{
        txtChip.text = _chip;
	}

    public Transform getGoldTranform()
    {
        return txtChip.transform;
    }

    public void effectReceiveGold()
    {
        float time1 = 0.15f;
        txtChip.transform.DOKill();
        txtChip.transform.localScale = Vector2.one;
        txtChip.transform.DOScale(Vector2.one * 0.8f, time1).SetEase(Ease.Linear).OnComplete(() => {
            txtChip.transform.DOScale(Vector2.one * 1.1f, time1).SetEase(Ease.Linear).OnComplete(() => {
                txtChip.transform.DOScale(Vector2.one * 0.9f, time1).SetEase(Ease.Linear).OnComplete(() =>
                {
                    txtChip.transform.DOScale(Vector2.one, time1).SetEase(Ease.Linear);
                });
            }); ;
        });
    }
	public void UpdateGold(string _gold)
	{
//		txtGold.text = _gold;
	}

	void ShowAvatar(Sprite _spr, string urlAvatar="")
	{
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        
        if (loginType == LoginType.Facebook)
        {
            if (!MyInfo.AVATAR_FACEBOOK_LOAD)
            {
                MyInfo.AVATAR_FACEBOOK_LOAD = true;
                StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=normal&access_token=" + AccessToken.CurrentAccessToken.TokenString, this.imgAvatar));
            }
            else
            {
                imgAvatar.sprite = MyInfo.sprAvatar;
            }           
        }
        else
        {
            MyInfo.sprAvatar = _spr;
            imgAvatar.sprite = _spr;           
        }
    }
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        MyInfo.sprAvatar = avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
        
    }
    public void ShowOrHideUnreadInbox()
    {
        if (MyInfo.UNREAD_INBOX == 0)
        {
            TxtUnreadInbox.gameObject.SetActive(false);
            ImgUnreadInbox.gameObject.SetActive(false);
        }
        else
        {
            TxtUnreadInbox.text = MyInfo.UNREAD_INBOX + "";
            TxtUnreadInbox.gameObject.SetActive(true);
            ImgUnreadInbox.gameObject.SetActive(true);
        }
    }*/
}
