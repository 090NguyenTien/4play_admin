﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
public class ChangeBundleID : EditorWindow
{
    [MenuItem("Tools/Change BundleID %#t")]
    public static void Init()
    {
        // Get existing open window or if none, make a new one:
        ChangeBundleID window = (ChangeBundleID)EditorWindow.GetWindow(typeof(ChangeBundleID));
        window.Show();
    }

    int indexSelect = 0;
    string bundleVerisonCode = "";
    private GameKind[] arrBundleID = new GameKind[]
        {
            GameKind.All,
            GameKind.TLMN,
            GameKind.MB,
            GameKind.XT,
            GameKind.Phom,
            GameKind.TaiXiu,
            GameKind.TLDL
        };

     
    void OnGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        //EditorGUILayout.BeginHorizontal();
        //{
        indexSelect = EditorGUILayout.Popup(indexSelect, new string[] { "ALL", "TLMN", "Mậu Binh", "Xì Tố", "Phỏm", "Tài Xỉu", "TL Đếm lá" });
        //}
        //EditorGUILayout.Popup()

        EditorGUILayout.Space();
        bundleVerisonCode =  EditorGUILayout.TextField("Bundle version Code: ", bundleVerisonCode);
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        

        if (GUILayout.Button("Apply"))
        {
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, GameHelper.getBundleIDByGameKind((GameKind)indexSelect));
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, GameHelper.getBundleIDByGameKind((GameKind)indexSelect));
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.WSA, GameHelper.getBundleIDByGameKind((GameKind)indexSelect));
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Standalone, GameHelper.getBundleIDByGameKind((GameKind)indexSelect));

            PlayerSettings.applicationIdentifier = GameHelper.getBundleIDByGameKind((GameKind)indexSelect);

            PlayerSettings.productName = GameHelper.getGameNameProduct();


            if (bundleVerisonCode.Length > 0)
            {
                PlayerSettings.Android.bundleVersionCode = int.Parse(bundleVerisonCode);
                PlayerSettings.bundleVersion = bundleVerisonCode.ToString() + "." + indexSelect.ToString();
            }
            else
            {
                PlayerSettings.bundleVersion = PlayerSettings.Android.bundleVersionCode.ToString() + "." + indexSelect.ToString();
            }
            

            SetApplicationIcon(BuildTargetGroup.Android, (GameKind)indexSelect);
            //SetApplicationIcon(BuildTargetGroup.iOS, (GameKind)indexSelect);

            //FacebookSettings.SelectedAppIndex = indexSelect;
            //Debug.LogError("ABCEXDFRDFSW========"+ indexSelect);
            //if (indexSelect == 0)//select all game
            //{                
            //    for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            //    {
            //        SetScene(EditorBuildSettings.scenes[i].path, true);
            //    }
            //    SetScene(EditorBuildSettings.scenes[1].path, false);
            //}
            //else//select 1 game
            //{
                //Debug.LogError("ABCEXDFRDFSW========" + indexSelect);
                //Debug.LogError("EditorBuildSettings.scenes.Length========" + EditorBuildSettings.scenes.Length);               
                //for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
                //{
                //    SetScene(EditorBuildSettings.scenes[i].path, true);
                //}
               // SetScene(EditorBuildSettings.scenes[2].path, false);

                //comment code set Scence Cua Thai
                /*
                SetScene(EditorBuildSettings.scenes[1].path, true);
                int activeMiniIndexScene = indexSelect;
                if (indexSelect == 6)//la tien len dem la
                {
                    activeMiniIndexScene = 1;//active giong voi tien len dem la
                }
                for (int i = 4; i < EditorBuildSettings.scenes.Length; i++)
                {
                    if (i == activeMiniIndexScene + 3)//bo qua 4 screen dau
                    {
                        SetScene(EditorBuildSettings.scenes[i].path, true);
                    }
                    else
                    {
                        SetScene(EditorBuildSettings.scenes[i].path, false);
                    }

                }
                SetScene(EditorBuildSettings.scenes[2].path, false);
                SetScene(EditorBuildSettings.scenes[8].path, true);*/
                //End

            //}

            //Debug.LogError("Bundle ID dc set thanh " + Application.identifier);

        }
    }

    static void SetApplicationIcon(BuildTargetGroup platform, GameKind gameKind)
    {
        Debug.Log(gameKind.ToString());
        Texture2D textureIcon = Resources.Load("Icon/icon_" + gameKind.ToString()) as Texture2D;
        //Texture2D textureIcon = Resources.Load("../Icon/icon_" + gameKind.ToString()) as Texture2D;
        Texture2D[] iconSet;
        int[] num;
        if (platform == BuildTargetGroup.Android)
        {
            iconSet = new Texture2D[6];
            num = new int[] {192 ,144, 96, 72, 48, 36 };

        }
        else if (platform == BuildTargetGroup.iOS)
        {
            iconSet = new Texture2D[19];
            num = new int[] { 180, 167, 152, 144, 120, 114, 76, 72, 57, 120, 80, 40, 87, 58, 29, 60, 40, 20, 1024 };

        }
        else
        {
            Debug.LogError("Switch to ANDROID or IOS platform");
            return;
        }

        for (int i = 0; i < num.Length; i++)
        {
            int n = num[i];
            Texture2D icon = new Texture2D(n, n);
            icon = textureIcon;
            iconSet[i] = icon;
        }
        PlayerSettings.SetIconsForTargetGroup(platform, iconSet);
    }

    void SetScene(string scenePatch, bool sceneEnabled)
    {
        EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
        foreach (EditorBuildSettingsScene scene in scenes)
        {
            if (scene.path.Contains(scenePatch))
            {
                scene.enabled = sceneEnabled;
            }
        }
        EditorBuildSettings.scenes = scenes;
    }
}