﻿using BaseCallBack;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeNameController : MonoBehaviour
{
    [SerializeField]
    Text txtDescription;
    [SerializeField]
    InputField NewName, OldName;
    onCallBackString onClosePanel;
    [SerializeField]
    InputField inputUsername;
    int fee;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Show(onCallBackString _onClosePanel)
    {
        onClosePanel = _onClosePanel;
        gameObject.SetActive(true);
        //call service get info init
        API.Instance.RequestGetChangeNameInfo(RspGetChangeNameInfo);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    private void RspGetChangeNameInfo(string _json)
    {
        OldName.text = MyInfo.NAME;
        JSONNode data = JSONNode.Parse(_json);
        fee = int.Parse(data["change_name_fee"].Value);
        txtDescription.text = "Phí Đổi Tên Là <color=yellow>" + Utilities.GetStringMoneyByLong(fee)+" Chip</color>";

        NewName.contentType = InputField.ContentType.Name;
    }

    public void ChangeName()
    {
        if (NewName.text == "")
        {
            AlertController.api.showAlert("Bạn chưa nhập tên cần đổi!");
            return;
        }else if (NewName.text.Length<6 || NewName.text.Length > 19)
        {
            AlertController.api.showAlert("Tên Mới Phải Nhiều Hơn 5 Ký Tự Và Ít Hơn 20 Ký Tự!");
            return;
        }
        if (MyInfo.CHIP < fee)
        {
            AlertController.api.showAlert("Bạn Không Đủ Chip Để Đổi Tên!");
            return;
        }
        AlertController.api.showAlert("Bạn Có Chắc Muốn Đổi Tên Với Chi Phí Là <color=yellow>"+ Utilities.GetStringMoneyByLong(fee)+" Chip</color>", ()=>{
            API.Instance.RequestChangeName(NewName.text, RspChangeName);
        });        
    }

    private void RspChangeName(string _json)
    {
        string name_key = "";
        JSONNode data = JSONNode.Parse(_json);
        if (int.Parse(data["status"].Value) == 1)
        {
            AlertController.api.showAlert("ĐỔI TÊN THÀNH CÔNG!");
            MyInfo.CHIP = long.Parse(data["chip"].Value);
            MyInfo.NAME = (data["name"].Value);
            name_key = (data["name_key"].Value);
            if (inputUsername != null) inputUsername.text = MyInfo.NAME;
        }
        else
        {
            AlertController.api.showAlert(data["msg"].Value);
        }
        onClosePanel(name_key);
    }
}
