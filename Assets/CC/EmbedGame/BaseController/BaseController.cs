﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    GameObject ModuleChangeGem;
    GameObject ModuleSuKien;
    GameObject ModuleDangTin;
    GameObject ModuleListBangHoi;
    GameObject ModuleInfoBangHoi;

    public int Y_SuKien;

    public void OpenModuleChangeGem(GameObject _Parent)
    {
        ModuleChangeGem = Instantiate(Resources.Load("Modules/PanelChangeGem", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, _Parent.transform) as GameObject;        
        
    }
    public void OpenModuleSuKien(GameObject _Parent)
    {
        ModuleSuKien = Instantiate(Resources.Load("Modules/PanelSuKien", typeof(GameObject)), new Vector3(0, Y_SuKien, 0), Quaternion.identity, _Parent.transform) as GameObject;
    }
    public void OpenModuleDangTin(GameObject _Parent)
    {
        ModuleDangTin = Instantiate(Resources.Load("Modules/PanelDangTin", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, _Parent.transform) as GameObject;
    }
    public void OpenModuleListBangHoi(GameObject _Parent)
    {
        ModuleDangTin = Instantiate(Resources.Load("Modules/PanelListBangHoi", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, _Parent.transform) as GameObject;
    }
    public void OpenModuleInfoBangHoi(GameObject _Parent)
    {
        if (MyInfo.BangChatRoomName == "")
        {
            AlertController.api.showAlert("Bạn Chưa Tham Gia Vào Bang! Vui Lòng Tham Gia Vào Ít Nhất 1 Bang", ()=> {
                OpenModuleListBangHoi(_Parent);
            });
        }
        else
        {
            ModuleInfoBangHoi = Instantiate(Resources.Load("Modules/PanelBangHoi", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, _Parent.transform) as GameObject;
        }        
    }
}
