﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardCheats : MonoBehaviour
{
    [SerializeField]
    List<CardCheat> lstCardCheat;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SetCardIndex(int index,string cardId)
    {
        lstCardCheat[index].Init(cardId);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
